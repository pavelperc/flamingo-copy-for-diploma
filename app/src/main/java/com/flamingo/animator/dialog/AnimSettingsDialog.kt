package com.flamingo.animator.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.SeekBar
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.flamingo.animator.R
import com.flamingo.animator.editors.drawingEditor.dataHolders.AnimContainer
import com.flamingo.animator.utils.realmUtils.transaction

class AnimSettingsDialog(val animContainer: AnimContainer) : DialogFragment() {
    private val anim = animContainer.animation
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme);
    }
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.anim_settings_fragment, container)
        
        val btnClose = root.findViewById<ImageButton>(R.id.btnClose)
        btnClose.setOnClickListener {
            this.dismiss()
        }
        
        val tvFps = root.findViewById<TextView>(R.id.tvFps)
        val sbFps = root.findViewById<SeekBar>(R.id.sbFps)
        tvFps.text = anim.fps.toString()
        sbFps.max = 12
        sbFps.progress = Math.floor(anim.fps).toInt()
        
        sbFps.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (progress == 0) {
                    anim.transaction { fps = 0.5 }
                } else {
                    anim.transaction { fps = progress.toDouble() }
                }
                tvFps.text = anim.fps.toString()
            }
            
            override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit
            override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit
        })
        
        return root
    }
}