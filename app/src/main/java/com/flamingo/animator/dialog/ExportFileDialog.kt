package com.flamingo.animator.dialog

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.FileProvider
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.flamingo.animator.R
import com.flamingo.animator.utils.SomeUsefulActivity
import com.flamingo.animator.utils.commonUtils.logError
import com.flamingo.animator.utils.commonUtils.setVisible
import kotlinx.android.synthetic.main.dialog_export_file.*
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.toast
import java.io.File

class ExportFileDialog(
    val activity: SomeUsefulActivity,
    val internalFile: File,
    val suggestedName: String,
    val extension: String,
    val heading: String = "Export file",
    val message: String? = null,
    val shareType: String = "*/*",
    val onSuccess: () -> Unit = {}
) : DialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_export_file, container)
    }
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        isCancelable = false
        
        if (message != null) {
            tvMessage.setVisible()
            tvMessage.setText(message)
        }
        
        etFileName.setText(suggestedName)
        tvHeading.setText(heading)
        
        btnCancel.setOnClickListener {
            dismiss()
        }
        
        if (!internalFile.exists()) {
            logError("ExportFileDialog: file not exists: " + internalFile.absolutePath)
            dismiss()
            return
        }
        
        when (extension) {
            "gif" -> {
                ivPreview.setVisible()
                Glide.with(this)
                    .asGif()
                    .load(internalFile)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(ivPreview)
            }
            "png", "jpg" -> {
                ivPreview.setVisible()
                Glide.with(this)
                    .load(internalFile)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(ivPreview)
            }
        }
        
        btnSaveToDownloads.setOnClickListener {
            activity.requestWritePermission {
                val downloadsFolder = android.os.Environment.getExternalStoragePublicDirectory(
                    android.os.Environment.DIRECTORY_DOWNLOADS
                )
                if (!downloadsFolder.canWrite()) {
                    longToast("Can not write to downloads folder:\n$downloadsFolder")
                    return@requestWritePermission
                }
                
                if (downloadsFolder == null) {
                    toast("Downloads folder not found")
                    return@requestWritePermission
                }
                val enteredName = etFileName.text.toString()
                var newName = "$enteredName.$extension"
                var i = 1
                while (downloadsFolder.resolve(newName).exists()) {
                    newName = "$enteredName(${i++}).$extension"
                }
                val file = downloadsFolder.resolve(newName)
                internalFile.copyTo(file, overwrite = true)
                longToast("Saved to " + file.absolutePath)
                dismiss()
                onSuccess()
            }
        }
        
        btnShare.setOnClickListener {
            val renamedFile = activity.repo.cacheDir.resolve("${etFileName.text}.$extension")
            internalFile.copyTo(renamedFile, overwrite = true)
            val contentUri = FileProvider.getUriForFile(
                activity,
                "com.flamingo.animator.fileprovider",
                renamedFile
            )
            
            val share = Intent(Intent.ACTION_SEND)
            share.type = shareType
            share.putExtra(Intent.EXTRA_STREAM, contentUri)
            activity.startActivity(Intent.createChooser(share, heading))
            dismiss()
            onSuccess()
        }
    }
}

// warning, file is renamed after sharing
fun SomeUsefulActivity.showExportFileDialog(
    file: File,
    suggestedName: String,
    extension: String,
    heading: String = "Export file",
    message: String? = null,
    shareType: String = "*/*",
    onSuccess: () -> Unit = {}
) {
    ExportFileDialog(
        activity = this,
        internalFile = file,
        suggestedName = suggestedName,
        extension = extension,
        message = message,
        heading = heading,
        shareType = shareType,
        onSuccess = onSuccess
    )
        .show(supportFragmentManager, "export_file_dialog")
}
fun SomeUsefulActivity.showExportGifDialog(
    file: File,
    suggestedName: String
) {
    showExportFileDialog(
        file = file,
        suggestedName = suggestedName,
        extension = "gif",
        heading = "Export gif",
        shareType = "image/gif"
    )
}
