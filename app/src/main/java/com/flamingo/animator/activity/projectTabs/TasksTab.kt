package com.flamingo.animator.activity.projectTabs

import com.flamingo.animator.activity.ProjectActivity
import com.flamingo.animator.utils.commonUtils.setGone
import com.flamingo.animator.utils.commonUtils.setVisible
import kotlinx.android.synthetic.main.tasks_tab.*

class TasksTab(val activity: ProjectActivity) : ProjectActivityTab {
    override fun setGone() {
        activity.llTasks.setGone()
    }
    
    override fun setVisible() {
        activity.llTasks.setVisible()
    }
}