package com.flamingo.animator.activity.projectTabs

import android.app.Activity
import android.content.Intent
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.flamingo.animator.R
import com.flamingo.animator.activity.BackgroundActivity
import com.flamingo.animator.activity.ObjectActivity
import com.flamingo.animator.activity.ProjectActivity
import com.flamingo.animator.activity.spine.SpineInfoActivity
import com.flamingo.animator.adapters.AssetListAdapter
import com.flamingo.animator.adapters.BackgroundListAdapter
import com.flamingo.animator.adapters.ObjectListAdapter
import com.flamingo.animator.adapters.SpineListAdapter
import com.flamingo.animator.export.flampack.importFlampackDialog
import com.flamingo.animator.export.flampack.showExportFlampackDialog
import com.flamingo.animator.model.Asset
import com.flamingo.animator.model.Background
import com.flamingo.animator.model.Object
import com.flamingo.animator.utils.commonUtils.*
import com.flamingo.animator.utils.dialogUtils.enterNameAlert
import kotlinx.android.synthetic.main.activity_project.*
import kotlinx.android.synthetic.main.assets_tab.*
import org.jetbrains.anko.*


interface ProjectActivityTab {
    val selectionEnabled: Boolean get() = false
    
    fun enableSelectionMode(enabled: Boolean) {
    }
    
    fun setVisible()
    
    fun setGone()
}

// TODO refactor to fragments or not??

/** Class for handling assets tab. Setups the activity. */
class AssetsTab(val activity: ProjectActivity) : ProjectActivityTab {
    
    /** Adapter for backgrounds recycle view. */
    val backgroundsAdapter: BackgroundListAdapter
    val objectsAdapter: ObjectListAdapter
    val spinesAdapter: SpineListAdapter
    
    val projectRepo = activity.projectRepo
    
    
    /** Starts activity to create an asset. */
    private inline fun <reified T : Activity, A : Asset> createAsset(
        assetAdapter: AssetListAdapter<A>,
        crossinline onIdReceived: (Long) -> A
    ) {
        activity.activityHelper.startForResult<T>(
            "projectName" to activity.projectName,
            "createNew" to true
        ) { _, data ->
            val createdId = data?.getLongExtra("createdId", -1) ?: -1
            if (createdId != -1L) {
                val asset = onIdReceived(createdId)
                assetAdapter.assets.add(asset)
            }
            updateAssetsIfNeed(data)
        }
    }
    
    init {
        with(activity) {
            // setupAssetLists
            
            // setup backgrounds:
            backgroundsAdapter = BackgroundListAdapter(this@AssetsTab)
            rvBackgrounds.layoutManager = LinearLayoutManager(activity)
            rvBackgrounds.adapter = backgroundsAdapter
            
            // setup objects:
            objectsAdapter = ObjectListAdapter(this@AssetsTab)
            rvObjects.layoutManager = LinearLayoutManager(activity)
            rvObjects.adapter = objectsAdapter
            
            // setup spines:
            spinesAdapter = SpineListAdapter(this@AssetsTab)
            rvSpines.layoutManager = LinearLayoutManager(activity)
            rvSpines.adapter = spinesAdapter
            
            updateAssetsContent()
    
    
            fabAddAsset.setClosedOnTouchOutsideAndExpand()
            
            
            fabBackground.setOnClickListener {
                fabAddAsset.close(false)
                // launch the activity
                createAsset<BackgroundActivity, Background>(backgroundsAdapter) { assetId ->
                    assetRepo.findBackgroundById(assetId)
                }
            }
            
            fabObject.setOnClickListener {
                fabAddAsset.close(false)
                // launch the activity
                createAsset<ObjectActivity, Object>(objectsAdapter) { assetId ->
                    assetRepo.findObjectById(assetId)
                }
            }
            
            fabSpineObject.setOnClickListener {
                fabAddAsset.close(false)
                // create here with name!! todo create all like this
                val spineNames = spinesAdapter.assets.toList().map { it.name }
                val uniqueName = uniqueName(spineNames, "spine")
                enterNameAlert("Enter spine name:", uniqueName) { newName ->
                    
                    if (!assetRepo.isAssetNameUnique(newName)) {
                        toast("Name $newName is already used.")
                        false
                    } else {
                        val spine = assetRepo.createSpineObject(newName)
                        spinesAdapter.assets.add(spine)
                        
                        startActivity<SpineInfoActivity>(
                            "projectName" to activity.projectName,
                            "assetId" to spine.id
                        )
                        true
                    }
                }
            }
            
            
            btnDeleteSelected.setOnClickListener {
                deleteSelectedAssets()
            }
            
            btnExportSelected.setOnClickListener {
                exportSelectedAssets()
            }
            
            btnSelectAll.setOnClickListener {
                val selectedBackgrounds = backgroundsAdapter.selectedAssets
                val selectedObjects = objectsAdapter.selectedAssets
                val selectedSpines = spinesAdapter.selectedAssets
                
                if (selectedBackgrounds.size == backgroundsAdapter.assets.size()
                    && selectedObjects.size == objectsAdapter.assets.size()
                    && selectedSpines.size == spinesAdapter.assets.size()
                ) {
                    selectedBackgrounds.clear()
                    selectedObjects.clear()
                    selectedSpines.clear()
                } else {
                    selectedBackgrounds.addAll(backgroundsAdapter.assets.toList())
                    selectedObjects.addAll(objectsAdapter.assets.toList())
                    selectedSpines.addAll(spinesAdapter.assets.toList())
                }
                backgroundsAdapter.notifyDataSetChanged()
                objectsAdapter.notifyDataSetChanged()
                spinesAdapter.notifyDataSetChanged()
            }
            
            btnImportRes.setOnClickListener {
                importFlampackDialog(assetRepo) {
                    updateAssetsContent()
                }
            }
            setupHidingAssets()
        }
    }
    
    private fun exportSelectedAssets() {
        val selectedBackgrounds = backgroundsAdapter.selectedAssets
        val selectedObjects = objectsAdapter.selectedAssets
        val selectedSpines = spinesAdapter.selectedAssets
        val size = selectedBackgrounds.size + selectedObjects.size + selectedSpines.size
        if (size == 0) {
            return
        }
    
        with(activity) {
            showExportFlampackDialog(
                assetRepo,
                projectName,
                selectedBackgrounds.toList(),
                selectedObjects.toList(),
                selectedSpines.toList()
            ) {
                enableSelectionMode(false)
            }
        }
    }
    
    private fun deleteSelectedAssets() {
        val selectedBackgrounds = backgroundsAdapter.selectedAssets
        val selectedObjects = objectsAdapter.selectedAssets
        val selectedSpines = spinesAdapter.selectedAssets
        val size = selectedBackgrounds.size + selectedObjects.size + selectedSpines.size
        if (size == 0) {
            return
        }
        with(activity) {
            alert {
                message = if (size > 1) "Delete $size assets?" else "Delete $size asset?"
                cancelButton { }
                yesButton {
                    // search in list before removing from db to avoid accessing deleted fields!!
                    selectedBackgrounds.forEach { bg ->
                        backgroundsAdapter.assets.remove(bg)
                        assetRepo.removeBackground(bg)
                    }
                    selectedObjects.forEach { obj ->
                        objectsAdapter.assets.remove(obj)
                        assetRepo.removeObject(obj)
                    }
                    selectedSpines.forEach { spine ->
                        spinesAdapter.assets.remove(spine)
                        assetRepo.removeSpine(spine)
                    }
                    // also we delete all invalid old selectedAssets from adapters in enableSelectionMode function
                    // (it is important!!)
                    enableSelectionMode(false)
                }
            }.show()
        }
    }
    
    private fun setupHidingAssets() {
        val projectSettings = projectRepo.projectSettings
        
        fun setVisible(tv: TextView, rv: RecyclerView, visible: Boolean) {
            rv.setVisible(visible)
            val triangle = if (visible) R.drawable.ic_down_triangle else R.drawable.ic_up_triangle
            tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, triangle, 0)
        }
        fun changeAndSave(tv: TextView, rv: RecyclerView, settingsName: String) {
            // don't hide if empty
            if (rv.isVisible() && rv.adapter?.itemCount == 0) {
                return
            }
            setVisible(tv, rv, !rv.isVisible())
            projectSettings.put(settingsName, rv.isVisible())
            projectRepo.saveProjectSettings()
        }
        
        val bgVisible = projectSettings.optBoolean("backgroundListVisible", true)
        val objVisible = projectSettings.optBoolean("objectListVisible", true)
        val spineVisible = projectSettings.optBoolean("spineListVisible", true)
        with(activity) {
            setVisible(tvBackgrounds, rvBackgrounds, bgVisible)
            setVisible(tvObjects, rvObjects, objVisible)
            setVisible(tvSpines, rvSpines, spineVisible)
            
            tvBackgrounds.setOnClickListener {
                changeAndSave(tvBackgrounds, rvBackgrounds, "backgroundListVisible")
            }
            tvObjects.setOnClickListener {
                changeAndSave(tvObjects, rvObjects, "objectListVisible")
            }
            tvSpines.setOnClickListener {
                changeAndSave(tvSpines, rvSpines, "spineListVisible")
            }
        }
    }
    
    fun updateAssetsContent() {
        updateBackgroundsContent()
        updateObjectsContent()
        updateSpinesContent()
    }
    
    /** Updates assets, came from params in some activity intent */
    fun updateAssetsIfNeed(resultIntent: Intent?) {
        if (resultIntent == null) {
            return
        }
        if (resultIntent.getBooleanExtra("updateBackgrounds", false)) {
            updateBackgroundsContent()
        }
        if (resultIntent.getBooleanExtra("updateObjects", false)) {
            updateObjectsContent()
        }
        if (resultIntent.getBooleanExtra("updateSpines", false)) {
            updateSpinesContent()
        }
        
    }
    
    
    fun updateBackgroundsContent() {
        backgroundsAdapter.assets.clear()
        backgroundsAdapter.assets.addAll(activity.assetRepo.getAllBackgrounds())
    }
    fun updateObjectsContent() {
        objectsAdapter.assets.clear()
        objectsAdapter.assets.addAll(activity.assetRepo.getAllObjects())
    }
    fun updateSpinesContent() {
        spinesAdapter.assets.clear()
        spinesAdapter.assets.addAll(activity.assetRepo.getAllSpines())
    }
    
    
    
    override fun setVisible() {
        activity.llAssets.setVisible()
        activity.btnImportRes.setVisible()
    }
    
    override fun setGone() {
        activity.llAssets.setGone()
        activity.btnImportRes.setGone()
        activity.fabAddAsset.close(false)
    }
    
    override val selectionEnabled: Boolean
        get() = backgroundsAdapter.selectionEnabled
    
    override fun enableSelectionMode(enabled: Boolean) {
        with(activity) {
            // in assets xml
            llHeaderAssetsSelection.setVisible(enabled)
            // in activity xml
            llHeaderCommon.setVisible(!enabled)
        }
        
        backgroundsAdapter.enableSelectionMode(enabled)
        objectsAdapter.enableSelectionMode(enabled)
        spinesAdapter.enableSelectionMode(enabled)
    }
}