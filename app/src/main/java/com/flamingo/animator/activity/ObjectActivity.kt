package com.flamingo.animator.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.flamingo.animator.R
import com.flamingo.animator.adapters.AnimListAdapter
import com.flamingo.animator.editors.drawingEditor.showPictureSizeDialog
import com.flamingo.animator.model.Animation
import com.flamingo.animator.model.Object
import com.flamingo.animator.repository.AssetRepo
import com.flamingo.animator.repository.ProjectRepo
import com.flamingo.animator.utils.SomeUsefulActivity
import com.flamingo.animator.utils.commonUtils.setUnderline
import com.flamingo.animator.utils.dialogUtils.enterNameAlert
import com.flamingo.animator.utils.importUtils.importImage
import com.flamingo.animator.utils.realmUtils.transaction
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_object.*
import org.jetbrains.anko.toast

class ObjectActivity : SomeUsefulActivity() {
    /** Database Entity, it updates automatically. */
    lateinit var obj: Object
    lateinit var projectRepo: ProjectRepo
    val assetRepo by lazy { AssetRepo(projectRepo) }
    val spineRepo get() = assetRepo.spineRepo
    
    private val realm: Realm
        get() = assetRepo.realm
    
    val animListAdapter by lazy { AnimListAdapter(this) }
    
    val projectName by lazy { intent.getStringExtra("projectName") }
    val isNew by lazy { intent.getBooleanExtra("createNew", true) }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_object)
        projectRepo = ProjectRepo(repo, projectName)
        
        obj = if (isNew) {
            assetRepo.createObject()
        } else {
            val assetId = intent.getLongExtra("assetId", -1)
            check(assetId != -1L) { "Not received assetId in background activity." }
            assetRepo.findObjectById(assetId)
        }
        
        tvName.text = obj.name
        tvName.setUnderline()
        tvName.setOnClickListener {
            enterNameAlert("Enter asset name:", obj.name) { newName ->
                if (!assetRepo.isAssetNameUnique(newName)) {
                    toast("Name $newName is already used.")
                    false
                } else {
                    obj.transaction { name = newName }
                    tvName.text = newName
                    true
                }
            }
        }
        
        fabAddAnim.setOnClickListener {
            askPictureSizeIfNeed { width, height ->
                fabAdd.close(false)
                createAnim(width, height)
            }
        }
        fabImportImage.setOnClickListener {
            importImage { bitmap ->
                fabAdd.close(false)
                createAnimFromImage(bitmap)
            }
        }
        
        btnBack.setOnClickListener {
            finish()
        }
        rvAnimations.layoutManager = LinearLayoutManager(this)
        rvAnimations.adapter = animListAdapter
        
        animListAdapter.anims.addAll(obj.animations)
    }
    
    fun askPictureSizeIfNeed(doAfter: (width: Int, height: Int) -> Unit) {
        if (obj.width == 0 || obj.height == 0) {
            showPictureSizeDialog(this, false) { width, height, bitmap ->
                doAfter(width, height)
            }
        } else {
            doAfter(obj.width, obj.height)
        }
    }
    
    private fun openAnim(anim: Animation) {
        activityHelper.startForResult<DrawingActivity>(
            "drawingSetup" to DrawingSetup(
                0, // don't use it
                projectName,
                anim.drawConfigReq.id,
                anim.name,
                anim.id
            )
        )
        { _, intent ->
            animListAdapter.anims.add(anim)
            
            passResultMark("updateSpines", intent)
        }
    }
    
    // todo make frameToCopyFrom
    fun createAnim(width: Int, height: Int) {
        
        val anim = assetRepo.transaction {
            initAnim(obj, width, height, null)
        }
//        if (stateToCopyFrom != null) {
//            animation.transaction { drawConfigReq.copyFromOther(stateToCopyFrom.drawConfigReq) }
//        }
        
        openAnim(anim)
    }
    
    fun createAnimFromImage(bitmap: Bitmap) {
        val anim = assetRepo.transaction {
            initAnim(obj, bitmap.width, bitmap.height, null, bitmap)
        }
        openAnim(anim)
        
    }
    
    
    override fun finish() {
        if (isNew) {
            val data = Intent()
            data.putExtra("createdId", obj.id)
            setResult(Activity.RESULT_OK, data)
        }
        super.finish()
    }
    
    override fun onDestroy() {
        projectRepo.close()
        super.onDestroy()
    }
}