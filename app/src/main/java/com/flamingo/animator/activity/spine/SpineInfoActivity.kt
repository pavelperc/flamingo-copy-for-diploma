package com.flamingo.animator.activity.spine

import android.graphics.BitmapFactory
import android.os.Bundle
import com.flamingo.animator.R
import com.flamingo.animator.activity.DrawingActivity
import com.flamingo.animator.activity.DrawingSetup
import com.flamingo.animator.editors.drawingEditor.showPictureSizeDialog
import com.flamingo.animator.model.spine.Spine
import com.flamingo.animator.repository.AssetRepo
import com.flamingo.animator.repository.ProjectRepo
import com.flamingo.animator.utils.SomeUsefulActivity
import com.flamingo.animator.utils.commonUtils.setUnderline
import com.flamingo.animator.utils.dialogUtils.enterNameAlert
import com.flamingo.animator.utils.realmUtils.transaction
import kotlinx.android.synthetic.main.activity_spine_info.*
import org.jetbrains.anko.toast
import java.io.File


class SpineInfoActivity : SomeUsefulActivity() {
    
    lateinit var projectRepo: ProjectRepo
    val assetRepo by lazy { AssetRepo(projectRepo) }
    
    lateinit var spine: Spine
    lateinit var projectName: String
    lateinit var imageFile: File
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spine_info)
        
        
        projectName = intent.getStringExtra("projectName")
        projectRepo = ProjectRepo(repo, projectName)
        
        
        val assetId = intent.getLongExtra("assetId", -1)
        check(assetId != -1L) { "Not received assetId in spine activity." }
        
        spine = assetRepo.findSpineById(assetId)
        imageFile = spine.previewReq.getFile(assetRepo.assetsDir)
        
        updateImageFile()
        
        tvName.text = spine.name
        tvName.setUnderline()
        
        tvName.setOnClickListener {
            enterNameAlert("Enter spine name:", spine.name) { newName ->
                if (!assetRepo.isAssetNameUnique(newName)) {
                    toast("Name $newName is already used.")
                    false
                } else {
                    spine.transaction { name = newName }
                    tvName.text = newName
                    true
                }
            }
        }
        btnBack.setOnClickListener {
            finish()
        }
        llRigging.setOnClickListener {
            activityHelper.startForResult<SpineEditorActivity>(
                "projectName" to projectName,
                "spineId" to spine.id
            ) { resultCode, data ->
                updateImageFile()
            }
        }
        llDrawing.setOnClickListener {
            createOrOpenUncroppedImage()
        }
    }
    
    private fun updateImageFile() {
        if (imageFile.exists()) {
            ivSpine.setImageBitmap(BitmapFactory.decodeFile(imageFile.absolutePath))
        } else {
            ivSpine.setImageDrawable(getDrawable(R.drawable.spine_tutorial))
        }
    }
    
    /** create or open image for drawing. */
    private fun createOrOpenUncroppedImage() {
        if (spine.uncroppedImageReq.layers.isEmpty()) {
            
            showPictureSizeDialog(this) { width, height, bitmap ->
                
                assetRepo.transaction {
                    initLayeredImage(
                        spine.uncroppedImageReq,
                        width,
                        height,
                        predefinedBitmap = bitmap
                    )
                }
                openDrawingEditor()
            }
        } else {
            openDrawingEditor()
        }
    }
    
    private fun openDrawingEditor() {
        activityHelper.startForResult<DrawingActivity>(
            "drawingSetup" to DrawingSetup(
                spine.uncroppedImageReq.id,
                projectName,
                spine.uncroppedImageDrawConfigReq.id,
                spine.name,
                spineId = spine.id
            )
        ) { resultCode, intent ->
            updateImageFile()
        }
    }
    
    
    override fun onDestroy() {
        projectRepo.close()
        super.onDestroy()
    }
}
