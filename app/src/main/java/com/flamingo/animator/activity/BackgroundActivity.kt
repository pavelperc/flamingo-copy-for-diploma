package com.flamingo.animator.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import com.flamingo.animator.R
import com.flamingo.animator.editors.drawingEditor.showPictureSizeDialog
import com.flamingo.animator.model.Background
import com.flamingo.animator.repository.AssetRepo
import com.flamingo.animator.repository.ProjectRepo
import com.flamingo.animator.utils.SomeUsefulActivity
import com.flamingo.animator.utils.commonUtils.setGone
import com.flamingo.animator.utils.commonUtils.setUnderline
import com.flamingo.animator.utils.commonUtils.setVisible
import com.flamingo.animator.utils.dialogUtils.enterNameAlert
import com.flamingo.animator.utils.realmUtils.transaction
import kotlinx.android.synthetic.main.activity_background.*
import org.jetbrains.anko.toast

class BackgroundActivity : SomeUsefulActivity() {
    lateinit var background: Background
    lateinit var projectRepo: ProjectRepo
    
    val assetRepo by lazy { AssetRepo(projectRepo) }
    
    val projectName by lazy { intent.getStringExtra("projectName") }
    val isNew by lazy { intent.getBooleanExtra("createNew", true) }
    
    val imageFile by lazy { background.imageReq.builtImageReq.getFile(assetRepo.assetsDir) }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_background)
        projectRepo = ProjectRepo(repo, projectName)
        
        background = if (isNew) {
            assetRepo.createBackground()
        } else {
            val assetId = intent.getLongExtra("assetId", -1)
            check(assetId != -1L) { "Not received assetId in background activity." }
            assetRepo.findBackgroundById(assetId)
        }
        
        tvName.text = background.name
        tvName.setUnderline()
        tvName.setOnClickListener {
            enterNameAlert("Enter asset name:", background.name) { newName ->
                if (!assetRepo.isAssetNameUnique(newName)) {
                    toast("Name $newName is already used.")
                    false
                } else {
                    background.transaction { name = newName }
                    tvName.text = newName
                    true
                }
            }
        }
        btnDrawNew.setOnClickListener {
            showPictureSizeDialog(this) { width, height, bitmap ->
                if (bitmap != null) {
                    assetRepo.transaction {
                        initLayeredImage(
                            background.imageReq,
                            bitmap.width,
                            bitmap.height,
                            predefinedBitmap = bitmap
                        )
                    }
                } else {
                    assetRepo.transaction { initLayeredImage(background.imageReq, width, height) }
                }
                background.drawConfigReq.transaction { backgroundColor = Color.WHITE }
                openImageEditor()
            }
        }
        
        ivAsset.setOnClickListener {
            openImageEditor()
        }
        btnBack.setOnClickListener {
            finish()
        }
        
        // load initial
        if (imageFile.exists()) {
            val bitmap = BitmapFactory.decodeFile(imageFile.absolutePath)
            updateImageView(bitmap)
        } else {
            ivAsset.setGone()
            btnDrawNew.setVisible()
        }
    }
    
    
    fun openImageEditor() {
        activityHelper.startForResult<DrawingActivity>(
            "drawingSetup" to DrawingSetup(
                background.imageReq.id,
                projectName,
                background.drawConfigReq.id,
                background.name
            )
        ) { _, intent ->
            val bitmap = BitmapFactory.decodeFile(imageFile.absolutePath)
            updateImageView(bitmap)
            
            passResultMark("updateSpines", intent)
        }
    }
    
    private fun updateImageView(bitmap: Bitmap) {
        ivAsset.setImageBitmap(bitmap)
        ivAsset.setVisible()
        btnDrawNew.setGone()
    }
    
    override fun finish() {
        if (isNew) {
            val data = Intent()
            data.putExtra("createdId", background.id)
            setResult(Activity.RESULT_OK, data)
        }
        super.finish()
    }
    
    override fun onDestroy() {
        projectRepo.close()
        super.onDestroy()
    }
}