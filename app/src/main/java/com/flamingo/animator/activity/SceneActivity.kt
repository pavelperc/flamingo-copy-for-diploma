package com.flamingo.animator.activity

import android.os.Bundle
import com.flamingo.animator.R
import com.flamingo.animator.editors.sceneEditor.SceneSurfaceView
import com.flamingo.animator.model.Scene
import com.flamingo.animator.repository.AssetRepo
import com.flamingo.animator.repository.ProjectRepo
import com.flamingo.animator.repository.SceneRepo
import com.flamingo.animator.utils.SomeUsefulActivity
import kotlinx.android.synthetic.main.activity_scene.*

class SceneActivity : SomeUsefulActivity() {
    
    lateinit var projectRepo: ProjectRepo
    val assetRepo by lazy { AssetRepo(projectRepo) }
    val sceneRepo by lazy { SceneRepo(projectRepo) }
    
    lateinit var scene: Scene
    
    lateinit var surfaceView: SceneSurfaceView
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scene)
        supportActionBar?.hide()
        projectRepo = ProjectRepo(repo, intent.getStringExtra("projectName"))
        
        scene = sceneRepo.findSceneById(intent.getLongExtra("sceneId", 0))
        
        surfaceView = SceneSurfaceView(this)
        llCanvas.addView(surfaceView)
        
        tvName.text = scene.name
        
        tvScale.setOnLongClickListener {
            surfaceView.resetScale()
            true
        }
    }
    
    // ----- Functions to connect with SurfaceView
    
    fun updateScaleText(scale: Float) {
        tvScale.text = String.format("%.2f", scale)
    }
    
}
