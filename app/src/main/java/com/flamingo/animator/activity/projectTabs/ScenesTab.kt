package com.flamingo.animator.activity.projectTabs

import android.content.DialogInterface
import androidx.recyclerview.widget.LinearLayoutManager
import com.flamingo.animator.activity.ProjectActivity
import com.flamingo.animator.adapters.PreviewListAdapter
import com.flamingo.animator.adapters.SceneListAdapter
import com.flamingo.animator.model.Background
import com.flamingo.animator.utils.commonUtils.setGone
import com.flamingo.animator.utils.commonUtils.setVisible
import kotlinx.android.synthetic.main.scenes_tab.*
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView


class ScenesTab(val activity: ProjectActivity) : ProjectActivityTab {
    override fun setGone() {
        activity.llScenes.setGone()
    }
    
    override fun setVisible() {
        activity.llScenes.setVisible()
        refreshAllScenes()
    }
    
    private val sceneListAdapter = SceneListAdapter(this)
    
    private fun refreshAllScenes() {
        sceneListAdapter.scenes.clear()
        // add in addition reversed order.
        sceneListAdapter.scenes.addAll(activity.sceneRepo.findAllScenes().reversed())
        sceneListAdapter.notifyDataSetChanged()
    }
    
    
    init {
        with(activity) {
            rvScenes.layoutManager = LinearLayoutManager(this)
            rvScenes.adapter = sceneListAdapter
            
            refreshAllScenes()
            
            fabAddScene.setOnClickListener {
                selectBackgroundFromList { background ->
                    toast("selected bg ${background.name}")
                    val scene = sceneRepo.transaction {
                        // scene name is equal to background
                        initScene(background)
                    }
                    sceneListAdapter.scenes.add(0, scene)
                    sceneListAdapter.notifyItemInserted(0)
                }
            }
        }
    }
    
    
    private fun selectBackgroundFromList(onSuccess: (background: Background) -> Unit) {
        with(activity) {
            
            var dialog: DialogInterface? = null
            dialog = alert {
                title = "Select background"
                customView {
                    val backgrounds = assetRepo.getAllBackgrounds().toList()
                    val filesWithNames = backgrounds.map { bg ->
                        val file = bg.imageReq.builtImageReq.getFile(assetRepo.assetsDir)
                        bg.name to file
                    }
                    
                    recyclerView {
                        topPadding = dip(8)
                        adapter = PreviewListAdapter(filesWithNames) { selectedPos ->
                            onSuccess(backgrounds[selectedPos])
                            dialog?.dismiss()
                        }
                        layoutManager = LinearLayoutManager(activity)
                    }
                }
            }.show()
        }
    }
    
}