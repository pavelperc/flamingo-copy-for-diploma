package com.flamingo.animator.activity.spine

import android.content.res.Configuration
import android.graphics.Bitmap
import android.os.Bundle
import android.widget.PopupMenu
import android.widget.ToggleButton
import com.flamingo.animator.R
import com.flamingo.animator.adapters.spine.AnimationListSpinnerAdapter
import com.flamingo.animator.adapters.spine.TimelineAdapter
import com.flamingo.animator.dialog.showExportGifDialog
import com.flamingo.animator.editors.spineEditor.SpineEditorConfig
import com.flamingo.animator.editors.spineEditor.SpineSurfaceView
import com.flamingo.animator.editors.spineEditor.dataHolders.SlotContainer
import com.flamingo.animator.editors.spineEditor.dataHolders.SpineAnimationContainer
import com.flamingo.animator.editors.spineEditor.tools.BoneToolType
import com.flamingo.animator.editors.spineEditor.tools.SlotToolType
import com.flamingo.animator.model.spine.Spine
import com.flamingo.animator.repository.AssetRepo
import com.flamingo.animator.repository.ProjectRepo
import com.flamingo.animator.utils.SomeUsefulActivity
import com.flamingo.animator.utils.commonUtils.onShowing
import com.flamingo.animator.utils.commonUtils.safeJson
import com.flamingo.animator.utils.commonUtils.setGone
import com.flamingo.animator.utils.commonUtils.setVisible
import com.flamingo.animator.utils.dialogUtils.enterNameAlert
import com.flamingo.animator.utils.dialogUtils.fastAlert
import com.flamingo.animator.utils.importUtils.importMultipleImages
import com.flamingo.animator.utils.realmUtils.transaction
import kotlinx.android.synthetic.main.activity_spine_editor.*
import kotlinx.android.synthetic.main.toolbar_spine_animation.*
import kotlinx.android.synthetic.main.toolbar_spine_bones.*
import kotlinx.android.synthetic.main.toolbar_spine_slots.*
import org.jetbrains.anko.configuration
import org.jetbrains.anko.dip
import org.jetbrains.anko.toast


/** Skin attachments for skeletal animation. Draw bones. Make animations. */
class SpineEditorActivity : SomeUsefulActivity() {
    
    lateinit var projectRepo: ProjectRepo
    val assetRepo by lazy { AssetRepo(projectRepo) }
    val spineRepo get() = assetRepo.spineRepo
    
    lateinit var spine: Spine
    lateinit var surfaceView: SpineSurfaceView
    
    lateinit var popupMenu: PopupMenu
    
    val timelineAdapter by lazy { TimelineAdapter(this) }
    
    private var shouldSave = true
    
    val spineDrawConfig by lazy {
        SpineEditorConfig(spine.riggingDrawConfigJson.safeJson())
    }
    
    /** In bones or in slots editor mode. */
    var inBonesMode = false
        set(value) {
            field = value
            if (value) {
                tvPrevMode.text = "slots"
                tvNextMode.text = "anim"
                btnPrevMode.isEnabled = true
                btnNextMode.isEnabled = true
            } else {
                tvPrevMode.text = ""
                tvNextMode.text = "bones"
                btnPrevMode.isEnabled = false
                btnNextMode.isEnabled = true
            }
            tbBoneTools.setVisible(value)
            tbSlotTools.setVisible(!value)
            surfaceView.updateScreen()
        }
    
    /** Switcher between animation and pose setup modes. */
    var inAnimationMode = false
        set(value) {
            field = value
            if (value) {
                tvPrevMode.text = "bones"
                tvNextMode.text = ""
                btnPrevMode.isEnabled = true
                btnNextMode.isEnabled = false
            } else {
                tvPrevMode.text = "slots"
                tvNextMode.text = "anim"
                btnPrevMode.isEnabled = true
                btnNextMode.isEnabled = true
            }
            enableAnimation(value)
            
            llTimelinePanel.setVisible(value)
            btnPlayAnim.setVisible(value)
            
            llAnimHeader.setVisible(value)
            btnExportGif.setVisible(value)
            llPoseHeader.setVisible(!value)
            
            llPoseSetupTools.setVisible(!value)
            tbAnimTools.setVisible(value)
        }
    
    var isAnimationPlaying = false
        set(value) {
            if (field == value) return
            field = value
            btnPlayAnim.isActivated = value
            llTimelinePanel.setVisible(!value)
            animationToolButtons.forEach { it.isEnabled = !value }
            
            if (value) {
                surfaceView.startPlayingAnimation()
            } else {
                onBoneSelectionChanged()
            }
        }
    
    private val slotSelectionButtons by lazy {
        listOf(btnSlotToBack, btnSlotToFront)
    }
    
    // rich slot means has 2 or more attachments
    private val richSlotButtons by lazy {
        listOf(btnDismantleSlot, btnSwapAttachment)
    }
    
    private val boneSelectionButtons by lazy {
        listOf(btnDeleteBone, btnDisableSubtree, btnDisableSubtreeAnim)
    }
    
    private val boneWithRichSlotButtons by lazy {
        listOf(btnSwapAttachmentBones, btnSwapAttachmentAnim)
    }
    
    
    val animationToolButtons by lazy {
        listOf(
            btnMoveAnimationPose, btnSwapAttachmentAnim, btnDisableSubtreeAnim,
            btnClearTimestamp, btnZeroTimestamp, btnRepeatTimestamp
        )
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spine_editor)
        
        projectRepo = ProjectRepo(repo, intent.getStringExtra("projectName"))
        
        spine = assetRepo.findSpineById(intent.getLongExtra("spineId", -1))
        
        spineRepo.updateSpineFromDrawingEditor(spine)
        
        surfaceView = SpineSurfaceView(this)
        llCanvas.addView(surfaceView)
        
        if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            reattachBottomAppBar(true)
        }
        
        tvScale.setOnLongClickListener {
            surfaceView.resetScale()
            true
        }
        
        popupMenu = PopupMenu(this, btnOptions)
        popupMenu.menuInflater.inflate(R.menu.spine_editor_activity_menu, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener(onMenuItemClickListener)
        btnOptions.setOnClickListener {
            popupMenu.show()
        }
        
        setupModeButtons()
        
        setupSlotTools()
        
        setupBoneTools()
        setupAnimTools()
        
        setupTimeline()
        
        setupAnimList()
        
        // !inBonesMode && inAnimationMode is invalid state, fix it (may come from older versions)
        if (spineDrawConfig.inAnimationMode) {
            inBonesMode = true
            inAnimationMode = true
        } else {
            inAnimationMode = false
            inBonesMode = spineDrawConfig.inBonesMode
        }
    }
    
    private fun setupModeButtons() {
        llNextMode.setOnClickListener {
            if (inBonesMode) {
                // to anim
                if (surfaceView.bonesContainer.root == null) {
                    toast("Create at least one bone first.")
                    return@setOnClickListener
                }
                if (surfaceView.bonesContainer.traverseBones().all { it.slotContainer == null }) {
                    toast("Attach at least one bone to image first.")
                    return@setOnClickListener
                }
                inAnimationMode = true
            } else {
                // to bones
                if (surfaceView.spineDataHolder.slotContainers.isEmpty()) {
                    toast("Import or draw at least one image first.")
                    return@setOnClickListener
                }
                inBonesMode = true
            }
        }
        llPrevMode.setOnClickListener {
            if (inAnimationMode) {
                // to bones
                inAnimationMode = false
            } else {
                // to slots
                inBonesMode = false
            }
        }
    }
    
    private fun setupSlotTools() {
        onSlotSelectionChanged()
        
        btnAddSlot.setOnClickListener {
            addSlot()
        }
        
        btnSlotToBack.setOnClickListener {
            surfaceView.moveSelectedSlotToBack()
        }
        btnSlotToFront.setOnClickListener {
            surfaceView.moveSelectedSlotToFront()
        }
        
        btnSwapAttachment.setOnClickListener {
            surfaceView.swapAttachmentForSelectedSlot()
        }
        
        btnDismantleSlot.setOnClickListener {
            fastAlert("Dismantle images, combined together?") {
                surfaceView.dismantleSelectedSlot()
            }
        }
        
        
        val toolButtons = listOf(btnMoveAttachment, btnMergeSlots)
        
        fun toggleToolButton(selectedButton: ToggleButton) {
            toolButtons.forEach { it.isChecked = it == selectedButton }
            surfaceView.slotToolsContainer.selectedToolType = when (selectedButton) {
                btnMoveAttachment -> SlotToolType.TRANSFORM_TOOL
                btnMergeSlots -> SlotToolType.SLOT_MERGE_TOOL
                else -> throw IllegalStateException("Unknown slot tool")
            }
        }
        
        toolButtons.forEach { btn -> btn.setOnClickListener { toggleToolButton(btn) } }
        
        when (spineDrawConfig.selectedSlotTool) {
            SlotToolType.TRANSFORM_TOOL -> toggleToolButton(btnMoveAttachment)
            SlotToolType.SLOT_MERGE_TOOL -> toggleToolButton(btnMergeSlots)
        }
    }
    
    private fun setupBoneTools() {
        onBoneSelectionChanged()
        btnDeleteBone.setOnClickListener {
            deleteSelectedBone()
        }
        btnSwapAttachmentBones.setOnClickListener {
            surfaceView.swapAttachmentForSelectedBone()
        }
        btnDisableSubtree.setOnClickListener {
            surfaceView.disableOrEnableSubtreeForSelectedBone()
        }
        
        val toolButtons = listOf(btnDrawBones, btnMoveOneBone, btnMoveBoneTree)
        
        fun toggleToolButton(selectedButton: ToggleButton) {
            toolButtons.forEach { it.isChecked = it == selectedButton }
            surfaceView.boneToolsContainer.selectedToolType = when (selectedButton) {
                btnDrawBones -> BoneToolType.DRAW_BONE
                btnMoveOneBone -> BoneToolType.MOVE_ONE_BONE
                btnMoveBoneTree -> BoneToolType.MOVE_BONE_TREE
                else -> throw IllegalStateException("Unknown bone tool")
            }
        }
        toolButtons.forEach { btn -> btn.setOnClickListener { toggleToolButton(btn) } }
        
        when (spineDrawConfig.selectedBoneTool) {
            BoneToolType.DRAW_BONE -> toggleToolButton(btnDrawBones)
            BoneToolType.MOVE_ONE_BONE -> toggleToolButton(btnMoveOneBone)
            BoneToolType.MOVE_BONE_TREE -> toggleToolButton(btnMoveBoneTree)
        }
    }
    
    private fun deleteSelectedBone() {
        val selectedBone = surfaceView.bonesContainer.selectedBone ?: return
        val childrenCount = selectedBone.childrenRecursive.size
        
        if (childrenCount > 0 || selectedBone.slotContainer != null) {
            val message =
                when {
                    childrenCount > 1 -> "Selected bone has $childrenCount children."
                    childrenCount == 1 -> "Selected bone has $childrenCount child."
                    else -> "Selected bone has attached image."
                }
            fastAlert(message, "Delete bone?") {
                surfaceView.deleteSelectedBone()
            }
        } else {
            surfaceView.deleteSelectedBone()
        }
    }
    
    private fun setupAnimTools() {
        btnMoveAnimationPose.isChecked = true
        btnMoveAnimationPose.setOnClickListener {
            btnMoveAnimationPose.isChecked = true
        }
        
        btnClearTimestamp.setOnClickListener {
            surfaceView.clearCurrentTimestamp()
            timelineAdapter.updateItemOnTime(surfaceView.animationDataHolder.animationTime)
        }
        btnZeroTimestamp.setOnClickListener {
            surfaceView.addZeroTimestamp()
            timelineAdapter.updateItemOnTime(surfaceView.animationDataHolder.animationTime)
        }
        btnRepeatTimestamp.setOnClickListener {
            surfaceView.repeatTimestamp()
            timelineAdapter.updateItemOnTime(surfaceView.animationDataHolder.animationTime)
        }
        btnSwapAttachmentAnim.setOnClickListener {
            surfaceView.swapAttachmentOnTimeline()
            timelineAdapter.updateItemOnTime(surfaceView.animationDataHolder.animationTime)
        }
        btnDisableSubtreeAnim.setOnClickListener {
            surfaceView.disableOrEnableSubtreeOnTimeline()
            timelineAdapter.updateItemOnTime(surfaceView.animationDataHolder.animationTime)
        }
        
        btnExportGif.setOnClickListener {
            exportGif()
        }
    }
    
    private fun addSlot() {
        importMultipleImages { bitmapSequence ->
            val bitmaps = bitmapSequence.iterator()
            if (!bitmaps.hasNext()) {
                toast("No images selected")
                return@importMultipleImages
            }
            var size = 0
            while (bitmaps.hasNext()) {
                val bitmap = bitmaps.next()
                val slot = spineRepo.createSlotFromBitmap(spine, bitmap)
                spine.transaction {
                    slots.add(slot)
                }
                size++
                surfaceView.spineDataHolder.slotContainers +=
                    SlotContainer(slot, surfaceView.spineDataHolder, listOf(bitmap))
            }
            
            surfaceView.updateScreen()
            toast("Loaded images: $size")
        }
    }
    
    private val onMenuItemClickListener = PopupMenu.OnMenuItemClickListener { menuItem ->
        when (menuItem.itemId) {
            R.id.itemCropAll -> {
                fastAlert("Crop all images?") {
                    surfaceView.cropAllSlots(forceCropped = true)
                }
            }
            R.id.itemQuitWithoutSaving -> {
                fastAlert(
                    "Quit without saving doesn't work for adding, " +
                            "deleting images and animations, and merging or dismantling slots. " +
                            "Your data may become being in an inconsistent state.", "Caution"
                ) {
                    shouldSave = false
                    finish()
                }
            }
            R.id.itemResetIgonredLayers -> {
                spine.transaction {
                    ignoredUncroppedLayersForImport.clear()
                }
                toast("Done!")
            }
        }
        true
    }
    
    private fun exportConfigs() {
        surfaceView.exportCameraState(spineDrawConfig)
        spineDrawConfig.inBonesMode = inBonesMode
        spineDrawConfig.inAnimationMode = inAnimationMode
        spineDrawConfig.selectedBoneTool = surfaceView.boneToolsContainer.selectedToolType
        spineDrawConfig.selectedSlotTool = surfaceView.slotToolsContainer.selectedToolType
        
        spine.transaction { riggingDrawConfigJson = spineDrawConfig.toJson().toString() }
    }
    
    // this method is called again on orientation change
    private fun setupTimeline() {
        val pointerMargin = dip(50)
        
        rvTimeline.adapter = timelineAdapter
        rvTimeline.layoutManager = timelineAdapter.layoutManager
        timelineAdapter.snapHelper.attachToRecyclerView(rvTimeline)
        
        rvTimeline.addOnScrollListener(timelineAdapter.MyScrollListener())
        
        rvTimeline.addOnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
            val width = right - left
            val oldWidth = oldRight - oldLeft
            if (width == oldWidth) {
                return@addOnLayoutChangeListener
            }
            rvTimeline.clipToPadding = false
            rvTimeline.setPaddingRelative(
                pointerMargin, rvTimeline.paddingTop,
                rvTimeline.width - pointerMargin - 1, rvTimeline.paddingBottom
            )
            // only instant scroll not works for some reason...
            rvTimeline.scrollToPosition(0)
            rvTimeline.onShowing {
                rvTimeline.scrollToPosition(0)
            }
        }
        
        btnPlayAnim.setOnClickListener {
            isAnimationPlaying = !isAnimationPlaying
        }
    }
    
    private fun setupAnimList() {
        val adapter = AnimationListSpinnerAdapter(this)
        spnAnimations.adapter = adapter
        spnAnimations.onItemSelectedListener = adapter.onItemSelectedListener
        spnAnimations.setOnLongClickListener {
            val animContainer = spnAnimations.selectedItem as? SpineAnimationContainer
                ?: return@setOnLongClickListener true
            val anim = animContainer.spineAnimation
            
            enterNameAlert("Enter animation name", anim.name) { newName ->
                val existingNames = spine.animations.map { it.name }
                if (newName in existingNames) {
                    toast("Name $newName already exists.")
                    return@enterNameAlert false
                }
                anim.transaction { name = newName }
                adapter.notifyDataSetChanged()
                true
            }
            true
        }
    }
    
    override fun onStop() {
        if (shouldSave) {
            surfaceView.saveAll()
        }
        exportConfigs()
        isAnimationPlaying = false
        super.onStop()
    }
    
    // we use finish, because it is called before onActivityResult, unlike onStop.
    override fun finish() {
        if (shouldSave) {
            updatePreview()
            surfaceView.saveAll()
        }
        shouldSave = false // don't save in onStop.
        super.finish()
    }
    
    override fun onDestroy() {
        projectRepo.close()
        super.onDestroy()
    }
    
    private fun updatePreview() {
        val previewFile = spine.previewReq.getFile(assetRepo.assetsDir)
        val bitmap =
            surfaceView.slotDrawer.createPreview(surfaceView.spineDataHolder.visibleSlotContainers.toList())
        
        if (bitmap == null) {
            previewFile.delete()
        } else {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, previewFile.outputStream())
        }
    }
    
    private fun enableAnimation(enabled: Boolean) {
        isAnimationPlaying = false
        if (enabled) {
            if (surfaceView.animationDataHolder.clearInvalidTimestamps()) {
                timelineAdapter.notifyDataSetChanged()
            }
        }
        surfaceView.enableAnimation(enabled)
    }
    
    fun updateScaleText(scale: Float) {
        tvScale.text = String.format("%.2f", scale)
    }
    
    fun onSlotSelectionChanged() {
        val slot = surfaceView.spineDataHolder.selectedSlot
        val attachmentCount = slot?.attachmentContainers?.size ?: 0
        slotSelectionButtons.forEach { button ->
            button.isEnabled = slot != null
        }
        richSlotButtons.forEach { button ->
            button.isEnabled = attachmentCount > 1
        }
    }
    
    fun onBoneSelectionChanged() {
        val selectedBone = surfaceView.bonesContainer.selectedBone
        boneSelectionButtons.forEach { it.isEnabled = selectedBone != null }
        
        val attachmentCount = selectedBone?.slotContainer?.attachmentContainers?.size ?: 0
        boneWithRichSlotButtons.forEach { button ->
            button.isEnabled = attachmentCount > 1
        }
    }
    
    /** time in millis */
    fun onAnimationTimeChanged(time: Int) {
        tvAnimationTime.text = "${time}ms"
        surfaceView.changeAnimationTimeAndUpdate(time)
    }
    
    fun onAnimationSwapped() {
        isAnimationPlaying = false
        timelineAdapter.notifyDataSetChanged()
        surfaceView.animationDataHolder.updateEditedBonesToHighlight()
        surfaceView.updateAnimation()
    }
    
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            reattachBottomAppBar(true)
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            reattachBottomAppBar(false)
        }
    }
    
    private fun reattachBottomAppBar(horizontal: Boolean) {
        if (horizontal) {
            appBarLayoutDown.setGone()
            vShadowBottom.setGone()
            appBarLayoutDown.removeView(llDownContent)
            llUpperAdditional.addView(llDownContent)
        } else {
            llUpperAdditional.removeView(llDownContent)
            appBarLayoutDown.addView(llDownContent)
            appBarLayoutDown.setVisible()
            vShadowBottom.setVisible()
        }
    }
    
    private fun exportGif() {
        val animationContainer = surfaceView.animationDataHolder.currentAnimationContainer ?: return
        
        val suggestedName = "${spine.name}_${animationContainer.spineAnimation.name}"
        
        val file = assetRepo.repo.cacheDir.resolve("temp.gif")
        surfaceView.slotDrawer.createGif(file) {
            showExportGifDialog(file, suggestedName)
        }
    }
}
