package com.flamingo.animator.activity

import android.os.Bundle
import com.codemonkeylabs.fpslibrary.TinyDancer
import com.flamingo.animator.BuildConfig
import com.flamingo.animator.R
import com.flamingo.animator.activity.spine.SpineInfoActivity
import com.flamingo.animator.adapters.OnlySpineListAdapter
import com.flamingo.animator.export.flampack.FlampackExport
import com.flamingo.animator.export.flampack.importFlampackDialog
import com.flamingo.animator.export.flampack.showExportFlampackDialog
import com.flamingo.animator.preferences.Preferences
import com.flamingo.animator.repository.AssetRepo
import com.flamingo.animator.repository.ProjectRepo
import com.flamingo.animator.utils.SomeUsefulActivity
import com.flamingo.animator.utils.commonUtils.setVisible
import com.flamingo.animator.utils.commonUtils.toList
import com.flamingo.animator.utils.commonUtils.uniqueName
import com.flamingo.animator.utils.dialogUtils.enterNameAlert
import com.flamingo.animator.utils.removeAllUnknownFiles
import kotlinx.android.synthetic.main.activity_spines_main.*
import org.jetbrains.anko.*

class SpinesMainActivity : SomeUsefulActivity() {
    companion object {
        /** Folder name for all spines in internal storage. */
        const val SPINE_PROJECT_NAME = "SPINE_PROJECT"
    }
    
    val projectRepo by lazy { ProjectRepo(repo, SPINE_PROJECT_NAME) }
    val assetRepo by lazy { AssetRepo(projectRepo) }
    
    val spinesAdapter by lazy { OnlySpineListAdapter(this) }
    
    val preferences by lazy {
        Preferences(this)
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spines_main)
        TinyDancer.create().show(this) // debug fps
        if (BuildConfig.DEBUG) {
            removeAllUnknownFiles(assetRepo)
        }
        
        rvSpines.adapter = spinesAdapter
        
        loadExamplesFlampack()
        updateSpinesContent()
        enableSelectionMode(false)
        
        fabCreateSpine.setOnClickListener { createSpine() }
        
        btnDeleteSelected.setOnClickListener {
            deleteSelectedSpines()
        }
        
        btnExportSelected.setOnClickListener {
            exportSelectedSpines()
        }
        
        btnSelectAll.setOnClickListener {
            val selectedSpines = spinesAdapter.selectedSpines
            
            if (selectedSpines.size == spinesAdapter.spines.size()) {
                selectedSpines.clear()
            } else {
                selectedSpines.addAll(spinesAdapter.spines.toList())
            }
            spinesAdapter.notifyDataSetChanged()
        }
        
        btnImportFlampack.setOnClickListener {
            importFlampackDialog(assetRepo, onlySpines = true) {
                updateSpinesContent()
            }
        }
        btnAbout.setOnClickListener {
            alert {
                val version = packageManager.getPackageInfo(packageName, 0).versionName
                title = "About"
                message = "Made by Pavel Pertsukhov.\n\n" +
                        "Version $version\n\n" +
                        "Feel free to share your flampacks!\n" +
                        "(Long click to select a model for share.)"
                positiveButton("Done") { dialog ->
                    dialog.dismiss()
                }
            }.show()
        }
        
        btnAbout.setOnLongClickListener {
            // old menu
            startActivity<MainActivity>()
            true
        }
    }
    
    fun enableSelectionMode(enabled: Boolean) {
        llHeaderSpinesSelection.setVisible(enabled)
        llHeaderCommon.setVisible(!enabled)
        
        spinesAdapter.enableSelectionMode(enabled)
    }
    
    private fun updateSpinesContent() {
        spinesAdapter.spines.clear()
        spinesAdapter.spines.addAll(assetRepo.getAllSpines())
    }
    
    private fun createSpine() {
        val spineNames = spinesAdapter.spines.toList().map { it.name }
        val uniqueName = uniqueName(spineNames, "spine")
        enterNameAlert("Enter spine name:", uniqueName) { newName ->
            
            if (!assetRepo.isAssetNameUnique(newName)) {
                toast("Name $newName is already used.")
                false
            } else {
                val spine = assetRepo.createSpineObject(newName)
                val position = spinesAdapter.spines.add(spine)
                
                activityHelper.startForResult<SpineInfoActivity>(
                    "projectName" to SPINE_PROJECT_NAME,
                    "assetId" to spine.id
                ) { _, _ ->
                    spinesAdapter.updateSpine(position)
                }
                true
            }
        }
    }
    
    private fun deleteSelectedSpines() {
        val selectedSpines = spinesAdapter.selectedSpines
        val size = selectedSpines.size
        if (size == 0) {
            return
        }
        
        alert {
            message = if (size > 1) "Delete $size spines?" else "Delete $size spine?"
            cancelButton { }
            yesButton {
                // search in list before removing from db to avoid accessing deleted fields!!
                selectedSpines.forEach { spine ->
                    spinesAdapter.spines.remove(spine)
                    assetRepo.removeSpine(spine)
                }
                // also we delete all invalid old selectedAssets from adapters in enableSelectionMode function
                // (it is important!!)
                enableSelectionMode(false)
            }
        }.show()
    }
    
    private fun exportSelectedSpines() {
        val selectedSpines = spinesAdapter.selectedSpines
        val size = selectedSpines.size
        if (size == 0) {
            return
        }
        val suggestedName = if (size == 1) selectedSpines.first().name else "Spines"
        
        showExportFlampackDialog(
            assetRepo, suggestedName,
            emptyList(), emptyList(),
            selectedSpines.toList()
        ) {
            enableSelectionMode(false)
        }
    }
    
    override fun onDestroy() {
        projectRepo.close()
        super.onDestroy()
    }
    
    override fun onBackPressed() {
        if (spinesAdapter.selectionEnabled) {
            enableSelectionMode(false)
            return
        }
        super.onBackPressed()
    }
    
    
    private fun loadExamplesFlampack() {
        val loadedVersion = preferences.loadedExamplesFlampackName
        val currentFlampackName = "examples_v1_2"
        if (loadedVersion != currentFlampackName) {
            val inputStream = resources.openRawResource(R.raw.examples_v1_2)
            FlampackExport.importResourcePack(
                inputStream, assetRepo, onlySpines = false, skipExisting = true
            )
            preferences.loadedExamplesFlampackName = currentFlampackName
        }
    }
}
