package com.flamingo.animator.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.PopupMenu
import android.widget.SeekBar
import android.widget.TextView
import android.widget.ToggleButton
import com.flamingo.animator.R
import com.flamingo.animator.activity.layerBottomSheetHelper.LayerBottomSheetHelper
import com.flamingo.animator.activity.spine.SpineEditorActivity
import com.flamingo.animator.dialog.AnimSettingsDialog
import com.flamingo.animator.editors.drawUtils.drawBitmap
import com.flamingo.animator.editors.drawUtils.imageCropper.ImageCropper
import com.flamingo.animator.editors.drawUtils.setRippleBackground
import com.flamingo.animator.editors.drawingEditor.BrushType
import com.flamingo.animator.editors.drawingEditor.DrawingSurfaceView
import com.flamingo.animator.model.DrawConfig
import com.flamingo.animator.model.spine.Spine
import com.flamingo.animator.repository.AssetRepo
import com.flamingo.animator.repository.ProjectRepo
import com.flamingo.animator.utils.SomeUsefulActivity
import com.flamingo.animator.utils.commonUtils.*
import com.flamingo.animator.utils.dialogUtils.fastAlert
import kotlinx.android.synthetic.main.activity_drawing.*
import kotlinx.android.synthetic.main.toolbar_drawing_anim.*
import kotlinx.android.synthetic.main.toolbar_drawing_spine.*
import org.jetbrains.anko.*
import java.io.Serializable
import kotlin.math.max

/** Info from the previous activity. */
class DrawingSetup(
    /** Id of layered image. May be invalid (0), if animationId is valid. */
    val layeredImageId: Long,
    val projectName: String,
    /** Id of config record in database */
    val drawConfigId: Long,
    /** Is used in spine export. */
    val imageName: String,
    /** 0 means, that it is not animation */
    val animationId: Long = 0L,
    /** Spine id. 0 means, that it is not spine. */
    val spineId: Long = 0L
) : Serializable {
    val isAnimation: Boolean
        get() = animationId != 0L
    val isSpine: Boolean
        get() = spineId != 0L
}


/** Handles UI Actions for drawing. Main drawing logic is in [DrawingSurfaceView]. */
class DrawingActivity : SomeUsefulActivity() {
    val drawingSetup by lazy { intent.getSerializableExtra("drawingSetup") as DrawingSetup }
    lateinit var surfaceView: DrawingSurfaceView
    
    lateinit var popupMenu: PopupMenu
    
    /** It is false, when we click quit without save. */
    private var shouldSave = true
    
    lateinit var projectRepo: ProjectRepo
    val assetRepo by lazy { AssetRepo(projectRepo) }
    lateinit var drawConfig: DrawConfig
    
    override fun onDestroy() {
        projectRepo.close()
        super.onDestroy()
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        
        supportActionBar?.hide()
        setContentView(R.layout.activity_drawing)
        
        projectRepo = ProjectRepo(repo, drawingSetup.projectName)
        
        drawConfig = assetRepo.findDrawConfigById(drawingSetup.drawConfigId)
        
        setupMenu()
        
        btnOptions.setOnClickListener {
            popupMenu.show()
        }
        
        
        surfaceView = DrawingSurfaceView(this, drawingSetup, drawConfig)
        
        
        llCanvas.addView(surfaceView)
        
        btnUndo.setOnClickListener {
            surfaceView.undo()
        }
        btnRedo.setOnClickListener {
            surfaceView.redo()
        }
        
        tvScale.setOnLongClickListener {
            surfaceView.resetScale()
            true
        }
        
        setupBrushes()
        
        // setup color dialog
        
        btnColor.setOnClickListener {
            try {
                surfaceView.colorPicker.show(supportFragmentManager, "dlg_color")
            } catch (e: IllegalStateException) {
                // it is thrown if the fragment is already added. (two fast clicks on the button)
            }
        }
        btnColorMini.setOnClickListener {
            surfaceView.setColor(btnColorMini.color)
        }
        
        
        if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            reattachBottomAppBar(true)
        }
        
        surfaceView.onStateChanged = {
            btnUndo.isEnabled = surfaceView.currentRestorableCanvas.undoSize > 0
            btnRedo.isEnabled = surfaceView.currentRestorableCanvas.redoSize > 0
            tvLayerNum.text =
                surfaceView.currentImageContainer.selectedLayerPosition.plus(1).toString()
        }
        
        // setup anim and layers
        tvLayerNum.text =
            surfaceView.currentImageContainer.selectedLayerPosition.plus(1).toString()
        setupLayersList()
        
        tbSpine.setVisible(drawingSetup.isSpine)
        btnOpenInSpine.setOnClickListener {
            if (!drawingSetup.isSpine) {
                return@setOnClickListener
            }
            finish()
            startActivity<SpineEditorActivity>(
                "projectName" to drawingSetup.projectName,
                "spineId" to drawingSetup.spineId
            )
        }
        
        if (!drawingSetup.isAnimation) {
            tbAnim.setGone()
        } else {
            tbAnim.setVisible()
            
            val animContainer = surfaceView.drawDataHandler.animContainer!!
            
            btnAddFrame.setOnClickListener {
                alert("Duplicate current frame?") {
                    negativeButton("No") {
                        surfaceView.addFrame(false)
                    }
                    positiveButton("Yes") {
                        surfaceView.addFrame(true)
                    }
                }.show()
                
            }
            btnNextFrame.setOnClickListener { surfaceView.nextFrame() }
            btnPrevFrame.setOnClickListener { surfaceView.prevFrame() }
            btnPlayAnim.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    surfaceView.startAnim()
                } else {
                    surfaceView.stopAnim()
                }
            }
            btnDeleteFrame.setOnClickListener {
                if (animContainer.size <= 1) {
                    return@setOnClickListener
                }
                if (surfaceView.isAnimPlaying) {
                    return@setOnClickListener
                }
                
                fastAlert("Delete this frame?") {
                    surfaceView.deleteCurrFrame()
                }
                
            }
            
            val animDialog = AnimSettingsDialog(animContainer)
            
            btnAnimSettings.setOnClickListener {
                animDialog.show(supportFragmentManager, "dlg_anim")
            }
        }
        
        btnSubmitTool.setOnClickListener {
            surfaceView.compoundToolsContainer.submitDrawer()
            setCompoundDrawerMode(false)
        }
        btnCancelTool.setOnClickListener {
            surfaceView.compoundToolsContainer.cancelDrawer()
            setCompoundDrawerMode(false)
        }
    }
    
    private fun setupMenu() {
        popupMenu = PopupMenu(this, btnOptions)
        popupMenu.menuInflater.inflate(R.menu.drawing_activity_menu, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener(onMenuItemClickListener)
        if (drawingSetup.isSpine) {
            popupMenu.menu.removeItem(R.id.itemExportLayersToSpine)
        }
    }
    
    // hideLayers is used only if enabled=true
    private fun setCompoundDrawerMode(enabled: Boolean, hideLayers: Boolean = true) {
        if (enabled) {
            flSubmitCancel.setVisible()
            llUpperMainContent.setGone()
            llDownContent.setGone()
            if (hideLayers) {
                flLayers.setGone()
            }
        } else {
            flSubmitCancel.setGone()
            llUpperMainContent.setVisible()
            llDownContent.setVisible()
            flLayers.setVisible()
        }
    }
    
    private fun setupBrushes() {
        // to prevent touching surface
        llToolPopup.setOnTouchListener { _, _ -> true }
        llToolPopup.setInvisible()
        
        val brushPaint = surfaceView.brushContainer.brushPaint
        val filledBrushPaint = surfaceView.brushContainer.filledBrushPaint
        val bucketFillPaint = surfaceView.brushContainer.bucketFillPaint
        val bucketFillDrawer = surfaceView.brushContainer.bucketFillDrawer
        val eraserDrawer = surfaceView.brushContainer.eraserDrawer
        val brushDrawer = surfaceView.brushContainer.brushDrawer
        
        // only one of them can be selected
        val brushButtons = listOf(btnBrush, btnEraser, btnFilledBrush, btnBucketFill)
        
        
        fun toggleBrushButton(brush: ToggleButton) {
            brushButtons.forEach { btn ->
                btn.isChecked = btn == brush
            }
        }
        
        // map of popup views, which, will be attached for each brush.
        // views will be placed in linear layout.
        val brushPopups = mapOf(
            btnBrush to listOf(
                checkbox(
                    "Pressure",
                    brushDrawer.usePressure
                ) { isChecked -> brushDrawer.usePressure = isChecked },
                sizeSeekBar(brushPaint),
                transparencySeekBar(brushPaint)
            ),
            btnFilledBrush to listOf(
                transparencySeekBar(filledBrushPaint)
            ),
            btnBucketFill to listOf(
                checkbox("Background eraser", bucketFillDrawer.backgroundEraser) { isChecked ->
                    bucketFillDrawer.backgroundEraser = isChecked
                },
                transparencySeekBar(bucketFillPaint),
                seekBarView(bucketFillDrawer.tolerance, "Tolerance", 0..100) { tolerance ->
                    bucketFillDrawer.tolerance = tolerance
                }
            ),
            btnEraser to listOf(
                seekBarView(eraserDrawer.eraserPadDp / 6, "Size", 1..8) { size ->
                    eraserDrawer.eraserPadDp = size * 6
                },
                checkbox("Square", eraserDrawer.isSquare) { isChecked ->
                    eraserDrawer.isSquare = isChecked
                },
                centeredButton("Clear All") { surfaceView.clearCanvas() }
            )
        )
        
        var enabledButton = btnBrush
        
        brushButtons.forEach { btn ->
            btn.setOnClickListener {
                toggleBrushButton(btn)
                
                // not already enabled
                if (enabledButton != btn) {
                    llToolPopup.setInvisible()
                    enabledButton = btn
                    when (btn) {
                        btnBrush -> surfaceView.setBrush(BrushType.BRUSH)
                        btnFilledBrush -> surfaceView.setBrush(BrushType.FILL_BRUSH)
                        btnEraser -> surfaceView.setBrush(BrushType.ERASER)
                        btnBucketFill -> surfaceView.setBrush(BrushType.BUCKET_FILL)
                    }
                } else {
                    // already enabled
                    val popups = brushPopups[btn]
                    if (popups != null) {
                        if (!llToolPopup.isVisible()) {
                            // show popups
                            llToolPopup.removeAllViews()
                            // make vertical list of popups.
                            popups.forEach { llToolPopup.addView(it) }
                            llToolPopup.setVisible()
                        } else {
                            // hide popups
                            llToolPopup.setInvisible()
                        }
                    }
                }
            }
            
            btnEraser.setOnLongClickListener {
                surfaceView.clearCanvas()
                true
            }
            
        }
    }
    
    private lateinit var layerBottomSheetHelper: LayerBottomSheetHelper
    
    private fun setupLayersList() {
        layerBottomSheetHelper = LayerBottomSheetHelper(this)
        
        btnLayer.setOnClickListener {
            if (surfaceView.isAnimPlaying) {
                return@setOnClickListener
            }
            layerBottomSheetHelper.bottomSheetDialog.show()
        }
    }
    
    
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            reattachBottomAppBar(true)
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            reattachBottomAppBar(false)
        }
    }
    
    
    private fun reattachBottomAppBar(horizontal: Boolean) {
        if (horizontal) {
            appBarLayoutDown.setGone()
            vShadowBottom.setGone()
            appBarLayoutDown.removeView(llDownContent)
            llUpperAdditional.addView(llDownContent)
        } else {
            llUpperAdditional.removeView(llDownContent)
            appBarLayoutDown.addView(llDownContent)
            appBarLayoutDown.setVisible()
            vShadowBottom.setVisible()
        }
    }
    
    // ----- Functions to connect with SurfaceView
    
    fun updateScaleText(scale: Float) {
        tvScale.text = String.format("%.2f", scale)
    }
    
    fun updateColorButton(newColor: Int) {
        btnColor.color = newColor
    }
    
    fun updateMiniColorButton(color: Int) {
        btnColorMini.color = color
    }
    
    fun onSurfaceTouched() {
        llToolPopup.setInvisible()
    }
    
    // -----
    
    private val onMenuItemClickListener = PopupMenu.OnMenuItemClickListener { menuItem ->
        when (menuItem.itemId) {
            R.id.itemNotSave -> {
                shouldSave = false
                finish()
            }
            R.id.itemSave -> {
                saveAll()
            }
            R.id.itemShiftImage -> {
                surfaceView.enterImageShiftMode()
                setCompoundDrawerMode(true, hideLayers = false)
            }
            R.id.itemExportLayersToSpine -> {
                exportLayersToSpine()
            }
        }
        true
    }
    
    private fun exportLayersToSpine() {
        // only for not spines. For spines all updates are automatic.
        if (drawingSetup.isSpine) {
            return
        }
        alert {
            title = "Export to spine"
            message = "All visible layers will be exported to spine."
            cancelButton { }
            okButton {
                saveAll()
                val allNames = assetRepo.getAllSpines().map { it.name }
                
                val spine = assetRepo.spineRepo.createSpineFromLayeredImage(
                    surfaceView.drawDataHandler.currentImageContainer.layeredImage,
                    makeNameUnique(allNames, drawingSetup.imageName),
                    drawConfig
                )
                putResultMark("updateSpines")
                finish()
                startActivity<SpineEditorActivity>(
                    "projectName" to drawingSetup.projectName,
                    "spineId" to spine.id
                )
            }
        }.show()
    }
    
    override fun onStop() {
        surfaceView.stopAnim()
        if (shouldSave) {
            saveAll()
        }
        super.onStop()
    }
    
    // we use finish, because it is called before onActivityResult, unlike onStop.
    override fun finish() {
        if (shouldSave) {
            saveAll()
        }
        shouldSave = false // don't save in onStop.
        super.finish()
    }
    
    
    override fun onBackPressed() {
        if (surfaceView.compoundToolsContainer.isCompoundToolEnabled) {
            btnCancelTool.performClick()
        } else {
            super.onBackPressed()
        }
    }
    
    private fun saveAll() {
        val savedCount = surfaceView.saveAll()
        if (drawingSetup.isSpine && savedCount > 0) {
            updateSpinePreview()
        }
    }
    
    private fun updateSpinePreview() {
        val spine = assetRepo.spineRepo.findSpineById(drawingSetup.spineId)
        val previewFile = spine.previewReq.getFile(assetRepo.assetsDir)
        val builtImage = surfaceView.drawDataHandler.currentImageContainer.builtImage
        
        val rect = ImageCropper.cropImage(builtImage, cropAlreadyCropped = true)
        if (rect == null) {
            previewFile.delete()
            return
        }
        val scale = Spine.MAX_PREVIEW_SIZE.toFloat() / max(rect.width(), rect.height())
        
        val bitmap = Bitmap.createBitmap(
            (rect.width() * scale).toInt(), (rect.height() * scale).toInt(),
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        canvas.scale(scale, scale)
        canvas.translate(-rect.left.toFloat(), -rect.top.toFloat())
        canvas.drawBitmap(builtImage)
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, previewFile.outputStream())
    }
}


// --- Fast views for popup: ---

private fun Context.transparencySeekBar(paint: Paint) =
    seekBarView(paint.alpha * 100 / 255, "Transparency") { alpha ->
        paint.alpha = alpha * 255 / 100
    }

private fun Context.sizeSeekBar(paint: Paint) =
    seekBarView(paint.strokeWidth.toInt(), "Size") { size ->
        paint.strokeWidth = size.toFloat()
    }

private fun Context.centeredButton(text: String, onClick: () -> Unit) =
    UI {
        verticalLayout {
            textView(text) {
                isClickable = true
                setOnClickListener {
                    onClick()
                }
                elevation = 15.0f
            }.lparams {
                this.gravity = Gravity.CENTER
            }
            bottomPadding = dip(8)
        }
    }.view

private fun Context.checkbox(
    text: String,
    checked: Boolean,
    onCheck: (isChecked: Boolean) -> Unit
) =
    UI {
        var isChecked = checked
        verticalLayout {
            textView(text) {
                isClickable = true
                if (checked) {
                    paintFlags = paintFlags xor Paint.UNDERLINE_TEXT_FLAG
                }
                setOnClickListener {
                    isChecked = !isChecked
                    paintFlags = paintFlags xor Paint.UNDERLINE_TEXT_FLAG
                    onCheck(isChecked)
                }
            }.lparams {
                this.gravity = Gravity.CENTER
            }
            bottomPadding = dip(8)
        }
    }.view


private fun Context.seekBarView(
    initial: Int,
    text: String,
    range: IntRange = 1..100,
    callback: (Int) -> Unit
) = UI {
    verticalLayout {
        padding = dip(8)
        var sb: SeekBar? = null
        var tvSize: TextView? = null
        linearLayout {
            imageButton(R.drawable.ic_menu_left) {
                setOnClickListener {
                    sb?.apply { progress-- }
                }
                setRippleBackground()
                
            }.lparams(dip(36), dip(24))
            
            tvSize = textView("$text: $initial") {
                gravity = Gravity.CENTER
                textAlignment = View.TEXT_ALIGNMENT_CENTER
            }.lparams(wrapContent, wrapContent, weight = 1f)
            
            imageButton(R.drawable.ic_menu_right) {
                setOnClickListener {
                    sb?.apply { progress++ }
                }
                setRippleBackground()
            }.lparams(dip(36), dip(24))
        }.lparams(matchParent, wrapContent)
        
        sb = seekBar {
            progress = initial - range.first
            max = range.last - range.first
            
            setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                @SuppressLint("SetTextI18n")
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    val progrUpd = progress + range.first
                    
                    callback.invoke(progrUpd)
                    tvSize!!.text = "$text: $progrUpd"
                }
                
                override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit
                override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit
            })
        }.lparams(matchParent, wrapContent)
    }
}.view