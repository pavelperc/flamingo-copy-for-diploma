package com.flamingo.animator.activity.layerBottomSheetHelper

import android.view.View
import android.widget.ImageButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.flamingo.animator.R
import com.flamingo.animator.activity.DrawingActivity
import com.flamingo.animator.adapters.LayerListAdapter
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog


class LayerBottomSheetHelper(val activity: DrawingActivity) {
    
    val rootView = activity.layoutInflater.inflate(R.layout.layers_bottom_sheet, null)
    
    
    val bottomSheetDialog =
        BottomSheetDialog(activity).apply {
            setContentView(rootView)
            val behavior = BottomSheetBehavior.from(rootView.parent as View)
            
            // but hiding on touch outside is still enabled!!
            behavior.isHideable = false
//            behavior.isFitToContents = true
            
            setOnShowListener { 
                layerListAdapter.notifyDataSetChanged()
            }
        }
    
    val layerListAdapter: LayerListAdapter
    
    init {
        val rvLayers = rootView.findViewById<RecyclerView>(R.id.rvLayers)
        val linearLayoutManager = LinearLayoutManager(activity)
        rvLayers.layoutManager = linearLayoutManager
        
        // disable animation, as it works bad..
        rvLayers.itemAnimator = null
        
        layerListAdapter = LayerListAdapter(activity, rvLayers, bottomSheetDialog)
        
        rvLayers.adapter = layerListAdapter
        layerListAdapter.itemTouchHelper.attachToRecyclerView(rvLayers)
    
        val btnAdd = rootView.findViewById<ImageButton>(R.id.btnAddLayer)
        btnAdd.setOnClickListener {
            // makes the new layer selected.
            activity.surfaceView.currentImageContainer.addLayer()
            layerListAdapter.notifyItemInserted(layerListAdapter.layerContainers.size - 1)
            layerListAdapter.updateSelectedLayer()
            // to reset the back button and refresh the screen
            activity.surfaceView.updateState()
            bottomSheetDialog.dismiss()
        }
    }
    
}