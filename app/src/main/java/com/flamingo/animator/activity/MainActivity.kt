package com.flamingo.animator.activity

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.flamingo.animator.R
import com.flamingo.animator.adapters.ProjectsListAdapter
import com.flamingo.animator.utils.SomeUsefulActivity
import com.flamingo.animator.utils.dialogUtils.enterNameAlert
import com.flamingo.animator.utils.fileUtils.listDirs
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast

class MainActivity : SomeUsefulActivity() {
    
    private val projectsListAdapter by lazy { ProjectsListAdapter(this) }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        
        rvProjects.setHasFixedSize(true)
        rvProjects.layoutManager = LinearLayoutManager(this)
        rvProjects.adapter = projectsListAdapter
        
        projectsListAdapter.projects.addAll(
            repo.projectsDir
                .listDirs()
                .map { it.name }
                .toMutableList())
        
        fab.setOnClickListener {
            enterNameAlert("Project name") { projectName ->
                val projectDir = repo.projectsDir.resolve(projectName)
                if (projectDir.exists()) {
                    toast("Project with name $projectName already exists.")
                    return@enterNameAlert false
                }
                if (!projectDir.mkdirs()) {
                    toast("Can not create project with name $projectName.")
                    return@enterNameAlert false
                }
                projectsListAdapter.projects.add(projectName)
                openProject(projectName)
                true
            }
        }
    }
    
    fun openProject(projectName: String) {
        activityHelper.startForResult<ProjectActivity>("projectName" to projectName) { _, data ->
            val newName = data?.getStringExtra("newName")
            if (newName != null && projectName != newName) {
                projectsListAdapter.updateProject(projectName, newName)
            }
        }
    }
}
