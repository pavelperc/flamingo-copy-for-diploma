 package com.flamingo.animator.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.flamingo.animator.BuildConfig
import com.flamingo.animator.R
import com.flamingo.animator.activity.projectTabs.AssetsTab
import com.flamingo.animator.activity.projectTabs.ProjectActivityTab
import com.flamingo.animator.activity.projectTabs.ScenesTab
import com.flamingo.animator.activity.projectTabs.TasksTab
import com.flamingo.animator.repository.AssetRepo
import com.flamingo.animator.repository.ProjectRepo
import com.flamingo.animator.repository.SceneRepo
import com.flamingo.animator.utils.SomeUsefulActivity
import com.flamingo.animator.utils.commonUtils.setUnderline
import com.flamingo.animator.utils.dialogUtils.enterNameAlert
import com.flamingo.animator.utils.removeAllUnknownFiles
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_project.*
import org.jetbrains.anko.toast

 class ProjectActivity : SomeUsefulActivity() {
    
    private val bottomNavigationListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val oldTab = selectedTab
        selectedTab = when (item.itemId) {
            R.id.navigation_assets -> assetsTab
            R.id.navigation_scenes -> scenesTab
            R.id.navigation_tasks -> tasksTab
            else -> null
        }
        if (oldTab != selectedTab) {
            oldTab?.setGone()
            oldTab?.enableSelectionMode(false)
            selectedTab?.setVisible()
        }
        true
    }
    
    var projectName: String
        get() = tvProjectName.text.toString()
        set(value) {
            tvProjectName.text = value
        }
    
    lateinit var projectRepo: ProjectRepo
    lateinit var assetRepo: AssetRepo
    lateinit var sceneRepo: SceneRepo
    
    override fun onDestroy() {
        projectRepo.close()
        super.onDestroy()
    }
    
    lateinit var assetsTab: AssetsTab
    lateinit var scenesTab: ScenesTab
    lateinit var tasksTab: TasksTab
    
    var selectedTab: ProjectActivityTab? = null
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_project)
        supportActionBar?.hide()
        
        projectName = intent.getStringExtra("projectName")!!
        projectRepo = ProjectRepo(repo, projectName)
        assetRepo = AssetRepo(projectRepo)
        sceneRepo = SceneRepo(projectRepo)
        
        if (BuildConfig.DEBUG) {
            removeAllUnknownFiles(assetRepo)
        }
        
        assetsTab = AssetsTab(this)
        scenesTab = ScenesTab(this)
        tasksTab = TasksTab(this)
        selectedTab = assetsTab
    
        tvProjectName.setUnderline()
        // project renaming
        tvProjectName.setOnClickListener {
            enterNameAlert("Project name", projectName) { newName ->
                val newProjectDir = repo.projectsDir.resolve(newName)
                
                if (newProjectDir.exists()) {
                    toast("Project with name $newName already exists.")
                    return@enterNameAlert false
                }
                
                projectRepo.close() // close realm
                val renamed = repo.getProjectDir(projectName).renameTo(newProjectDir)
                if (renamed) {
                    projectName = newName
                } else {
                    toast("Project not renamed.")
                }
                // update realm instance and refresh all
                projectRepo = ProjectRepo(repo, projectName)
                assetRepo = AssetRepo(projectRepo)
                
                assetsTab.updateAssetsContent()
                true
            }
        }
        
        // bottom menu
        navView.setOnNavigationItemSelectedListener(bottomNavigationListener)
        
        assetsTab.enableSelectionMode(false)
        
    }
    
    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        // restore the selected tab
        navView.menu.findItem(navView.selectedItemId)?.also { menuItem ->
            bottomNavigationListener.onNavigationItemSelected(menuItem)
        }
    }
    
    
    override fun finish() {
        val data = Intent()
        data.putExtra("newName", projectName)
        setResult(Activity.RESULT_OK, data)
        super.finish()
    }
    
    override fun onBackPressed() {
        if (selectedTab?.selectionEnabled == true) {
            selectedTab?.enableSelectionMode(false)
            return
        }
        super.onBackPressed()
    }
}
