package com.flamingo.animator.utils.dialogUtils

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.Gravity
import android.view.WindowManager
import android.widget.EditText
import org.jetbrains.anko.*

/** Overrides positive button onClick, but now it returns a boolean to close dialog or not. */
fun DialogInterface.nonClosingPositiveButton(onClick: () -> Boolean) {
    (this as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE)
        .setOnClickListener {
            val shouldClose = onClick()
            
            if (shouldClose) {
                this.dismiss()
            }
        }
}

fun Context.fastAlert(message: String, title: String? = null, onSuccess: () -> Unit) {
    alert(message, title) {
        cancelButton { }
        okButton { onSuccess() }
    }.show()
}

fun Context.fastYesNoAlert(message: String, title: String? = null, onSuccess: () -> Unit) {
    alert(message, title) {
        negativeButton("No") { }
        positiveButton("Yes") { onSuccess() }
    }.show()
}

/** onSuccess returns true if we should close dialog.
 * Returned name is trimmed. */
fun Context.enterNameAlert(
    title: String = "Enter Name",
    initialName: String = "",
    onSuccess: (String) -> Boolean
) {
    var etName: EditText? = null
    alert {
        customTitle {
            textView(title) {
                padding = dip(4)
                textAppearance = android.R.style.TextAppearance_Material_Headline
                gravity = Gravity.CENTER
            }
            
        }
        
        customView {
            linearLayout {
                setPadding(dip(8), 0, dip(8), 0)
                etName = editText(initialName) {
                }.lparams(
                    matchParent,
                    wrapContent
                )
            }
        }
        
        cancelButton { }
        yesButton { /* override below. */ }
    }
        .show()
        .also {
            etName?.selectAll()
            // show keyboard
            (it as AlertDialog).window!!.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
            )
        }
        .nonClosingPositiveButton {
            val projectName = etName!!.text.toString().trim()
            if (projectName.isEmpty()) {
                // do not dismiss
                return@nonClosingPositiveButton false
            }
            
            onSuccess(projectName)
        }
}
