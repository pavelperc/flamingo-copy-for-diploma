package com.flamingo.animator.utils

import android.app.Activity
import com.flamingo.animator.model.ImageFile
import com.flamingo.animator.repository.AssetRepo
import org.jetbrains.anko.toast

/** Removes images, that are not in database. Used for testing and may be slow!! */
fun Activity.removeAllUnknownFiles(assetRepo: AssetRepo) {
    val dbFileNames = assetRepo.realm.where(ImageFile::class.java)
        .findAll()
        .map { it.fileName }
    
    val assetImages =
        assetRepo.assetsDir.listFiles { file -> file.extension == "png" || file.extension == "" }
    
    val deletedNames = assetImages
        .filter { it.name !in dbFileNames }
        .map {
            it.delete()
            it.name
        }
    if (deletedNames.isNotEmpty()) {
        toast("Deleted images from db: $deletedNames")
    }
}