package com.flamingo.animator.utils

import android.app.Activity
import android.content.Intent
import android.util.Log
import org.jetbrains.anko.startActivityForResult


typealias OnActivityResult = (resultCode: Int, data: Intent?) -> Unit


/** Replaces startActivityForResult with [startForResult] with callback. */
class ActivityHelper(val activity: Activity) {
    var _requestCounter: Int = 0
    val _resultsMap = mutableMapOf<Int, OnActivityResult>()


    /** anko startActivityForResult version with a callback. */
    inline fun <reified T : Activity> startForResult(
        vararg params: Pair<String, Any?>,
        noinline onResult: OnActivityResult
    ) {
        _resultsMap[_requestCounter] = onResult
        activity.startActivityForResult<T>(_requestCounter, *params)
        _requestCounter++
    }

    /** default startActivityForResult version with a callback. */
    fun startForResult(
        intent: Intent,
        onResult: OnActivityResult
    ) {
        _resultsMap[_requestCounter] = onResult
        activity.startActivityForResult(intent, _requestCounter)
        _requestCounter++
    }

    /** should intercept onActivityResult from the activity. */
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        _resultsMap[requestCode]?.invoke(resultCode, data)
            ?: Log.e("my_tag", "not found request code $requestCode")

        _resultsMap.remove(requestCode)
    }
}