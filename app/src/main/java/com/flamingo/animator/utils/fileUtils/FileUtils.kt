package com.flamingo.animator.utils.fileUtils

import java.io.File


fun File.listDirs() = listFiles { file -> file.isDirectory }

fun File.rename(newName: String) = renameTo(parentFile.resolve(newName))

