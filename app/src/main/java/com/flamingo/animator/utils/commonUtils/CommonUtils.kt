package com.flamingo.animator.utils.commonUtils

import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.ImageView
import android.widget.Spinner
import android.widget.TextView
import androidx.recyclerview.widget.SortedList
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.bumptech.glide.signature.ObjectKey
import com.flamingo.animator.R
import com.flamingo.animator.editors.drawUtils.CheckerBoard
import com.github.clans.fab.FloatingActionMenu
import org.jetbrains.anko.dip
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.util.*
import kotlin.reflect.KMutableProperty0
import kotlin.reflect.KProperty
import kotlin.reflect.KProperty0

fun log(text: String) {
    Log.d("flam_tag", text)
}

fun logInfo(text: String) {
    Log.i("flam_tag", text)
}

fun logError(text: String, exc: Throwable? = null) {
    if (exc != null) {
        Log.e("flam_tag", text, exc)
    } else {
        Log.e("flam_tag", text)
    }
}

fun View.setVisible() {
    visibility = View.VISIBLE
}

/** Makes the view visible or gone depending on [isVisible]. */
fun View.setVisible(isVisible: Boolean) {
    visibility = if (isVisible) View.VISIBLE else View.GONE
}

fun View.setInvisible() {
    visibility = View.INVISIBLE
}

fun View.setGone() {
    visibility = View.GONE
}

fun View.isGone() = visibility == View.GONE
fun View.isVisible() = visibility == View.VISIBLE

fun View.onShowing(onInit: () -> Unit) {
    viewTreeObserver.addOnGlobalLayoutListener(object :
        ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            if (isShown) {
                viewTreeObserver.removeOnGlobalLayoutListener(this)
                onInit()
            }
        }
    })
}

fun TextView.setUnderline() {
    paintFlags = paintFlags or Paint.UNDERLINE_TEXT_FLAG
    
}

fun FloatingActionMenu.setClosedOnTouchOutsideAndExpand() {
    this.layoutParams.apply {
        width = ViewGroup.LayoutParams.MATCH_PARENT
        height = ViewGroup.LayoutParams.MATCH_PARENT
    }
    this.setClosedOnTouchOutside(true)
}

/** Draws a compressed picture from file, with caching by Glide lib.
 * [drawCheckersAndAdjust] means, that the image background will be filled with checkers.
 * Warning!!! Changes image width and height, according to image ratio and [heightDp],
 * if [drawCheckersAndAdjust] is enabled. */
fun ImageView.setFilePreview(
    imageFile: File,
    drawCheckersAndAdjust: Boolean = true,
    heightDp: Int = this.context.dip(150)
) {
    val ivImage = this
    Glide.with(this.context)
        .load(imageFile)
        .signature(ObjectKey(imageFile.lastModified()))
        .placeholder(R.drawable.ic_placeholder)
        .into(object : CustomTarget<Drawable>() {
            override fun onLoadCleared(placeholder: Drawable?) {
                ivImage.setImageDrawable(placeholder)
            }
            
            override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                if (drawCheckersAndAdjust) {
                    ivImage.background = CheckerBoard.createCheckeredDrawable()
                    ivImage.layoutParams.height = heightDp
                    ivImage.layoutParams.width =
                        heightDp * resource.intrinsicHeight / resource.intrinsicHeight
                    ivImage.requestLayout()
                }
                ivImage.setImageDrawable(resource)
            }
        })
}

fun <T> SortedList<T>.toList() = List(size()) { i -> get(i) }

fun JSONArray.toList() = List(length()) { i -> getJSONObject(i)!! }
fun JSONArray.toIntList() = List(length()) { i -> getInt(i) }
fun JSONArray.toLongList() = List(length()) { i -> getLong(i) }
fun JSONArray.toStringList() = List(length()) { i -> getString(i)!! }
fun List<JSONObject>.toJsonArray() = fold(JSONArray()) { ja, jo -> ja.put(jo)!! }

fun List<Int>.toIntJsonArray() = fold(JSONArray()) { ja, i -> ja.put(i)!! }
fun List<Long>.toLongJsonArray() = fold(JSONArray()) { ja, i -> ja.put(i)!! }

fun JSONObject.intOrNull(name: String) = if (has(name) && !isNull(name)) getInt(name) else null
fun JSONObject.boolOrNull(name: String) = if (has(name) && !isNull(name)) getBoolean(name) else null
fun JSONObject.doubleOrNull(name: String) =
    if (has(name) && !isNull(name)) getDouble(name) else null

/** Returns 0f by default. */
fun JSONObject.optFloat(name: String) = optDouble(name, 0.0).toFloat()

fun JSONObject.optFloat(name: String, fallback: Float) =
    optDouble(name, fallback.toDouble()).toFloat()

fun JSONObject.getFloat(name: String) = getDouble(name).toFloat()


/** Makes json from string or returns empty json in case of error. */
fun String?.safeJson() = try {
    if (this.isNullOrEmpty()) JSONObject() else JSONObject(this)
} catch (e: JSONException) {
    logError("error parsing json: '$this'", e)
    JSONObject()
}


/** Creates a unique name from [prefix] and [existingNames] size. */
fun uniqueName(existingNames: Collection<String>, prefix: String = ""): String {
    var pos = existingNames.size + 1
    var name = "$prefix$pos"
    while (name in existingNames) {
        pos++
        name = "$prefix$pos"
    }
    return name
}

/** Updates a name to make it unique amoung [existingNames], or stays it unchanged. */
fun makeNameUnique(existingNames: Collection<String>, preferredName: String): String {
    var name = preferredName
    var pos = 1
    while (name in existingNames) {
        name = "$preferredName($pos)"
        pos++
    }
    return name
}


/** Move item from [pos1] to [pos2] */
fun <T> MutableList<T>.move(pos1: Int, pos2: Int) {
    if (pos1 < pos2) {
        for (i in pos1 until pos2) {
            Collections.swap(this, i, i + 1)
        }
    } else {
        for (i in pos1 downTo (pos2 + 1)) {
            Collections.swap(this, i, i - 1)
        }
    }
}

fun Boolean.toInt() = if (this) 1 else 0

/** Writes a measured time after the prefix string. */
inline fun <T> measureTime(prefix: String, crossinline action: () -> T): T {
    return action()

//    val start = System.nanoTime()
//    val result = action()
//    val time = System.nanoTime() - start
//    log(prefix + ": %.2f".format(time / 1_000_000.0) + " millis")
//    return result
}

// this allows to make lie this:
// var absoluteX by bone::absoluteX
operator fun <T> KProperty0<T>.getValue(thisRef: Any?, property: KProperty<*>) = get()

operator fun <T> KMutableProperty0<T>.setValue(
    thisRef: Any?,
    property: KProperty<*>,
    value: T
) = set(value)

fun colorWithAlpha(color: Int, alpha: Int): Int {
    return Color.argb(alpha, Color.red(color), Color.green(color), Color.blue(color))
}

fun Spinner.hideDropdown() {
    try {
        val method = Spinner::class.java.getDeclaredMethod("onDetachedFromWindow")
        method.isAccessible = true;
        method.invoke(this)
    } catch (e: Exception) {
    }
}
