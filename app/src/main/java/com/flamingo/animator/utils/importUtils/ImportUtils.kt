package com.flamingo.animator.utils.importUtils

import android.content.Intent
import android.graphics.Bitmap
import android.provider.MediaStore
import androidx.appcompat.app.AppCompatActivity
import com.flamingo.animator.utils.SomeUsefulActivity


fun SomeUsefulActivity.importImage(onSuccess: (Bitmap) -> Unit) {
    val intent = Intent()
    intent.type = "image/*";
    intent.action = Intent.ACTION_GET_CONTENT
    
    activityHelper.startForResult(
        Intent.createChooser(
            intent,
            "Select Picture"
        )
    ) { resultCode, data ->
        // When an Image is picked
        if (resultCode == AppCompatActivity.RESULT_OK && data != null) {
            val uri = data.data
            
            if (uri != null) {
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uri)
                onSuccess(bitmap)
            }
        }
    }
}

// https://stackoverflow.com/questions/19585815/select-multiple-images-from-android-gallery

fun SomeUsefulActivity.importMultipleImages(callback: (Sequence<Bitmap>) -> Unit) {
    val intent = Intent()
    intent.type = "image/*";
    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
    intent.action = Intent.ACTION_GET_CONTENT
    
    activityHelper.startForResult(
        Intent.createChooser(
            intent,
            "Select Pictures"
        )
    ) { resultCode, data ->
        // When an Image is picked
        if (resultCode == AppCompatActivity.RESULT_OK && data != null) {
            val singleUri = data.data
            val clipData = data.clipData
            
            if (singleUri != null) {
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, singleUri)
                callback(sequenceOf(bitmap))
                
            } else if (clipData != null) {
                val sequence = (0 until clipData.itemCount).asSequence().map { i ->
                    val item = clipData.getItemAt(i)
                    val uri = item.uri
                    val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uri)
                    bitmap
                }
                callback(sequence)
            } else {
                callback(emptySequence())
            }
        } else {
            callback(emptySequence())
        }
    }
}