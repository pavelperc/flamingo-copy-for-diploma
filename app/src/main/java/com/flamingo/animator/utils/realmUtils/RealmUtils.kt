package com.flamingo.animator.utils.realmUtils

import io.realm.Realm
import io.realm.RealmObject
import io.realm.kotlin.createObject
import kotlin.reflect.KClass

/** Creates new unique long id. */
fun <T : RealmObject> Realm.generateId(kClass: KClass<T>) =
    (where(kClass.java).max("id")?.toLong() ?: 0) + 1


/** Creates an object with generated unique long id. Object should have long id field. */
inline fun <reified T : RealmObject> Realm.createWithId() =
    createObject<T>(generateId(T::class))

/** Executes transaction, but not handles cancel on crash. Returns the transaction result. */
fun <T> Realm.transaction(transaction: () -> T): T {
    beginTransaction()
    val result = transaction()
    commitTransaction()
    return result
}

/** Executes transaction in extension lambda with returning result. */
fun <T, O: RealmObject> O.transaction(transaction: O.() -> T): T {
    realm?.beginTransaction() // if object is detached realm is null
    val result = transaction()
    realm?.commitTransaction()
    return result
}
