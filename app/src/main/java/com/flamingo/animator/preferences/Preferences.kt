package com.flamingo.animator.preferences

import android.content.Context

class Preferences(context: Context) {
    companion object {
        private const val SHARED_PREF_NAME = "my_settings"
    }
    
    val sharedPref = context.getSharedPreferences(
        SHARED_PREF_NAME,
        Context.MODE_PRIVATE
    )
    
    var loadedExamplesFlampackName: String?
        get() = sharedPref.getString("loaded_examples_flampack_name", null)
        set(value) {
            sharedPref.edit().putString("loaded_examples_flampack_name", value).apply()
        }
    
}