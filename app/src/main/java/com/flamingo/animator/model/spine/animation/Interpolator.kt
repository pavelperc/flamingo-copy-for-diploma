package com.flamingo.animator.model.spine.animation

object Interpolator {
    fun interpolateLinear(
        startTime: Int,
        endTime: Int,
        currentTime: Int,
        startValue: Float,
        endValue: Float
    ): Float {
        if (endTime == startTime) {
            return startValue
        }
        val ratio = (currentTime - startTime) / (endTime - startTime).toFloat()
        return startValue + (endValue - startValue) * ratio
    }
}