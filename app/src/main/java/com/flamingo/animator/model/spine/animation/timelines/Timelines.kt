package com.flamingo.animator.model.spine.animation.timelines

import com.flamingo.animator.model.spine.animation.Interpolator
import com.flamingo.animator.utils.commonUtils.optFloat
import org.json.JSONObject
import java.util.*

class BoneTimeline {
    companion object {
        fun fromJson(jo: JSONObject): BoneTimeline {
            return BoneTimeline()
                .apply {
                    val boneTimestampsJson = jo.optJSONObject("boneTimestamps") ?: JSONObject()
                    boneTimestampsJson.keys().forEach { key ->
                        val time = key.toInt()
                        boneTimestamps[time] =
                            BoneTimestamp.fromJson(
                                boneTimestampsJson.getJSONObject(key)
                            )
                    }
                    
                    val atchSelTimestampsJson =
                        jo.optJSONObject("atchSelTimestamps") ?: JSONObject()
                    atchSelTimestampsJson.keys().forEach { key ->
                        val time = key.toInt()
                        attachmentSelectionTimestamps[time] = atchSelTimestampsJson.getInt(key)
                    }
                    
                    val visibilityTimestampsJson =
                        jo.optJSONObject("visibilityTimestamps") ?: JSONObject()
                    visibilityTimestampsJson.keys().forEach { key ->
                        val time = key.toInt()
                        visibilityTimestamps[time] = visibilityTimestampsJson.getBoolean(key)
                    }
                }
        }
    }
    
    val isNotEmpty
        get() = boneTimestamps.isNotEmpty()
                || attachmentSelectionTimestamps.isNotEmpty()
                || visibilityTimestamps.isNotEmpty()
    
    /** Time in millis to timestamp. */
    val boneTimestamps = TreeMap<Int, BoneTimestamp>()
    
    /** Time in millis to slot position (stored as delta with pose setup). */
    val attachmentSelectionTimestamps = TreeMap<Int, Int>()
    
    /** Time in millis to subtree visibility. Not a delta with pose setup.
     * Before the first timestamp it is visible. */
    val visibilityTimestamps = TreeMap<Int, Boolean>()
    
    fun toJson(): JSONObject {
        return JSONObject().apply {
            if (boneTimestamps.isNotEmpty()) {
                val boneTimestampsJson = JSONObject().apply {
                    for ((time, boneTimestamp) in boneTimestamps) {
                        put(time.toString(), boneTimestamp.toJson())
                    }
                }
                put("boneTimestamps", boneTimestampsJson)
            }
            if (attachmentSelectionTimestamps.isNotEmpty()) {
                val attachmentSelectionTimestampsJson = JSONObject().apply {
                    for ((time, selectedSlot) in attachmentSelectionTimestamps) {
                        put(time.toString(), selectedSlot)
                    }
                }
                put("atchSelTimestamps", attachmentSelectionTimestampsJson)
            }
            if (visibilityTimestamps.isNotEmpty()) {
                val visibilityTimestampsJson = JSONObject().apply {
                    for ((time, visible) in visibilityTimestamps) {
                        put(time.toString(), visible)
                    }
                }
                put("visibilityTimestamps", visibilityTimestampsJson)
            }
        }
    }
    
    fun findInterpolatedValue(currentTime: Int, boneProp: (BoneTimestamp) -> Float): Float {
        if (boneTimestamps.isEmpty()) {
            return 0f
        }
        
        var startTime = Int.MIN_VALUE
        var endTime = Int.MAX_VALUE
        
        for (time in boneTimestamps.keys) {
            if (time <= currentTime) {
                startTime = time
            } else {
                endTime = time
                break
            }
        }
        var startValue = 0f
        val endValue: Float
        
        if (startTime == Int.MIN_VALUE) {
            startTime = 0
        } else {
            startValue = boneProp(boneTimestamps.getValue(startTime))
        }
        if (endTime == Int.MAX_VALUE) {
            return boneProp(boneTimestamps.lastEntry().value)
        } else {
            endValue = boneProp(boneTimestamps.getValue(endTime))
        }
        
        return Interpolator.interpolateLinear(
            startTime, endTime,
            currentTime,
            startValue, endValue
        )
    }
    
    // stored as a delta with pose setup
    fun findAttachmentSelection(currentTime: Int): Int {
        var answer = 0
        for ((time, value) in attachmentSelectionTimestamps.entries) {
            if (time <= currentTime) {
                answer = value
            } else {
                break
            }
        }
        return answer
    }
    
    // not a delta! default is true
    fun findVisibilitySelection(currentTime: Int): Boolean {
        var answer = true
        for ((time, value) in visibilityTimestamps.entries) {
            if (time <= currentTime) {
                answer = value
            } else {
                break
            }
        }
        return answer
    }
}

/** All values are additional to real bone coords.  */
class BoneTimestamp(
    var x: Float,
    var y: Float,
    var rotation: Float
) {
    constructor() : this(0f, 0f, 0f)
    
    companion object {
        fun fromJson(jo: JSONObject): BoneTimestamp {
            return BoneTimestamp(
                x = jo.optFloat("x"),
                y = jo.optFloat("y"),
                rotation = jo.optFloat("rotation")
            )
        }
    }
    
    fun toJson(): JSONObject {
        return JSONObject().apply {
            put("x", x)
            put("y", y)
            put("rotation", rotation)
        }
    }
}