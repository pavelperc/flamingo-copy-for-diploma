package com.flamingo.animator.model.spine

import com.flamingo.animator.model.*
import com.flamingo.animator.utils.realmUtils.createWithId
import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


/** Main class for spine animation. */
open class Spine : RealmObject(), Asset {
    companion object {
        /** Preview width and height should be less than this value. */
        val MAX_PREVIEW_SIZE = 1000
    
        fun init(realm: Realm, name: String) = realm.createWithId<Spine>().apply {
            this.name = name
            uncroppedImage = LayeredImage.init(realm, key())
            uncroppedImageDrawConfig = DrawConfig.init(realm)
            preview = ImageFile.init(realm, key() + "_preview")
        }
    }
    
    @PrimaryKey
    override var id: Long = 0L
    override var name: String = "spine0"
    
    /** Stores spine editor camera state, etc */
    var riggingDrawConfigJson: String? = null
    
    /** Null means bones are not set. */
    var bonesJson: String? = null
    
    var slots = RealmList<Slot>()
    
    var animations = RealmList<SpineAnimation>()
    
    /** Stores DrawConfig for [uncroppedImage]. */
    var uncroppedImageDrawConfig: DrawConfig? = null
    val uncroppedImageDrawConfigReq: DrawConfig
        get() = uncroppedImageDrawConfig
            ?: throw RealmNotFound("uncroppedImageDrawConfig", "Spine")
    
    /** Layered image with body parts as layers. Can be opened in drawing editor. */
    var uncroppedImage: LayeredImage? = null
    val uncroppedImageReq: LayeredImage
        get() = uncroppedImage ?: throw RealmNotFound("uncroppedImage", "Spine")
    
    // all layers, that are deleted from realm will be deleted from here too.
    var ignoredUncroppedLayersForImport = RealmList<Layer>()
    
    var preview: ImageFile? = null
    val previewReq: ImageFile get() = preview ?: throw RealmNotFound("preview", "Spine")
    
    fun key() = "spine$id"
    
    fun cascadeDelete() {
        slots.toList().forEach { it.cascadeDelete() }
        uncroppedImage?.cascadeDelete()
        uncroppedImageDrawConfig?.cascadeDelete()
        preview?.cascadeDelete()
        animations.toList().forEach { it.cascadeDelete() }
        deleteFromRealm()
    }
}

/** One slot can have multiple attachments (eyes closed/open).
 * All attachments should have equal size. Slot should contain at least one attachment. */
open class Slot : RealmObject() {
    companion object {
        fun init(realm: Realm, ownerKey: String) = realm.createWithId<Slot>().apply {
            this.ownerKey = ownerKey
            this.name = key()
        }
    }
    
    @PrimaryKey
    var id: Long = 0L
    var name: String = "spine0_slot0"
    var ownerKey: String = "spine0"
    var attachments = RealmList<Attachment>()
    
    /** 0 means no bone. */
    var boneId: Int = 0
    var selectedAttachmentPosition: Int = 0
    
    fun key() = "${ownerKey}_slot$id"
    
    val width: Int get() = attachments.firstOrNull()?.width ?: 0
    
    val height: Int get() = attachments.firstOrNull()?.height ?: 0
    
    fun cascadeDelete() {
        attachments.toList().forEach { it.cascadeDelete() }
        deleteFromRealm()
    }
}


open class Attachment : RealmObject() {
    companion object {
        fun init(realm: Realm, ownerKey: String) = realm.createWithId<Attachment>().apply {
            this.ownerKey = ownerKey
            this.name = key()
            image = ImageFile.init(realm, key())
        }
    }
    
    @PrimaryKey
    var id: Long = 0L
    var name: String = "atch0"
    // may become invalid. use only for image name!
    var ownerKey: String = "spine0_slot0"
    
    var image: ImageFile? = null
    val imageReq: ImageFile get() = image ?: throw RealmNotFound("image", "attachment")
    
    var width: Int = 0
    var height: Int = 0
    
    /** Contains x, y, rotation, boneId */
    var jsonData: String? = null
    
    /** Temp value for editor. Should not be exported. */
    var isCropped = true
    /** Attachment position will be changed after cropping.
     * Should be true for new images, and false for updated images.*/
    var shiftAfterCrop: Boolean = false
    
    /** Nullable. Connected layer from drawing editor. */
    var boundLayer: Layer? = null
    /** Timestamp in millis. 0 by default. */
    var boundLayerLastEdited : Long = 0L
    
    val boundLayerIsRemoved get() = boundLayer == null && boundLayerLastEdited > 0
    
    fun key() = "${ownerKey}_atch$id"
    
    fun cascadeDelete() {
        image?.cascadeDelete()
        deleteFromRealm()
    }
}

open class SpineAnimation : RealmObject() {
    companion object {
        fun init(realm: Realm) = realm.createWithId<SpineAnimation>()
    }
    
    @PrimaryKey
    var id: Long = 0L
    var name: String = "anim0"
    
    var timelinesJson: String = ""
    
    fun cascadeDelete() {
        deleteFromRealm()
    }
}
