package com.flamingo.animator.model

import android.graphics.Color
import com.flamingo.animator.utils.realmUtils.createWithId
import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.RealmResults
import io.realm.annotations.LinkingObjects
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required
import io.realm.kotlin.createObject
import java.io.File

/*
Contract about database entities:

In most entities id should be a type of Long.
Each entity should contain two methods: companion object init, and cascadeDelete.

'init' method should take a realm object and create an entity with a unique id.
Also it must initialize all fields, that are considered as required.

'cascadeDelete()' should recursively delete all dependant entities.
It shouldn't delete images and other resources, that are connected to the entity.
This method should be present, even if it actually just removes this entity.  

Each entity should be created with init method, and be deleted with cascadeDelete method.

All entity fields should be var.
All object fields are always nullable.
Database primitive types are mostly required, as the
@Required annotation is inferred automatically from kotlin types.
If the field is nullable, it may contain a non-null method wrapper, like image and imageReq().
Such wrapper should throw RealmNotFound() exception. 

Entities (or their children), that represent some resources (like images),
should have a 'key()' method or property, that returns
a completely unique and immutable name to store the resource in the resource folder.
For example the key may consist of the entity name and id, like "background11"
Or contain some parent's key, like "anim5_layer6".

Cases when the data may be stored in json string:
* it is always used together: config always loaded as a whole, bone tree is always loaded together...
 you are sure you won't use it partially
* you want to cache the database data to have a faster access
* you want to provide some additional logic to your object and don't want
 to put it in the database entity class
* it should be used in another thread, not where the realm was initialized.

*/


/** Cached image info from the editor. */
open class DrawConfig : RealmObject() {
    @PrimaryKey
    var id: Long = 0
    
    companion object {
        fun init(realm: Realm) = realm.createWithId<DrawConfig>()
        
        val initialColors = Array(10) { i -> if (i == 0) Color.BLACK else Color.WHITE }
    }
    
    @Required
    var latestColors: RealmList<Int> = RealmList(*initialColors)
    var latestFrame = 0
    var brushStateJson: String = ""
    var backgroundColor = Color.TRANSPARENT
    
    fun setLatestColors(intArray: IntArray) {
        latestColors.clear()
        latestColors.addAll(intArray.toTypedArray())
    }
    
    fun copyFromOther(other: DrawConfig) {
        backgroundColor = other.backgroundColor
        latestColors.clear()
        latestColors.addAll(other.latestColors)
        
        latestFrame = other.latestFrame
        brushStateJson = other.brushStateJson
    }
    
    fun cascadeDelete() {
        latestColors.deleteAllFromRealm()
        deleteFromRealm()
    }
}

// Background, Object or Character
interface Asset {
    val id: Long
    var name: String
}

open class ImageFile : RealmObject() {
    companion object {
        /** [name] without extension. Should be unique. Extension will be added. */
        fun init(realm: Realm, name: String) = realm.createObject<ImageFile>("$name.png")
    }
    
    /** File name with png extension. */
    @PrimaryKey
    var fileName: String = "undefined"
    
    fun getFile(parentDir: File) = parentDir.resolve(fileName)
    fun cascadeDelete() {
        deleteFromRealm()
    }
}

open class Layer : RealmObject() {
    companion object {
        fun init(realm: Realm, ownerKey: String) = realm.createWithId<Layer>().apply {
            this.ownerKey = ownerKey
            image = ImageFile.init(realm, key())
            isVisible = true
            lastEdited = System.currentTimeMillis()
        }
    }
    
    var isVisible: Boolean = true
    @PrimaryKey
    var id: Long = 0L
    var image: ImageFile? = null
    val imageReq: ImageFile get() = image ?: throw RealmNotFound("image", "layer")
    /** Not unique */
    var name: String = key()
    
    /** Timestamp in millis when the layer was saved.
     * It may not correspond with the file lastModified, if the file was imported. */
    var lastEdited: Long = 0L
    
    var ownerKey: String = "bg0"
    fun key() = "${ownerKey}_lr$id"
    
    fun copyFromOther(oldLayer: Layer, assetsDir: File) {
        name = oldLayer.name
        isVisible = oldLayer.isVisible
        val oldFile = oldLayer.imageReq.getFile(assetsDir)
        val newFile = imageReq.getFile(assetsDir)
        oldFile.copyTo(newFile, true)
    }
    
    fun cascadeDelete() {
        image?.cascadeDelete()
        deleteFromRealm()
    }
}


open class LayeredImage : RealmObject() {
    companion object {
        fun init(realm: Realm, ownerKey: String) = realm.createWithId<LayeredImage>().apply {
            this.ownerKey = ownerKey
            builtImage = ImageFile.init(realm, ownerKey)
        }
    }
    
    @PrimaryKey
    var id: Long = 0L
    var layers = RealmList<Layer>()
    var builtImage: ImageFile? = null
    val builtImageReq: ImageFile get() = builtImage ?: throw RealmNotFound("builtImage", "layeredImage ${key()}")
    
    var width: Int = 0
    var height: Int = 0
    
    var ownerKey: String = "bg0"
    /** Layered image doesn't produce a new key. Owners key equals builtImage.fileName without extension. */
    fun key() = ownerKey
    
    fun cascadeDelete() {
        layers.toList().forEach { it.cascadeDelete() }
        builtImage?.cascadeDelete()
        deleteFromRealm()
    }
    
    /** Deletes all layer existing files and built image file. */
    fun deleteAllFiles(assetsDir: File) {
        builtImage?.getFile(assetsDir)?.delete()
        layers.forEach { it.image?.getFile(assetsDir)?.delete() }
    }
}


class RealmNotFound(field: String, owner: String) : IllegalStateException("Not found $field in $owner.")

open class Background : RealmObject(), Asset {
    companion object {
        fun init(realm: Realm) = realm.createWithId<Background>().apply {
            name = key()
            drawConfig = DrawConfig.init(realm)
            image = LayeredImage.init(realm, key())
        }
    }
    
    fun key() = "bg$id"
    
    @PrimaryKey
    override var id: Long = 0L
    override var name: String = "bg0"
    
    
    var drawConfig: DrawConfig? = null
    val drawConfigReq: DrawConfig get() = drawConfig ?: throw RealmNotFound("drawConfig", "bg")
    
    var image: LayeredImage? = null
    val imageReq: LayeredImage get() = image ?: throw RealmNotFound("image", "bg")
    
    @LinkingObjects("background")
    val scenes: RealmResults<Scene>? = null
    
    fun cascadeDelete() {
        drawConfig?.cascadeDelete()
        image?.cascadeDelete()
        // delete all scenes, bound to this background
        scenes?.toList()?.forEach { it.deleteFromRealm() }
        deleteFromRealm()
    }
}

enum class ObjectType {
    BACKGROUND, FOREGROUND, COLLIDE, COLLECT;
}

open class Object : RealmObject(), Asset {
    companion object {
        fun init(realm: Realm) = realm.createWithId<Object>().apply {
            name = key()
        }
    }
    
    fun key() = "obj$id"
    
    @PrimaryKey
    override var id: Long = 0L
    override var name: String = "obj0"
    
    var animations = RealmList<Animation>()
    
    // TODO create a common type for Object and Spine and put these fields there
    private var objectTypeStr: String = ObjectType.BACKGROUND.name
    /** How will the object behave in the scene. */
    var objectType: ObjectType
        get() = ObjectType.valueOf(objectTypeStr)
        set(value) {
            objectTypeStr = value.name
        }
    
    val width: Int
        get() = animations.firstOrNull()?.width ?: 0
    
    val height: Int
        get() = animations.firstOrNull()?.height ?: 0
    
    
    fun cascadeDelete() {
        // toList to avoid bug with iterator
        animations.toList().forEach {
            it.cascadeDelete()
        }
        deleteFromRealm()
    }
}

open class Animation : RealmObject() {
    companion object {
        /** [ownerKey] is a unique name to group image files by prefix */
        fun init(realm: Realm, ownerKey: String) = realm.createWithId<Animation>().apply {
            this.ownerKey = ownerKey
            name = key()
            drawConfig = DrawConfig.init(realm)
            spriteImage = ImageFile.init(realm, key())
        }
    }
    
    val width: Int
        get() = frames.firstOrNull()?.image?.width ?: 0
    
    val height: Int
        get() = frames.firstOrNull()?.image?.height ?: 0
    
    
    @PrimaryKey
    var id: Long = 0L
    var name: String = "obj0_anim0"
    
    var ownerKey: String = "obj0"
    fun key() = "${ownerKey}_anim$id"
    
    var drawConfig: DrawConfig? = null
    val drawConfigReq: DrawConfig get() = drawConfig ?: throw RealmNotFound("drawConfig", "anim")
    
    var fps = 4.0
    var shiftX = 0
    var shiftY = 0
    var isLoop = false
    var frames = RealmList<Frame>()
    
    /** False if the sprite image does not exist or invalid. */
    var spriteSaved: Boolean = false
    var spriteImage: ImageFile? = null
    val spriteImageReq: ImageFile get() = spriteImage ?: throw RealmNotFound("spriteImage", "animation")
    
    fun cascadeDelete() {
        drawConfig?.cascadeDelete()
        // toList to avoid bug with iterator
        frames.toList().forEach {
            it.cascadeDelete()
        }
        spriteImage?.cascadeDelete()
        deleteFromRealm()
    }
}

open class Frame : RealmObject() {
    companion object {
        /** [ownerKey] is a unique anim name to group image files by prefix */
        fun init(realm: Realm, ownerKey: String) = realm.createWithId<Frame>().apply {
            this.ownerKey = ownerKey
            image = LayeredImage.init(realm, key())
        }
    }
    
    @PrimaryKey
    var id: Long = 0L
    
    var ownerKey: String = "obj0_anim0"
    fun key() = "${ownerKey}_$id"
    
    fun cascadeDelete() {
        image?.cascadeDelete()
        deleteFromRealm()
    }
    
    var image: LayeredImage? = null
    val imageReq: LayeredImage get() = image ?: throw RealmNotFound("image", "frame")
}




open class Scene : RealmObject() {
    companion object {
        fun init(realm: Realm, background: Background) = realm.createWithId<Scene>().apply {
            this.background = background
        }
    }
    
    @PrimaryKey
    var id: Long = 0L
    
    var name: String = "scene0"
    fun key() = "scene$id"
    
    var leftScene: Scene? = null
    var rightScene: Scene? = null
    
    var worldPaddingLeft: Int = 0 
    var worldPaddingRight: Int = 0
    var worldPaddingTop: Int = 0
    var worldPaddingBottom: Int = 0
    
    /** It always exists. */
    var background: Background? = null
    val backgroundReq get() = background ?: throw RealmNotFound("background", "Scene")
    var backgroundScale: Double = 1.0
    
    var sceneObjects = RealmList<SceneObject>()
    
    fun cascadeDelete() {
        sceneObjects.toList().forEach { it.cascadeDelete() }
        deleteFromRealm()
    }
    
}

/** A wrapper for object, put into the scene. */
open class SceneObject: RealmObject() {
    fun init(realm: Realm, obj: Object) = realm.createWithId<SceneObject>().apply {
        this.obj = obj
    }
    
    @PrimaryKey
    var id: Long = 0L
    
    var obj: Object? = null
    val objReq: Object get() = obj ?: throw RealmNotFound("obj", "SceneObject")
    
    var x: Int = 0
    var y: Int = 0
    var scale: Double = 1.0
    
    fun cascadeDelete() {
        deleteFromRealm()
    }
}

open class GameConfig : RealmObject() {
    
    var mainScene: Scene? = null
    
    // initial game start positions
    var playerStartX: Int = 0
    var playerStartY: Int = 0
    
    /** Object asset, that will be considered as a player. */
    var playerObject: Object? = null
    
    var projectSettingsJson = ""
    
    fun cascadeDelete() {
        deleteFromRealm()
    }
}