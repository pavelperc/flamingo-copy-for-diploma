package com.flamingo.animator.model.spine

import android.graphics.Matrix
import com.flamingo.animator.editors.spineEditor.dataHolders.SlotContainer
import com.flamingo.animator.utils.commonUtils.toList
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Math.toRadians
import kotlin.math.*


/** Relative parameters are used for storing the pose and updating rotation.
 * Absolute parameters are used for drawing on screen.
 * Timeline parameters are used during animation. */
class Bone private constructor(
    var parent: Bone?,
    // in relative coords. Bone start.
    var x: Float,
    var y: Float,
    /** In degrees, relative to parent's */
    var rotation: Float,
    var length: Float,
    var absoluteX: Float,
    var absoluteY: Float,
    // when rotation is 0, bone lies from (0,0) to (0, length)
    var absoluteRotation: Float,
    val id: Int
) {
    // here timeline coords are not additional to relative!!
    // but in timestamp class they are additional to relative
    var timelineX = 0f
    var timelineY = 0f
    var timelineRotation = 0f
    
    /** stored in db, disables the whole subtree */
    var visible = true
    /** Draw on screen. About current bone. */
    var absoluteVisible = true
    /** the same as visible, but from timeline */
    var timelineVisible = false
    
    val isInvisibleBranchStart
        get() = !absoluteVisible
                && (parent?.absoluteVisible == true || parent == null)
    
    /** slotContainer attached to bone. Not saved and used only while drawing. */
    var slotContainer: SlotContainer? = null
    
    val childrenRecursive: List<Bone> get() = children.flatMap { it.childrenRecursive + it }
    val children = mutableListOf<Bone>()
    
    constructor() : this(null, 0f, 0f, 0f, 0f, 0f, 0f, 0f, IdGenerator.generateId())
    
    companion object {
        private val matrix = Matrix()
        private val point = floatArrayOf(0f, 0f)
        
        fun fromJson(jo: JSONObject, parent: Bone? = null): Bone {
            val bone = Bone(
                parent,
                jo.getDouble("x").toFloat(),
                jo.getDouble("y").toFloat(),
                jo.getDouble("rotation").toFloat(),
                jo.getDouble("length").toFloat(),
                0f, 0f, 0f,
                IdGenerator.registerId(jo.optInt("id", IdGenerator.generateId()))
            )
            bone.visible = jo.optBoolean("visible", true)
            bone.updateAbsolute(false)
            val children = jo.getJSONArray("children").toList()
                .map { fromJson(it, bone) }
            
            bone.children.addAll(children)
            return bone
        }
    }
    
    private object IdGenerator {
        /** Maximum existing id. */
        private var maxId = 0
        
        fun generateId() = ++maxId
        
        fun registerId(id: Int): Int {
            // (very bad solution. Id is never reset. Even after app reloading.
            // in case of overload we will use min id
            maxId = if (maxId < 0) min(maxId, id) else max(id, maxId)
            return id
        }
    }
    
    fun setAbsoluteByStartEnd(
        startX: Float,
        startY: Float,
        endX: Float,
        endY: Float,
        updateLength: Boolean = true
    ) {
        val dx = endX - startX
        val dy = endY - startY
        val len = sqrt(dx * dx + dy * dy)
        
        var angle = Math.toDegrees(acos((dx / len).toDouble())).toFloat()
        if (dy < 0) {
            angle = -angle
        }
        if (updateLength) {
            length = len
        }
        absoluteRotation = angle
        absoluteX = startX
        absoluteY = startY
    }
    
    val absoluteEndX: Float
        get() = absoluteX + length * cos(toRadians(absoluteRotation.toDouble())).toFloat()
    
    val absoluteEndY: Float
        get() = absoluteY + length * sin(toRadians(absoluteRotation.toDouble())).toFloat()
    
    
    fun updateFrom(bone: Bone) {
        x = bone.x
        y = bone.y
        length = bone.length
        rotation = bone.rotation
        visible = bone.visible
        absoluteX = bone.absoluteX
        absoluteY = bone.absoluteY
        absoluteRotation = bone.absoluteRotation
        absoluteVisible = bone.absoluteVisible
        timelineX = bone.timelineX
        timelineY = bone.timelineY
        timelineRotation = bone.timelineRotation
        timelineVisible = bone.timelineVisible
        parent = bone.parent
    }
    
    fun updateFromAbsolute(timelineMode: Boolean) {
        fun set(x: Float, y: Float, rotation: Float, visible: Boolean) {
            if (timelineMode) {
                timelineX = x
                timelineY = y
                timelineRotation = rotation
                timelineVisible = visible
            } else {
                this.x = x
                this.y = y
                this.rotation = rotation
                this.visible = visible
            }
        }
        
        val parent = parent
        if (parent == null) {
            set(absoluteX, absoluteY, absoluteRotation, absoluteVisible)
            return
        }
        
        matrix.reset()
        matrix.postRotate(parent.absoluteRotation)
        matrix.postTranslate(parent.absoluteX, parent.absoluteY)
        matrix.invert(matrix)
        
        point[0] = absoluteX
        point[1] = absoluteY
        matrix.mapPoints(point)
        
        set(point[0], point[1], absoluteRotation - parent.absoluteRotation, absoluteVisible)
    }
    
    fun updateAbsolute(timelineMode: Boolean) {
        val x = if (timelineMode) timelineX else x
        val y = if (timelineMode) timelineY else y
        val rotation = if (timelineMode) timelineRotation else rotation
        val visible = if (timelineMode) timelineVisible else visible
        
        val parent = parent
        if (parent == null) {
            absoluteX = x
            absoluteY = y
            absoluteRotation = rotation
            absoluteVisible = visible
            return
        }
        matrix.reset()
        matrix.postRotate(parent.absoluteRotation)
        matrix.postTranslate(parent.absoluteX, parent.absoluteY)
        
        point[0] = x
        point[1] = y
        matrix.mapPoints(point)
        
        absoluteX = point[0]
        absoluteY = point[1]
        absoluteRotation = parent.absoluteRotation + rotation
        absoluteVisible = visible && parent.absoluteVisible
    }
    
    /** Updates absolute position of all children */
    fun updateAllChildren(timelineMode: Boolean) {
        children.forEach { child ->
            child.updateAbsolute(timelineMode)
            child.slotContainer?.updateAbsolute(timelineMode)
            child.updateAllChildren(timelineMode)
        }
    }
    
    
    /** Json version contains only relative parameters. */
    fun toJson(): JSONObject = JSONObject().apply {
        put("id", id)
        put("x", x)
        put("y", y)
        put("length", length)
        put("rotation", rotation)
        put("visible", visible)
        put("children", JSONArray(children.map { it.toJson() }))
    }
    
    override fun toString() = "Bone(x=$x, y=$y, rotation=$rotation, length=$length, " +
            "visible=${visible}, id=$id)"
}