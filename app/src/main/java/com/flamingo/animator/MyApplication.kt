package com.flamingo.animator

import android.app.Application
import com.flamingo.animator.repository.Repository
import io.realm.Realm

class MyApplication : Application() {
    
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
    }
    
    val repo by lazy { Repository(applicationContext) }
}