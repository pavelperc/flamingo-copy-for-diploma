package com.flamingo.animator.repository

import io.realm.DynamicRealm
import io.realm.FieldAttribute
import io.realm.RealmMigration

class Migration : RealmMigration {
    
    companion object {
        const val DB_VERSION = 5L
    }
    
    
    
    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {
        @Suppress("NAME_SHADOWING")
        var oldVersion = oldVersion
        
        val schema = realm.schema
        if (oldVersion == 0L) {
            schema.get("DrawConfig")!!
                .addField("latestFrame", Int::class.java, FieldAttribute.REQUIRED)
                .addField("brushStateJson", String::class.java, FieldAttribute.REQUIRED)
            oldVersion++
        }
        
        if (oldVersion == 1L) {
            schema.get("Layer")!!
                .setNullable("name", false)
                .transform { obj ->
                    val id = obj.getLong("id")
                    obj.setString("name", "layer$id")
                }
            oldVersion++
        }
        
        // add spriteImage file for anim
        if (oldVersion == 2L) {
            schema.get("Animation")!!
                .addRealmObjectField("spriteImage", schema.get("ImageFile")!!)
                .transform { anim ->
                    val id = anim.getLong("id")
                    val ownerKey = anim.getString("ownerKey")
                    val imageName = "${ownerKey}_anim$id"
                    val imageFile = realm.createObject("ImageFile", imageName)
                    anim.setObject("spriteImage", imageFile)
                }
            oldVersion++
        }
        
        // fix extension in sprite!!!
        if (oldVersion == 3L) {
            schema.get("Animation")!!
                .transform { anim ->
                    val oldSpriteImage = anim.getObject("spriteImage")!!
                    val oldName = oldSpriteImage.getString("fileName")
                    oldSpriteImage.deleteFromRealm()
                    val newName = "$oldName.png"
                    val newSpriteImage = realm.createObject("ImageFile", newName)
                    anim.setObject("spriteImage", newSpriteImage)
                }
            oldVersion++
        }
        
        if (oldVersion == 4L) {
            schema.get("Animation")!!
                .addField("spriteSaved", Boolean::class.java, FieldAttribute.REQUIRED)
//            oldVersion++
        }
    }
    
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return true
    }
    
    override fun hashCode(): Int {
        return javaClass.hashCode()
    }
    
}