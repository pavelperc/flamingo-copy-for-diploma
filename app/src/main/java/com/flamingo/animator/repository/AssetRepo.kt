package com.flamingo.animator.repository

import android.graphics.Bitmap
import android.graphics.Color
import com.flamingo.animator.editors.drawUtils.saveFilledImageToFile
import com.flamingo.animator.model.*
import com.flamingo.animator.model.spine.Spine
import com.flamingo.animator.utils.commonUtils.makeNameUnique
import com.flamingo.animator.utils.commonUtils.uniqueName
import com.flamingo.animator.utils.realmUtils.transaction
import io.realm.Realm
import java.io.FileOutputStream

/**
 * Helper class to work with assets in realm database.
 *
 */
class AssetRepo(
    val projectRepo: ProjectRepo
) {
    val realm: Realm get() = projectRepo.realm
    val repo: Repository get() = projectRepo.repo
    
    val spineRepo = SpineRepo(this)
    
    fun isAssetNameUnique(name: String) =
        realm.where(Background::class.java).equalTo("name", name).findFirst() == null
                && realm.where(Object::class.java).equalTo("name", name).findFirst() == null
                && realm.where(Spine::class.java).equalTo("name", name).findFirst() == null
    
    fun <T> transaction(block: AssetRepo.() -> T): T {
        return realm.transaction { block() }
    }
    
    /** Directory with all assets. */
    val assetsDir by lazy {
        projectRepo.repo.getProjectDir(projectRepo.projectName).resolve("assets").apply { mkdirs() }
    }
    
    // ------ CREATE SECTION ------
    
    fun createBackground() = realm.transaction {
        Background.init(
            realm
        )
    }
    
    fun createObject() = realm.transaction {
        Object.init(
            realm
        )
    }
    
    fun createSpineObject(name: String) = realm.transaction {
        Spine.init(
            realm,
            name
        )
    }
    
    /** Inits anim with one frame. */
    fun initAnim(
        obj: Object,
        width: Int,
        height: Int,
        layeredImageToCopyFrom: LayeredImage? = null,
        predefinedBitmap: Bitmap? = null
    ): Animation {
        val anim = Animation.init(realm, obj.key())
        obj.animations.add(anim)
        anim.drawConfigReq.backgroundColor = Color.TRANSPARENT
        
        // insert to animation inside
        addNewFrame(anim, width, height, 0, layeredImageToCopyFrom, predefinedBitmap)
        
        return anim
    }
    
    /** Create a frame with a file, and attach it to animation on [position]. */
    fun addNewFrame(
        animation: Animation,
        width: Int,
        height: Int,
        position: Int = animation.frames.size,
        layeredImageToCopyFrom: LayeredImage? = null,
        predefinedBitmap: Bitmap? = null
    ): Frame {
        val frame = Frame.init(realm, animation.key())
        
        initLayeredImage(frame.imageReq, width, height, layeredImageToCopyFrom, predefinedBitmap)
        animation.frames.add(position, frame)
        return frame
    }
    
    /** Adds to [layeredImage] one layer with a file.
     * Or copies all layers and built image from the [layeredImageToCopyFrom].
     * If [predefinedBitmap] or [layeredImageToCopyFrom] is not null, width and height are ignored. */
    @Suppress("NAME_SHADOWING")
    fun initLayeredImage(
        layeredImage: LayeredImage,
        width: Int,
        height: Int,
        layeredImageToCopyFrom: LayeredImage? = null,
        predefinedBitmap: Bitmap? = null
    ) {
        if (layeredImageToCopyFrom == null) {
            val width = predefinedBitmap?.width ?: width
            val height = predefinedBitmap?.height ?: height
            
            val layer = addNewLayer(layeredImage, width, height, 0, predefinedBitmap)
            val layerImageFile = layer.imageReq.getFile(assetsDir)
            
            val builtImageFile = layeredImage.builtImageReq.getFile(assetsDir)
            layerImageFile.copyTo(builtImageFile, true)
            
            layeredImage.width = width
            layeredImage.height = height
        } else {
            check(layeredImageToCopyFrom.layers.size > 0) {
                "initLayeredImage: Got layeredImageToCopyFrom ${layeredImageToCopyFrom.key()} without layers."
            }
            // copy layers
            val layers = layeredImageToCopyFrom.layers.map { oldLayer ->
                val layer = Layer.init(realm, layeredImage.key())
                layer.copyFromOther(oldLayer, assetsDir)
                layer
            }
            layeredImage.layers.addAll(layers)
            
            // copy built image
            val oldBuiltImageFile = layeredImageToCopyFrom.builtImageReq.getFile(assetsDir)
            val newBuiltImageFile = layeredImage.builtImageReq.getFile(assetsDir)
            oldBuiltImageFile.copyTo(newBuiltImageFile, true)
            // copy size
            layeredImage.width = layeredImageToCopyFrom.width
            layeredImage.height = layeredImageToCopyFrom.height
        }
    }
    
    fun copyLayeredImage(srcImage: LayeredImage, dstImage: LayeredImage) {
        initLayeredImage(dstImage, 0, 0, srcImage)
    }
    
    /** Inits a layer with a backing file.
     * And inserts it to [layeredImage] on position.
     * If the [predefinedBitmap] is not null, [width] and [height] are not used. */
    fun addNewLayer(
        layeredImage: LayeredImage,
        width: Int,
        height: Int,
        position: Int = layeredImage.layers.size,
        predefinedBitmap: Bitmap? = null
    ): Layer {
        val layer = Layer.init(realm, layeredImage.key())
        layer.name = uniqueName(
            layeredImage.layers.map { it.name },
            "layer"
        )
        
        val layerImageFile = layer.imageReq.getFile(assetsDir)
        
        if (predefinedBitmap == null) {
            saveFilledImageToFile(
                layerImageFile,
                width,
                height,
                Color.TRANSPARENT
            )
        } else {
            predefinedBitmap.compress(
                Bitmap.CompressFormat.PNG,
                100,
                FileOutputStream(layerImageFile)
            )
        }
        layeredImage.layers.add(position, layer)
        return layer
    }
    
    fun duplicateLayer(layeredImage: LayeredImage, position: Int): Layer {
        val oldLayer = layeredImage.layers[position]!!
        val newLayer = Layer.init(realm, layeredImage.key())
        newLayer.copyFromOther(oldLayer, assetsDir)
        newLayer.name = makeNameUnique(layeredImage.layers.map { it.name }, oldLayer.name)
        layeredImage.layers.add(position + 1, newLayer)
        return newLayer
    }
    
    // ------ REMOVE SECTION ------
    
    fun removeBackground(background: Background) {
        background.image?.deleteAllFiles(assetsDir)
        realm.executeTransaction { background.cascadeDelete() }
    }
    
    fun removeObject(managedObject: Object) {
        managedObject.animations.flatMap { it.frames }.forEach {
            it.image?.deleteAllFiles(assetsDir)
        }
        
        realm.executeTransaction { managedObject.cascadeDelete() }
    }
    
    fun removeSpine(managedSpine: Spine) {
        spineRepo.deleteSpine(managedSpine)
    }
    
    
    fun removeAnim(anim: Animation) {
        anim.frames.forEach { it.image?.deleteAllFiles(assetsDir) }
        realm.executeTransaction { anim.cascadeDelete() }
    }
    
    fun removeFrame(frame: Frame) {
        frame.image?.deleteAllFiles(assetsDir)
        realm.executeTransaction { frame.cascadeDelete() }
    }
    
    fun removeLayer(layer: Layer) {
        layer.image?.getFile(assetsDir)?.delete()
        realm.executeTransaction { layer.cascadeDelete() }
    }
    
    // ------ FIND SECTION ------
    
    fun findBackgroundById(id: Long) =
        realm.where(Background::class.java).equalTo("id", id).findFirst()
            ?: throw RealmNotFoundId("background", id)
    
    fun findObjectById(id: Long) =
        realm.where(Object::class.java).equalTo("id", id).findFirst()
            ?: throw RealmNotFoundId("object", id)
    
    fun findSpineById(id: Long) = spineRepo.findSpineById(id)
    
    
    fun getAllBackgrounds() = realm.where(Background::class.java).findAll()
    fun getAllObjects() = realm.where(Object::class.java).findAll()
    fun getAllSpines() = spineRepo.getAllSpines()
    
    fun getAssetsCount() = realm.where(Spine::class.java).count() +
            realm.where(Object::class.java).count() +
            realm.where(Background::class.java).count();
    
    
    fun findDrawConfigById(id: Long) =
        realm.where(DrawConfig::class.java).equalTo("id", id).findFirst()
            ?: throw RealmNotFoundId("drawConfig", id)
    
    fun findAnimById(id: Long) =
        realm.where(Animation::class.java).equalTo("id", id).findFirst()
            ?: throw RealmNotFoundId("animation", id)
    
    fun findLayeredImageById(id: Long): LayeredImage =
        realm.where(LayeredImage::class.java).equalTo("id", id).findFirst()
            ?: throw RealmNotFoundId("layeredImage", id)
}