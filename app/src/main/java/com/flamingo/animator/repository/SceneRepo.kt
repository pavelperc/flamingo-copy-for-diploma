package com.flamingo.animator.repository

import com.flamingo.animator.model.Background
import com.flamingo.animator.model.Scene
import com.flamingo.animator.utils.realmUtils.transaction

/** Helper class to work with scene config entities of the realm database. */
class SceneRepo(private val projectRepo: ProjectRepo) {
    val realm get() = projectRepo.realm
    
    fun removeScene(scene: Scene) {
        realm.executeTransaction { scene.cascadeDelete() }
    }
    
    fun isSceneNameUnique(name: String): Boolean {
        return realm.where(Scene::class.java).equalTo("name", name).findFirst() == null
    }
    
    fun <T> transaction(block: SceneRepo.() -> T): T {
        return realm.transaction { block() }
    }
    
    fun initScene(background: Background): Scene {
        return Scene.init(realm, background).apply {
            name = background.name
        }
    }
    
    fun findAllScenes(): List<Scene> {
        return realm.where(Scene::class.java).findAll()
    }
    
    fun findSceneById(id: Long): Scene {
        return realm.where(Scene::class.java).equalTo("id", id).findFirst()
            ?: throw RealmNotFoundId("scene", id)
    }
}