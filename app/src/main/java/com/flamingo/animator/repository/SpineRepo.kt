package com.flamingo.animator.repository

import android.graphics.Bitmap
import android.graphics.Color
import com.flamingo.animator.model.*
import com.flamingo.animator.model.spine.Attachment
import com.flamingo.animator.model.spine.Slot
import com.flamingo.animator.model.spine.Spine
import com.flamingo.animator.utils.commonUtils.makeNameUnique
import com.flamingo.animator.utils.commonUtils.safeJson
import com.flamingo.animator.utils.commonUtils.uniqueName
import com.flamingo.animator.utils.realmUtils.transaction

class SpineRepo(private val assetRepo: AssetRepo) {
    
    private val realm get() = assetRepo.realm
    
    fun <T> transaction(block: SpineRepo.() -> T): T {
        return realm.transaction { block() }
    }
    
    fun findSpineById(id: Long) =
        realm.where(Spine::class.java).equalTo("id", id).findFirst()
            ?: throw RealmNotFoundId("Spine", id)
    
    fun getAllSpines() = realm.where(Spine::class.java).findAll()
    
    fun createSpineFromAnim(anim: Animation): Spine {
        val image = anim.frames[0]!!.imageReq
        val name = makeNameUnique(
            assetRepo.getAllSpines().map { it.name },
            anim.name
        )
        
        return createSpineFromLayeredImage(image, name, anim.drawConfigReq)
    }
    
    fun updateSpineFromDrawingEditor(spine: Spine) {
        // update from all layers with bound ids
        // find not bound visible layers and add them
        realm.transaction {
            val layeredImage = spine.uncroppedImageReq
            val ignoredLayerIds = spine.ignoredUncroppedLayersForImport.map { it.id }.toSet()
            val visibleLayersByIds = layeredImage.layers
                .filter { it.isVisible && it.id !in ignoredLayerIds }
                .associateBy { it.id }
                .toMutableMap()
            
            spine.slots.forEach { slot ->
                for (attachment in slot.attachments) {
                    val boundLayer = attachment.boundLayer ?: continue
                    visibleLayersByIds.remove(boundLayer.id)
                    if (boundLayer.lastEdited > attachment.boundLayerLastEdited) {
                        
                        attachment.isCropped = false
                        attachment.shiftAfterCrop = false
                        updateAttachmentFromLayer(attachment, boundLayer, layeredImage)
                        attachment.boundLayerLastEdited = boundLayer.lastEdited
                    }
                }
            }
            
            // delete attachments and slots with removed bound layers
            spine.slots.toList().forEach { slot ->
                slot.attachments.toList().forEach { attachment ->
                    if (attachment.boundLayerIsRemoved) {
                        deleteAttachment(slot, attachment)
                    }
                }
                if (slot.attachments.isEmpty()) {
                    deleteSlot(spine, slot)
                }
            }
            
            // create new slots from not updated visible layers
            visibleLayersByIds.values.forEach { layer ->
                createSlotFromLayer(spine, layer, layeredImage)
            }
        }
    }
    
    /** Copies layered image to spine layered image, and makes attachments. */
    fun createSpineFromLayeredImage(
        image: LayeredImage,
        name: String,
        drawConfig: DrawConfig
    ): Spine {
        val spine = assetRepo.createSpineObject(name)
        spine.transaction {
            for (layer in image.layers) {
                if (!layer.isVisible) {
                    continue
                }
                createSlotFromLayer(spine, layer, image)
            }
            // copy layered image
            assetRepo.initLayeredImage(uncroppedImageReq, 0, 0, image)
            
            // also copy drawConfig to new Spine, except background
            uncroppedImageDrawConfigReq.copyFromOther(drawConfig)
            uncroppedImageDrawConfigReq.backgroundColor = Color.TRANSPARENT
        }
        return spine
    }
    
    fun createSlotFromBitmap(spine: Spine, bitmap: Bitmap): Slot {
        return spine.transaction {
            val slot = Slot.init(realm, spine.key())
            val names = slots.map { it.name }
            slot.name = uniqueName(names, "slot")
            
            val attachment =
                Attachment.init(realm, slot.key())
            attachment.name = slot.name
            slot.attachments.add(attachment)
            
            val file = attachment.imageReq.getFile(assetRepo.assetsDir)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, file.outputStream())
            
            attachment.width = bitmap.width
            attachment.height = bitmap.height
            
            attachment.isCropped = false
            attachment.shiftAfterCrop = true
            centerAttachment(attachment)
            slot
        }
    }
    
    fun deleteSpine(managedSpine: Spine) {
        managedSpine.slots.flatMap { it.attachments }.forEach { attachment ->
            attachment.image?.getFile(assetRepo.assetsDir)?.delete()
        }
        managedSpine.uncroppedImage?.deleteAllFiles(assetRepo.assetsDir)
        managedSpine.preview?.getFile(assetRepo.assetsDir)?.delete()
        
        realm.executeTransaction { managedSpine.cascadeDelete() }
    }
    
    fun deleteSlot(spine: Spine, slot: Slot) {
        spine.slots.remove(slot)
        slot.attachments.forEach { attachment ->
            attachment.image?.getFile(assetRepo.assetsDir)?.delete()
        }
        slot.cascadeDelete()
    }
    
    fun deleteAttachment(slot: Slot, attachment: Attachment) {
        attachment.image?.getFile(assetRepo.assetsDir)?.delete()
        slot.attachments.remove(attachment)
        attachment.cascadeDelete()
    }
    
    private fun createSlotFromLayer(spine: Spine, layer: Layer, layeredImage: LayeredImage) {
        val slot = Slot.init(realm, spine.key())
        spine.slots.add(slot)
        slot.name = layer.name
        val attachment = Attachment.init(realm, slot.key())
        attachment.name = slot.name
        attachment.boundLayer = layer
        attachment.boundLayerLastEdited = layer.lastEdited
        slot.attachments.add(attachment)
        
        attachment.isCropped = false
        attachment.shiftAfterCrop = true
        
        updateAttachmentFromLayer(attachment, layer, layeredImage)
        centerAttachment(attachment)
    }
    
    private fun updateAttachmentFromLayer(
        attachment: Attachment,
        layer: Layer, layeredImage: LayeredImage
    ) {
        val newFile = attachment.imageReq.getFile(assetRepo.assetsDir)
        val oldFile = layer.imageReq.getFile(assetRepo.assetsDir)
        oldFile.copyTo(newFile, true)
        
        attachment.width = layeredImage.width // !!!!!
        attachment.height = layeredImage.height
    }
    
    /** Coordinates are relative,
     * so this works only for new attachments! (who are not attached anywhere) */
    private fun centerAttachment(attachment: Attachment) {
        attachment.jsonData = attachment.jsonData.safeJson().apply {
            put("x", -attachment.width / 2)
            put("y", -attachment.height / 2)
        }.toString()
    }
    
    fun mergeSlotsWithoutDelete(slotToClear: Slot, slotToUpdate: Slot) {
        realm.transaction {
            if (slotToClear.attachments.isNotEmpty()) {
                slotToUpdate.selectedAttachmentPosition =
                    slotToUpdate.attachments.size + slotToClear.selectedAttachmentPosition
            }
            
            slotToClear.attachments.toList().forEach { attachment ->
                reattachAttachment(attachment, slotToClear, slotToUpdate)
            }
        }
    }
    
    /** Keeps only first attachment of the slot, another are moved to new slots.
     * Returns added slots. */
    fun dismantleSlot(spine: Spine, slot: Slot): List<Slot> {
        if (slot.attachments.size < 2) {
            return emptyList()
        }
        val indexToInsert = spine.slots.indexOf(slot) + 1
        if (indexToInsert == 0) {
            throw IllegalStateException("Spine doesn't contain the dismantled slot.")
        }
        
        return realm.transaction {
            slot.selectedAttachmentPosition = 0
            val attachmentsToChange = slot.attachments.drop(1)
            slot.attachments.removeAll(attachmentsToChange)
            val newSlots = attachmentsToChange
                .map { attachment ->
                    val newSlot = Slot.init(realm, spine.key())
                    newSlot.name = attachment.name
                    reattachAttachment(attachment, slot, newSlot)
                    newSlot
                }
            spine.slots.addAll(indexToInsert, newSlots)
            newSlots
        }
    }
    
    
    /** Renames the image and moves attachment to the end of new slot. */
    private fun reattachAttachment(attachment: Attachment, oldSlot: Slot, newSlot: Slot) {
        val oldImage = attachment.imageReq
        attachment.ownerKey = newSlot.key()
        val newImage = ImageFile.init(realm, attachment.key())
        
        val oldFile = oldImage.getFile(assetRepo.assetsDir)
        val newFile = newImage.getFile(assetRepo.assetsDir)
        oldFile.renameTo(newFile)
        
        attachment.image = newImage
        // doesn't deletes the file
        oldImage.cascadeDelete()
        
        oldSlot.attachments.remove(attachment)
        newSlot.attachments.add(attachment)
    }
}