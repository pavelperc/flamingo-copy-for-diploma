package com.flamingo.animator.repository

import android.content.Context
import io.realm.Realm
import io.realm.RealmConfiguration
import java.io.File


/** Works with local files.
 * Should be created in Application class.
 * And uses application context to access local storage. */
class Repository(private val applicationContext: Context) {
    
    val cacheDir: File
        get() = applicationContext.cacheDir
    /** Directory with all quest projects. */
    val projectsDir: File
        get() = applicationContext.filesDir.resolve("projects").apply { mkdirs() }
    
    fun getProjectDir(projectName: String) = projectsDir.resolve(projectName).apply { mkdirs() }
    
    /** Creates or opens a database in project folder. Don't forget to close it onDestroy!!! */
    fun initRealmForProject(projectName: String): Realm {
        val config = RealmConfiguration.Builder()
            .directory(getProjectDir(projectName))
            .name("database.realm")
            .schemaVersion(0)
//            .deleteRealmIfMigrationNeeded()
            .build()
        
        return Realm.getInstance(config)
    }
}


class RealmNotFoundId(what: String, id: Long) :
    IllegalArgumentException("Not found $what with id $id")

