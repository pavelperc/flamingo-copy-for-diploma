package com.flamingo.animator.repository

import com.flamingo.animator.model.GameConfig
import com.flamingo.animator.utils.realmUtils.transaction
import org.json.JSONException
import org.json.JSONObject
import java.io.Closeable

/**
 * Helper class to work with [realm] database, bound to [projectName] project.
 * Should be closed with [close] on Activity destroy!!!!
 */
class ProjectRepo(
    val repo: Repository,
    internal val projectName: String
) : Closeable {
    val realm = repo.initRealmForProject(projectName)
    
    override fun close() {
        realm.close()
    }
    
    
    val gameConfig by lazy {
        realm.where(GameConfig::class.java).findFirst()
            ?: realm.transaction { realm.createObject(GameConfig::class.java) }
    }
    
    val projectSettings by lazy {
        try {
            JSONObject(gameConfig.projectSettingsJson)
        } catch (e: JSONException) {
            JSONObject()
        }
    }
    
    fun saveProjectSettings() {
        gameConfig.transaction {
            projectSettingsJson = projectSettings.toString()
        }
    }
    
}