package com.flamingo.animator.editors.drawUtils

import android.graphics.BitmapFactory
import android.graphics.Color
import com.flamingo.animator.model.Animation
import com.flamingo.animator.repository.AssetRepo
import java.io.File
import java.io.FileOutputStream

object GifSaver {
    // https://stackoverflow.com/questions/16331437/how-to-create-an-animated-gif-from-jpegs-in-android-development/46431425#46431425
    fun saveGif(anim: Animation, outputFile: File, assetRepo: AssetRepo) {
        val encoder = AnimatedGifEncoder()
        encoder.setDelay((1000 / anim.fps).toInt())
        encoder.setRepeat(0)
        encoder.setTransparent(Color.TRANSPARENT)
        
        encoder.start(FileOutputStream(outputFile))
        anim.frames
            .map { frame -> frame.imageReq.builtImageReq.getFile(assetRepo.assetsDir) }
            .forEach { file ->
                val bitmap = BitmapFactory.decodeFile(file.toString())
                encoder.addFrame(bitmap)
            }
        encoder.finish()
    }
}