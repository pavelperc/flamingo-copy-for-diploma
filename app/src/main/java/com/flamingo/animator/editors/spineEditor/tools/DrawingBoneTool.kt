package com.flamingo.animator.editors.spineEditor.tools

import android.view.MotionEvent
import com.flamingo.animator.editors.drawingEditor.tools.Tool
import com.flamingo.animator.editors.spineEditor.SpineSurfaceView
import com.flamingo.animator.editors.spineEditor.dataHolders.SlotContainer
import com.flamingo.animator.model.spine.Bone
import org.jetbrains.anko.toast

/** Allows to create bones. */
class DrawingBoneTool(val surfaceView: SpineSurfaceView) : BoneTool {
    override val toolType = BoneToolType.DRAW_BONE
    
    private val bonesContainer get() = surfaceView.bonesContainer
    private val boneSelector get() = surfaceView.boneToolsContainer.boneSelector
    
    
    /** Touch start in screen coords. */
    private var startX = 0f
    private var startY = 0f
    
    private var startTime = 0L
    
    var selectedBone: Bone? = null
    
    val scaleHandler get() = surfaceView.scaleHandler
    
    val slotSelector = surfaceView.slotToolsContainer.slotSelector
    
    /** Slot, that lies under the new bone start point. */
    var connectedSlotContainer: SlotContainer? = null
    
    private var newBone = Bone()
    
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                startX = event.x
                startY = event.y
                startTime = System.currentTimeMillis()
                selectedBone = bonesContainer.selectedBone
                
                connectedSlotContainer = slotSelector.findSelectedSlot(
                    event.x, event.y,
                    ignoredSlot = selectedBone?.slotContainer
                )
            }
            MotionEvent.ACTION_MOVE -> {
                val timeDelta = System.currentTimeMillis() - startTime
                if (timeDelta < Tool.CLICK_WAIT_TIME) {
                    return true
                }
                // if not root and not selected, return
                if (selectedBone == null && bonesContainer.root != null) {
                    return true
                }
                
                connectNewBone(event, selectedBone)
                surfaceView.updateScreen()
                // in case of cancel
                removeNewBone(selectedBone)
            }
            
            MotionEvent.ACTION_UP -> {
                val timeDelta = System.currentTimeMillis() - startTime
                if (timeDelta < Tool.CLICK_WAIT_TIME) {
                    // select bone
                    bonesContainer.selectedBone = boneSelector.findSelectedBone(startX, startY)
                    surfaceView.updateScreen()
                    return true
                }
                // add bone
                
                // if not root and not selected, return
                if (selectedBone == null && bonesContainer.root != null) {
                    surfaceView.updateScreen()
                    surfaceView.activity.toast("Select a parent bone before drawing.")
                    return true
                }
                
                connectNewBone(event, selectedBone)
                newBone = Bone()
                surfaceView.updateScreen()
            }
        }
        return true
    }
    
    /** Updates new bone coords and connects it. */
    private fun connectNewBone(
        event: MotionEvent, parent: Bone?
    ) {
        val line = floatArrayOf(startX, startY, event.x, event.y)
        scaleHandler.inverseDrawMatrix.mapPoints(line)
        
        newBone.setAbsoluteByStartEnd(line[0], line[1], line[2], line[3])
        newBone.updateFromAbsolute(false)
        newBone.parent = parent
        
        parent?.children?.add(newBone)
        
        // selected bone is newBone
        bonesContainer.selectedBone = newBone
        
        if (bonesContainer.root == null) {
            bonesContainer.root = newBone
        }
        connectWithSlotContainer()
        
    }
    
    private fun removeNewBone(parent: Bone?) {
        parent?.children?.remove(newBone)
        bonesContainer.selectedBone = selectedBone
        if (bonesContainer.root == newBone) {
            bonesContainer.root = null
        }
        disconnectFromSlotContainer()
    }
    
    private fun connectWithSlotContainer() {
        // connect newBone with slotContainer
        val slotContainer = connectedSlotContainer
        if (slotContainer != null && slotContainer.bone == null) {
            slotContainer.bone = newBone
            newBone.slotContainer = slotContainer
            slotContainer.updateFromAbsolute(false)
        }
    }
    
    private fun disconnectFromSlotContainer() {
        // connect newBone with slotContainer
        val slotContainer = connectedSlotContainer
        if (slotContainer != null && slotContainer.bone == newBone) {
            slotContainer.bone = null
            newBone.slotContainer = null
            slotContainer.updateFromAbsolute(false)
        }
    }
}

