package com.flamingo.animator.editors.spineEditor.dataHolders

import com.flamingo.animator.editors.spineEditor.SpineSurfaceView
import com.flamingo.animator.utils.realmUtils.transaction

/** Holds all loaded bitmaps. And bones. And animations. */
class SpineDataHolder(
    val surfaceView: SpineSurfaceView
) {
    val spine get() = surfaceView.spine
    val assetRepo = surfaceView.activity.assetRepo
    
    val activity get() = surfaceView.activity
    
    val spineRepo get() = activity.spineRepo
    
    val animationDataHolder = SpineAnimationDataHolder(this)
    
    val slotContainers = spine.slots.map {
        SlotContainer(it, this)
    }.toMutableList()
    
    
    val visibleSlotContainers get() = slotContainers.asSequence().filter { it.isVisible }
    
    var selectedSlot: SlotContainer? = null
        set(value) {
            field = value
            surfaceView.onSlotSelectionChanged()
        }
    
    var secondSelectedSlot: SlotContainer? = null
    
    val bonesContainer = BonesContainer(surfaceView)
    
    init {
        bonesContainer.loadBones()
        loadBoneAndSlotMappings()
    }
    
    private fun loadBoneAndSlotMappings() {
        val bonesByIds = bonesContainer.traverseBones().map { it.id to it }.toMap()
        slotContainers.forEach { slotContainer ->
            slotContainer.bone = bonesByIds[slotContainer.boneIdOnLoad]
            slotContainer.bone?.slotContainer = slotContainer
            slotContainer.updateAbsolute(false)
        }
    }
    
    /** Saved to database all json data. */
    fun saveAll() {
        bonesContainer.saveBones()
        assetRepo.realm.transaction {
            slotContainers.forEach { it.save() }
        }
        animationDataHolder.saveAll()
    }
    
    fun deleteSlot(slotContainer: SlotContainer) {
        slotContainers.remove(slotContainer)
        slotContainer.bone?.slotContainer = null
        slotContainer.bone = null
        spineRepo.transaction {
            deleteSlot(spine, slotContainer.slot)
        }
        
        if (slotContainer == selectedSlot) {
            selectedSlot = null
        }
    }
    
    fun mergeSlots(slotToDelete: SlotContainer, slotToUpdate: SlotContainer) {
        // we need to move all AttachmentContainers, not just attachments!
        // rename all files, that are connected to attachments
        // move all attachments to new slot
        // move all attachmentContainers to new slotContainer
        // delete old slot and slotContainer
        // change selected attachment to the visible one in the first slot!
        // update selection in slotContainer
        // update relative coordinates!!!
        
        
        if (slotToDelete.attachmentContainers.isEmpty()) {
            return
        }
        spineRepo.mergeSlotsWithoutDelete(slotToDelete.slot, slotToUpdate.slot)
        val newPosition =
            slotToUpdate.attachmentContainers.size + slotToDelete.selectedAttachmentPosition
        
        val attachmentContainersToMove = slotToDelete.attachmentContainers.toList()
        slotToUpdate.attachmentContainers.addAll(attachmentContainersToMove)
        slotToDelete.attachmentContainers.clear()
        
        attachmentContainersToMove.forEach {
            it.updateCachedImageFile()
            it.parent = slotToUpdate
            it.updateFromAbsolute()
        }
        deleteSlot(slotToDelete)
        
        slotToUpdate.updateSelectedAttachmentPositionWithAbsolute(newPosition)
        selectedSlot = slotToUpdate
    }
    
    fun dismantleSlot(slotContainer: SlotContainer) {
        val indexToInsert = slotContainers.indexOf(slotContainer) + 1
        if (indexToInsert == 0) {
            return
        }
        if (slotContainer.attachmentContainers.size < 2) {
            return
        }
        val newSlots = spineRepo.dismantleSlot(spine, slotContainer.slot)
        
        val attachmentContainersToMove = slotContainer.attachmentContainers.drop(1)
        slotContainer.attachmentContainers.removeAll(attachmentContainersToMove)
        slotContainer.updateSelectedAttachmentPositionWithAbsolute(0)
        
        val newSlotContainers = newSlots.zip(attachmentContainersToMove)
            .map { (slot, attachmentContainer) ->
                // attachmentContainer parent is updated in constructor
                SlotContainer(slot, this, cachedAttachmentContainers = listOf(attachmentContainer))
            }
        attachmentContainersToMove.forEach {
            it.updateFromAbsolute()
        }
        slotContainers.addAll(indexToInsert, newSlotContainers)
        selectedSlot = newSlotContainers.last()
    }
}

