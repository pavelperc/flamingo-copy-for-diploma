package com.flamingo.animator.editors.spineEditor.tools

import android.graphics.Color
import android.graphics.Matrix
import android.graphics.RectF
import com.flamingo.animator.editors.spineEditor.SpineSurfaceView
import com.flamingo.animator.editors.spineEditor.dataHolders.AttachmentContainer
import com.flamingo.animator.editors.spineEditor.dataHolders.SlotContainer

class SlotSelector(val surfaceView: SpineSurfaceView) {
    val scaleHandler get() = surfaceView.scaleHandler
    val skinDataHandler get() = surfaceView.spineDataHolder
    
    private val matrix = Matrix()
    private val border = RectF()
    private val point = floatArrayOf(0f, 0f)
    
    /** [x] [y] in screen coords. Returns selected slot. */
    fun findSelectedSlot(
        x: Float, y: Float,
        preferSelectedSlotBoundingBox: Boolean = false,
        ignoredSlot: SlotContainer? = null
    ): SlotContainer? {
        // convert to field coords
        val point = floatArrayOf(x, y)
        scaleHandler.inverseDrawMatrix.mapPoints(point)
        
        val oldSelectedSlot = skinDataHandler.selectedSlot
        if (preferSelectedSlotBoundingBox && oldSelectedSlot != null) {
            if (oldSelectedSlot.selectedAttachment.isPointInSlot(
                    point[0], point[1], onlyBoundingBox = true
                )
            ) {
                return oldSelectedSlot
            }
        }
        
        val selectedSlot = skinDataHandler.visibleSlotContainers.asSequence()
            .filter { it != ignoredSlot }
            .lastOrNull { slotContainer ->
                slotContainer.selectedAttachment.isPointInSlot(point[0], point[1])
            }
        return selectedSlot
    }
    
    
    /** In field coords */
    private fun AttachmentContainer.isPointInSlot(
        x: Float,
        y: Float,
        onlyBoundingBox: Boolean = false
    ): Boolean {
        matrix.reset()
        applyTransform(matrix)
        matrix.invert(matrix) // from field coords to inside slot coords
        point[0] = x
        point[1] = y
        matrix.mapPoints(point)
        
        border.set(0f, 0f, width.toFloat(), height.toFloat())
        
        return border.contains(point[0], point[1]) && (onlyBoundingBox ||
                Color.alpha(bitmap.getPixel(point[0].toInt(), point[1].toInt())) > 0)
    }
    
    
    private fun AttachmentContainer.applyTransform(matrix: Matrix) {
        matrix.postRotate(absoluteRotation)
        matrix.postTranslate(absoluteX, absoluteY)
    }
}