package com.flamingo.animator.editors.spineEditor

import android.graphics.*
import com.flamingo.animator.editors.drawUtils.scale
import com.flamingo.animator.model.spine.Bone
import org.jetbrains.anko.dip

class BoneDrawer(val surfaceView: SpineSurfaceView) {
    
    private val scaleHandler get() = surfaceView.scaleHandler
    
    companion object {
        private const val UNSELECTED_STROKE_WIDTH = 1f
        private const val SELECTED_STROKE_WIDTH = 3f
        
        private val BONE_COLOR = Color.MAGENTA
        private val SELECTED_BONE_COLOR = Color.RED
        private val TIMESTAMP_EDITED_COLOR = Color.parseColor("#8650bd") // violet
        private val BORDER_COLOR = Color.BLACK
        private val GHOST_BORDER_COLOR = Color.parseColor("#A09F9F9F")
        
        
    }
    
    val boneFillPaint = Paint().apply {
        style = Paint.Style.FILL
    }
    val boneStrokePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
    }
    
    
    private val boneConnectorPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        strokeWidth = dip(1).toFloat()
        pathEffect = DashPathEffect(
            floatArrayOf(dip(4).toFloat(), dip(2).toFloat()), 0f
        )
        color = Color.RED
    }
    
    private val ghostBoneConnectorPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        strokeWidth = dip(1).toFloat()
        pathEffect = DashPathEffect(
            floatArrayOf(dip(4).toFloat(), dip(2).toFloat()), 0f
        )
        color = Color.parseColor("#A09F9F9F")
    }
    
    // half width of the bone
    private val boneSize = dip(4).toFloat()
    private val bonePath = Path()
    
    /** All in screen coords!! */
    fun drawBone(
        canvas: Canvas,
        bone: Bone,
        selected: Boolean = false,
        timestampEdited: Boolean = false,
        ghostMode: Boolean = false,
        outline: Boolean = false
    ) {
        val points = floatArrayOf(
            bone.absoluteX, bone.absoluteY,
            bone.parent?.absoluteX ?: 0f,
            bone.parent?.absoluteY ?: 0f
        )
        scaleHandler.drawMatrix.mapPoints(points)
        
        // compute coordinates in screen scale
        val startX = points[0]
        val startY = points[1]
        val parentX = points[2]
        val parentY = points[3]
        val length = bone.length * scaleHandler.drawMatrix.scale
        val angle = bone.absoluteRotation
        
        canvas.save()
        canvas.translate(startX, startY)
        canvas.rotate(angle)
        
        fun drawContour(paint: Paint, size: Float) {
            canvas.drawCircle(0f, 0f, size, paint)
            // draw a necktie
            bonePath.reset()
            bonePath.moveTo(2 * size, -size)
            bonePath.lineTo(1 * size, 0f)
            bonePath.lineTo(2 * size, size)
            bonePath.lineTo(length, 0f)
            bonePath.close()
            canvas.drawPath(bonePath, paint)
        }
        
        
        boneStrokePaint.strokeWidth =
            if (selected) SELECTED_STROKE_WIDTH else UNSELECTED_STROKE_WIDTH
        
        val color =
            if (ghostMode) GHOST_BORDER_COLOR
            else if (timestampEdited) TIMESTAMP_EDITED_COLOR
            else if (selected) SELECTED_BONE_COLOR
            else BONE_COLOR
        
        if (outline || ghostMode) {
            boneStrokePaint.color = color
            drawContour(boneStrokePaint, boneSize)
        } else {
            boneStrokePaint.color = BORDER_COLOR
            boneFillPaint.color = color
            drawContour(boneFillPaint, boneSize)
            drawContour(boneStrokePaint, boneSize)
        }
        
        canvas.restore()
        
        // draw dashed line connector from parent
        if (bone.parent != null) {
            if (ghostMode) {
                canvas.drawLine(parentX, parentY, startX, startY, ghostBoneConnectorPaint)
            } else {
                canvas.drawLine(parentX, parentY, startX, startY, boneConnectorPaint)
            }
        }
    }
    
    private fun dip(dp: Int) = surfaceView.context.dip(dp)
}