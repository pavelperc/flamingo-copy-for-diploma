package com.flamingo.animator.editors.drawingEditor.dataHolders

import android.graphics.*
import com.flamingo.animator.editors.drawUtils.makeFilledBitmap
import com.flamingo.animator.model.Layer
import com.flamingo.animator.model.LayeredImage
import com.flamingo.animator.repository.AssetRepo
import com.flamingo.animator.utils.commonUtils.measureTime
import com.flamingo.animator.utils.commonUtils.move
import com.flamingo.animator.utils.realmUtils.transaction
import java.io.FileOutputStream

/** Wrapper for [LayeredImage]. */
class ImageContainer(
    val layeredImage: LayeredImage,
    val backgroundColor: Int,
    val assetRepo: AssetRepo
) : DrawingContainer {
    val layerContainers = mutableListOf<LayerContainer>()
    val layers: List<Layer>
        get() = layeredImage.layers
    
    
    override val currentImageContainer: ImageContainer
        get() = this
    
    var selectedLayerPosition: Int = layerContainers.size - 1
    
    init {
        layerContainers += layeredImage.layers.map {
            LayerContainer(
                it,
                assetRepo.assetsDir
            )
        }
        
        val lastVisibleIndex = layers.indexOfLast { it.isVisible }
        selectedLayerPosition = if (lastVisibleIndex == -1) layers.size - 1 else lastVisibleIndex
    }
    
    val selectedLayerContainer: LayerContainer
        get() = layerContainers[selectedLayerPosition]
    
    override val restorableCanvas: RestorableCanvas
        get() = selectedLayerContainer.restorableCanvas
    
    override val width: Int
        get() = layeredImage.width
    override val height: Int
        get() = layeredImage.height
    
    
    /** Adds a layer to the end. */
    fun addLayer(): LayerContainer {
        val predefinedBitmap = makeFilledBitmap(width, height, Color.TRANSPARENT)
        val layer = assetRepo.transaction {
            addNewLayer(layeredImage, width, height, layerContainers.size, predefinedBitmap)
        }
        val layerContainer = LayerContainer(
            layer,
            assetRepo.assetsDir,
            predefinedBitmap
        )
        layerContainers.add(layerContainers.size, layerContainer)
        selectedLayerPosition = layerContainers.size - 1
        return layerContainer
    }
    
    /** Adds a layer to the end. */
    fun duplicateLayer(position: Int): LayerContainer {
        val layer = assetRepo.transaction {
            duplicateLayer(layeredImage, position)
        }
        val layerContainer = LayerContainer(
            layer,
            assetRepo.assetsDir
        )
        layerContainers.add(position + 1, layerContainer)
        selectedLayerPosition = position + 1
        return layerContainer
    }
    
    // we store a cached copy of built image for fast animation preview!!!
    // and semitransparent back frames
    
    /** Image with all layers. And sometimes with dirty bitmap instead some layer. */
    private val builtImageCached = Bitmap.createBitmap(
        layeredImage.width,
        layeredImage.height,
        Bitmap.Config.ARGB_8888
    )
    private val builtImageCanvas = Canvas(builtImageCached)
    
    /** [builtImage] may be empty or invalid. */
    private var isBuiltImageDirty = true
    
    
    fun invalidateBuiltImageCache() {
        isBuiltImageDirty = true
    }
    
    /** Returns not dirty built image. */
    val builtImage: Bitmap
        get() = if (isBuiltImageDirty) measureTime("Build valid image") { buildFrameImage() } else builtImageCached
    
    private fun saveBuiltImage() {
        val file = layeredImage.builtImageReq.getFile(assetRepo.assetsDir)
        builtImage.compress(Bitmap.CompressFormat.PNG, 100, FileOutputStream(file))
    }
    
    /** Layers (id to isVisible) pairs. Saves the order and visibility. */
    private var lastSavedLayersState = layers.map { it.id to it.isVisible }
    
    
    fun getUnsavedLayerContainers() = layerContainers.filter { !it.isSaved }
    
    override fun saveAll(): Int {
        val unsavedLayers = getUnsavedLayerContainers()
        var savedCount = unsavedLayers.map { it.saveImage() }.sum()
        
        // save built image if need
        val currentLayerState = layers.map { it.id to it.isVisible }
        // if changed layers itself, layer visibility, order or count
        if (savedCount > 0 || currentLayerState != lastSavedLayersState) {
            saveBuiltImage()
            savedCount++
            lastSavedLayersState = currentLayerState
        }
        
        return savedCount
    }
    
    /** Draw all visible layers before the current. [screenRect] is a rectangle, where to copy the image. */
    fun drawBackLayers(canvas: Canvas, screenRect: Rect) {
        layerContainers.take(selectedLayerPosition).forEach { layerContainer ->
            if (layerContainer.layer.isVisible) {
                canvas.drawBitmap(
                    layerContainer.restorableCanvas.mainBitmap, screenRect, screenRect, null
                )
            }
        }
    }
    
    /** Draw all visible layers after the current. [screenRect] is a rectangle, where to copy the image. */
    fun drawFrontLayers(canvas: Canvas, screenRect: Rect) {
        layerContainers.drop(selectedLayerPosition + 1).forEach { layerContainer ->
            if (layerContainer.layer.isVisible) {
                canvas.drawBitmap(
                    layerContainer.restorableCanvas.mainBitmap, screenRect, screenRect, null
                )
            }
        }
    }
    
    /** Collect all layers to image and update built image cache.*/
    private fun buildFrameImage(): Bitmap {
        builtImageCanvas.drawColor(backgroundColor, PorterDuff.Mode.SRC)
        
        isBuiltImageDirty = false
        
        for (layerContainer in layerContainers) {
            if (layerContainer.layer.isVisible) {
                builtImageCanvas.drawBitmap(
                    layerContainer.restorableCanvas.mainBitmap, 0f, 0f, null
                )
            }
        }
        return builtImageCached
    }
    
    fun moveLayer(pos1: Int, pos2: Int) {
        val oldSelectedLayer = selectedLayerContainer
        layerContainers.move(pos1, pos2)
        layeredImage.transaction { layers.move(pos1, pos2) }
        
        selectedLayerPosition = layerContainers.indexOf(oldSelectedLayer)
    }
    
    fun removeLayer(position: Int) {
        check(layerContainers.size > 1) { "Tried to remove the last layer in the image." }
        layerContainers.removeAt(position)
        val layer = layeredImage.transaction {
            layers.removeAt(position)
        }
        assetRepo.removeLayer(layer)
        
        if (selectedLayerPosition > position || selectedLayerPosition == layerContainers.size) {
            selectedLayerPosition--
        }
    }
    
    /** Removes the src!!! And prints it over the dst. */
    fun mergeLayers(srcPosition: Int, dstPosition: Int) {
        val srcContainer = layerContainers[srcPosition]
        val dstContainer = layerContainers[dstPosition]
        dstContainer.restorableCanvas.performAction(
            BitmapAction(srcContainer.restorableCanvas.mainBitmap)
        )
        removeLayer(srcPosition)
    }
    
    companion object {
        private val invisibleLayerPaint = Paint().apply {
            alpha = 50
        }
    }
}