package com.flamingo.animator.editors.drawingEditor.dataHolders

import android.graphics.*
import androidx.annotation.ColorInt
import com.flamingo.animator.editors.drawUtils.drawBitmap
import com.flamingo.animator.editors.drawUtils.pressurePath.PressurePathHelper
import com.flamingo.animator.editors.drawUtils.pressurePath.PressurePoint
import java.util.*


interface RestorableAction {
    fun perform(canvas: Canvas, bitmap: Bitmap)
}

class PathAction(val path: Path, val paint: Paint) : RestorableAction {
    override fun perform(canvas: Canvas, bitmap: Bitmap) {
        canvas.drawPath(path, paint)
    }
}

class WidthPathAction(
    val points: List<PressurePoint>,
    val paint: Paint
) : RestorableAction {
    override fun perform(canvas: Canvas, bitmap: Bitmap) {
        PressurePathHelper.drawWithWidth(points, paint, canvas)
    }
}

class ColorAction(@ColorInt val color: Int) : RestorableAction {
    override fun perform(canvas: Canvas, bitmap: Bitmap) {
        canvas.drawColor(color, PorterDuff.Mode.SRC)
    }
}

class BitmapAction(private val newBitmap: Bitmap) : RestorableAction {
    override fun perform(canvas: Canvas, bitmap: Bitmap) {
        canvas.drawBitmap(newBitmap)
    }
}



class RestorableCanvas(
    val width: Int,
    val height: Int,
    val bufferSize: Int = DEFAULT_BUFFER_SIZE,
    canvasInit: (Canvas) -> Unit = {}
) {
    companion object {
        const val DEFAULT_BUFFER_SIZE = 50
        private val identityMatrix = Matrix()
    }
    
    /** Counts all preformed restorable actions in current session. Can be decremented. */
    var actionCounter: Int = 0
        private set
    
    val mainBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    // you can not write to canvas, you can only write to dirtyChanges. So it is private.
    private val canvas = Canvas(mainBitmap)
    
    /** This bitmap is used to store the state before the last undo operation. */
    private val backupBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    private val backupCanvas = Canvas(backupBitmap)
    private val undoActions = LinkedList<RestorableAction>()
    private val redoActions = LinkedList<RestorableAction>()
    
    init {
        require(bufferSize > 1) { "BufferSize in RestorableCanvas should be > 1, but it is $bufferSize." }
        canvasInit(canvas)
        canvasInit(backupCanvas)
    }
    
    /** Perform action that will be saved to history.
     * For temp canvas changes you can use [dirtyChangesCanvas]. */
    fun performAction(action: RestorableAction) {
        undoActions.addLast(action)
        action.perform(canvas, mainBitmap)
        redoActions.clear()
        if (undoActions.size > bufferSize) {
            val oldestAction = undoActions.removeFirst()
            oldestAction.perform(backupCanvas, mainBitmap)
        }
        actionCounter++
    }
    
    
    fun undo() {
        if (undoActions.isEmpty()) {
            return
        }
        val lastAction = undoActions.removeLast()
        redoActions.addFirst(lastAction)
        
        // copy the last saved state
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR) // if backup is transparent
        canvas.drawBitmap(
            backupBitmap,
            identityMatrix, null
        )
        
        undoActions.forEach { action ->
            action.perform(canvas, mainBitmap)
        }
        
        actionCounter--
    }
    
    fun redo() {
        if (redoActions.isEmpty()) {
            return
        }
        
        val restoredAction = redoActions.removeFirst()
        restoredAction.perform(canvas, mainBitmap)
        undoActions.addLast(restoredAction)
        actionCounter++
    }
    
    
    val undoSize: Int
        get() = undoActions.size
    val redoSize: Int
        get() = redoActions.size
    
    
    val lastAction: RestorableAction?
        get() = undoActions.lastOrNull()
}





