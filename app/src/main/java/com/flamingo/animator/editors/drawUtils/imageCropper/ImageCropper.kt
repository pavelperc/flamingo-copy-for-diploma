package com.flamingo.animator.editors.drawUtils.imageCropper

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Rect

object ImageCropper {
    
    /** Returns null, if already cropped (and [cropAlreadyCropped] is false) or the image is transparent. */
    fun cropImage(bitmap: Bitmap, cropAlreadyCropped: Boolean = false): Rect? {
        val width = bitmap.width
        val height = bitmap.height
        
        val pixels = IntArray(width * height)
        bitmap.getPixels(pixels, 0, width, 0, 0, width, height)
        
        var trimTop = -1
        var trimBottom = -1
        var trimLeft = -1
        var trimRight = -1
        
        f@ for (row in 0 until height) {
            for (col in 0 until width) {
                val pixel = pixels[row * width + col]
                if (Color.alpha(pixel) != 0) {
                    trimTop = row
                    break@f
                }
            }
        }
        if (trimTop == -1) {
            return null
        }
        
        f@ for (row in height - 1 downTo 0) {
            for (col in 0 until width) {
                val pixel = pixels[row * width + col]
                if (Color.alpha(pixel) != 0) {
                    trimBottom = row
                    break@f
                }
            }
        }
        
        f@ for (col in 0 until width) {
            for (row in trimTop..trimBottom) {
                val pixel = pixels[row * width + col]
                if (Color.alpha(pixel) != 0) {
                    trimLeft = col
                    break@f
                }
            }
        }
        
        f@ for (col in width - 1 downTo 0) {
            for (row in trimTop..trimBottom) {
                val pixel = pixels[row * width + col]
                if (Color.alpha(pixel) != 0) {
                    trimRight = col
                    break@f
                }
            }
        }
        if (trimLeft == 0 && trimTop == 0 && trimRight == width - 1 && trimBottom == height - 1
            && !cropAlreadyCropped) {
            return null
        }
        return Rect(trimLeft, trimTop, trimRight + 1, trimBottom + 1)
    }
}

