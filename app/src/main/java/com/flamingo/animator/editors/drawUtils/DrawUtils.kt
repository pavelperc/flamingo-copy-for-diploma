package com.flamingo.animator.editors.drawUtils

import android.graphics.*
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import java.io.File
import java.io.FileOutputStream
import kotlin.math.atan2
import kotlin.math.roundToInt


/** Common values for all matrices. May work bad in separate threads. */
private val matrixValues = FloatArray(9)

// TODO is it safe?

val Matrix.values: FloatArray
    get() {
        getValues(matrixValues)
        return matrixValues
    }

// TODO is scale formula right???
// better solution with skew: https://stackoverflow.com/questions/12256854/get-the-rotate-value-from-matrix-in-android
val Matrix.scale get() = values[Matrix.MSCALE_X]

val Matrix.rotate: Int
    get() {
        val v = values
        return (atan2(
            v[Matrix.MSKEW_X].toDouble(),
            v[Matrix.MSCALE_X].toDouble()
        ) * (180 / Math.PI)).roundToInt();
    }
val Matrix.x get() = values[Matrix.MTRANS_X]
val Matrix.y get() = values[Matrix.MTRANS_Y]


fun makeFilledBitmap(width: Int, height: Int, fillColor: Int): Bitmap {
    val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    canvas.drawColor(fillColor)
    return bitmap
}

fun saveFilledImageToFile(file: File, width: Int, height: Int, fillColor: Int): Bitmap {
    val bitmap = makeFilledBitmap(width, height, fillColor)
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, FileOutputStream(file))
    return bitmap
}

/** Draw the transparent color. */
fun Canvas.clear(color: Int = Color.TRANSPARENT) {
    drawColor(color, PorterDuff.Mode.SRC)
}

/** Draw the [color] inside the [rect]. */
fun Canvas.clear(rect: Rect, color: Int = Color.TRANSPARENT) {
    save()
    clipRect(rect)
    drawColor(color, PorterDuff.Mode.SRC)
    restore()
}

fun View.setRippleBackground() {
    val outValue = TypedValue()
    context.theme.resolveAttribute(
        android.R.attr.selectableItemBackgroundBorderless,
        outValue,
        true
    )
    setBackgroundResource(outValue.resourceId)
}

fun RectF.expand(offset: Float) {
    left -= offset
    top -= offset
    right += offset
    bottom += offset
}

val MotionEvent.isStylus get() = this.getToolType(0) == MotionEvent.TOOL_TYPE_STYLUS
val MotionEvent.isNotStylus get() = this.getToolType(0) != MotionEvent.TOOL_TYPE_STYLUS

/** Draws [bitmap] without any transform. */
fun Canvas.drawBitmap(bitmap: Bitmap) {
    drawBitmap(bitmap, 0f, 0f, null)
}

/** Draws [bitmap] with transform matrix. */
fun Canvas.drawBitmap(bitmap: Bitmap, matrix: Matrix) {
    drawBitmap(bitmap, matrix, null)
}