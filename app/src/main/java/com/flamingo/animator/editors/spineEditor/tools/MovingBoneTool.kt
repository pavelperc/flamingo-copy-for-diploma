package com.flamingo.animator.editors.spineEditor.tools

import android.view.MotionEvent
import com.flamingo.animator.editors.drawingEditor.tools.Tool
import com.flamingo.animator.editors.spineEditor.SpineSurfaceView
import com.flamingo.animator.editors.spineEditor.dataHolders.SlotContainer
import com.flamingo.animator.model.spine.Bone

/** Supports bone shift, length change and rotation. */
class MovingBoneTool(
    val surfaceView: SpineSurfaceView,
    val moveOneBoneMode: Boolean = false,
    val animationMode: Boolean = false
) : BoneTool {
    override val toolType =
        if (moveOneBoneMode) BoneToolType.MOVE_ONE_BONE else BoneToolType.MOVE_BONE_TREE
    
    private val animationDataHolder get() = surfaceView.spineDataHolder.animationDataHolder
    private val timelineAdapter get() = surfaceView.activity.timelineAdapter
    
    private var startTime = 0L
    var startX = 0f
    var startY = 0f
    
    /** This cache holds the bone coordinates before moving.
     * They are restored after each view update, because of the risk of zooming start. */
    private var boneToMoveCache = Bone()
    
    var boneToMove: Bone? = null
    var touchPart: BoneSelector.TouchPart = BoneSelector.TouchPart.Start
    
    // Start is moving, Middle is rotating, End is Rotating and changing length
    
    val bonesContainer get() = surfaceView.bonesContainer
    
    private val boneSelector get() = surfaceView.boneToolsContainer.boneSelector
    private val slotSelector = surfaceView.slotToolsContainer.slotSelector
    
    /** Slot, that lies under the bone start point in moveOneBone and touchPart.Start mode. */
    private var connectedSlotContainer: SlotContainer? = null
    
    private var oldSelectedBone: Bone? = null
    
    init {
        check(!(moveOneBoneMode && animationMode)) {
            "Can not be in animation and moveOneBone mode at the same time"
        }
    }
    
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                startX = event.x
                startY = event.y
                startTime = System.currentTimeMillis()
                oldSelectedBone = bonesContainer.selectedBone
                
                val touchResult = boneSelector.findSelectedBoneWithPart(startX, startY)
                if (touchResult != null) {
                    boneToMove = touchResult.first
                    touchPart = touchResult.second
                    boneToMoveCache.updateFrom(touchResult.first)
                } else {
                    boneToMove = null
                }
            }
            MotionEvent.ACTION_MOVE -> {
                val timeDelta = System.currentTimeMillis() - startTime
                if (timeDelta < Tool.CLICK_WAIT_TIME) {
                    return true
                }
                val bone = boneToMove ?: return true
                
                // update bone, draw and return back
                updateBoneFromTouch(bone, event)
                surfaceView.updateScreen()
                restoreBone(bone)
            }
            
            MotionEvent.ACTION_UP -> {
                val timeDelta = System.currentTimeMillis() - startTime
                if (timeDelta < Tool.CLICK_WAIT_TIME) {
                    // select bone
                    bonesContainer.selectedBone = boneToMove
                    surfaceView.updateScreen()
                    return true
                }
                val bone = boneToMove ?: return true
                // update bone and draw
                updateBoneFromTouch(bone, event)
                if (animationMode) {
                    animationDataHolder.createOrUpdateTimestampForBone(bone)
                    animationDataHolder.updateEditedBonesToHighlight()
                    timelineAdapter.updateItemOnTime(animationDataHolder.animationTime)
                }
                surfaceView.updateScreen()
            }
        }
        return true
    }
    
    private fun updateBoneFromTouch(bone: Bone, event: MotionEvent) {
        // drawn line
        val line = floatArrayOf(startX, startY, event.x, event.y)
        surfaceView.scaleHandler.inverseDrawMatrix.mapPoints(line)
        val initialAbsoluteRotation = bone.absoluteRotation
        when (touchPart) {
            BoneSelector.TouchPart.Start -> {
                // move bone
                bone.absoluteX += line[2] - line[0]
                bone.absoluteY += line[3] - line[1]
            }
            BoneSelector.TouchPart.Middle -> {
                // rotate bone
                bone.setAbsoluteByStartEnd(
                    bone.absoluteX, bone.absoluteY,
                    line[2], line[3], updateLength = false
                )
            }
            BoneSelector.TouchPart.End -> {
                // rotate bone and change length
                // change length only in one bone mode
                bone.setAbsoluteByStartEnd(
                    bone.absoluteX, bone.absoluteY,
                    line[2], line[3], updateLength = moveOneBoneMode
                )
            }
        }
        val newAbsoluteRotation = bone.absoluteRotation
        // normalize bone rotation
        if (newAbsoluteRotation - initialAbsoluteRotation > 180) {
            bone.absoluteRotation -= 360
        } else if (newAbsoluteRotation - initialAbsoluteRotation < -180) {
            bone.absoluteRotation += 360
        }
        
        bone.updateFromAbsolute(animationMode)
        
        if (moveOneBoneMode) {
            bonesContainer.selectedBone = bone
            bone.slotContainer?.updateFromAbsolute(animationMode)
            bone.children.forEach { child ->
                child.updateFromAbsolute(animationMode)
            }
        } else {
            bone.slotContainer?.updateAbsolute(animationMode)
            bone.updateAllChildren(animationMode)
        }
        
        if (moveOneBoneMode && touchPart == BoneSelector.TouchPart.Start
            && bone.slotContainer == null
        ) {
            connectedSlotContainer = slotSelector.findSelectedSlot(event.x, event.y)
            connectWithSlotContainer(bone)
        } else {
            connectedSlotContainer = null
        }
    }
    
    /** Restores the bone in case of action cancellation. */
    private fun restoreBone(bone: Bone) {
        bone.updateFrom(boneToMoveCache)
        if (moveOneBoneMode) {
            bonesContainer.selectedBone = oldSelectedBone
            bone.slotContainer?.updateFromAbsolute(animationMode)
            bone.children.forEach { child ->
                child.updateFromAbsolute(animationMode)
            }
        } else {
            bone.slotContainer?.updateAbsolute(animationMode)
            bone.updateAllChildren(animationMode)
        }
        if (moveOneBoneMode && touchPart == BoneSelector.TouchPart.Start) {
            disconnectFromSlotContainer(bone)
        }
    }
    
    private fun connectWithSlotContainer(bone: Bone) {
        val slotContainer = connectedSlotContainer
        if (slotContainer != null && slotContainer.bone == null) {
            slotContainer.bone = bone
            bone.slotContainer = slotContainer
            slotContainer.updateFromAbsolute(false)
        }
    }
    
    
    private fun disconnectFromSlotContainer(bone: Bone) {
        val slotContainer = connectedSlotContainer
        if (slotContainer != null && slotContainer.bone == bone) {
            slotContainer.bone = null
            bone.slotContainer = null
            slotContainer.updateFromAbsolute(false)
        }
    }
}