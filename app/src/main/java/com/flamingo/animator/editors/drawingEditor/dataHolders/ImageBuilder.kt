package com.flamingo.animator.editors.drawingEditor.dataHolders

import android.graphics.*
import com.flamingo.animator.editors.drawUtils.clear
import com.flamingo.animator.editors.drawUtils.drawBitmap
import com.flamingo.animator.editors.drawingEditor.ScaleHandler
import com.flamingo.animator.utils.commonUtils.measureTime

/** Helper class for building and caching all layers, the background image. */
class ImageBuilder(val drawDataHandler: DrawDataHandler) {
    // building the image:
    
    private val width: Int get() = drawDataHandler.width
    private val height: Int get() = drawDataHandler.height
    private val animContainer: AnimContainer? get() = drawDataHandler.animContainer
    private val imageContainer: ImageContainer get() = drawDataHandler.currentImageContainer
    private val currentRestorableCanvas: RestorableCanvas get() = drawDataHandler.currentRestorableCanvas
    private val scaleHandler: ScaleHandler get() = drawDataHandler.scaleHandler
    
    val dirtyChangesBitmap: Bitmap =
        Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    /** This canvas is used to write unsaved actions,
     * and be able to simply return to the last saved state.
     * May be dirty and incorrect at some time, so it should not be drawn by default.
     * Use [setDirtyCanvasCopyOfCurrentLayer] to start working with it. */
    val dirtyChangesCanvas: Canvas = Canvas(dirtyChangesBitmap)
    
    
    /** Bitmap of the screen size. Should contain all layers before current, including current.
     * Layers are scaled to screen size. */
    var screenBackBitmap: Bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)
        private set
    var screenBackCanvas: Canvas = Canvas(screenBackBitmap)
        private set
    
    fun updateScreenSize(screenWidth: Int, screenHeight: Int) {
        screenBackBitmap = Bitmap.createBitmap(screenWidth, screenHeight, Bitmap.Config.ARGB_8888)
        screenBackCanvas = Canvas(screenBackBitmap)
    }
    
    private val backgroundBitmapCache: Bitmap =
        Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    /** backgroundImage lightened. or transparent image. */
    val backgroundBitmap: Bitmap
        get() = if (isBackgroundBitmapValid) backgroundBitmapCache else updateBackgroundBitmap()
    val backgroundCanvas = Canvas(backgroundBitmapCache)
    
    private var isBackgroundBitmapValid = false
    
    
    fun invalidateBackground() {
        isBackgroundBitmapValid = false
    }
    
    private val prevFramePaint = Paint().apply {
        this.colorFilter = LightingColorFilter(0xFFFFFFFF.toInt(), 0x00999999)
    }
    
    private fun updateBackgroundBitmap(): Bitmap {
        backgroundCanvas.drawColor(0, PorterDuff.Mode.CLEAR)
        
        val prevFrame = animContainer?.previousFrameImage
        if (prevFrame != null) {
            measureTime("Update background") {
                backgroundCanvas.drawBitmap(prevFrame, 0f, 0f, prevFramePaint)
            }
        }
        isBackgroundBitmapValid = true
        return backgroundBitmapCache
    }
    
    
    // in image coordinates
    private val screenRectShifted = Rect()
    
    
    /** Builds image with shift for current layer to dirtyChanges. */
    fun buildImageWithShift(shiftX: Int, shiftY: Int, shiftAllLayers: Boolean): Bitmap {
        val builtCanvas = dirtyChangesCanvas
        val screenRect = scaleHandler.screenRectTransformed
        screenRectShifted.set(screenRect)
        screenRectShifted.offset(-shiftX, -shiftY)
        
        measureTime("Clear dirtyChanges with screen rectangle") {
            builtCanvas.clear(screenRect, imageContainer.backgroundColor)
        }
        
        // don't shift the background!!!
        measureTime("Draw and build background") {
            builtCanvas.drawBitmap(backgroundBitmap, screenRect, screenRect, null)
        }
        
        if (!shiftAllLayers) {
            measureTime("Draw with shifted layer") {
                imageContainer.drawBackLayers(builtCanvas, screenRect)
                builtCanvas.drawBitmap(
                    currentRestorableCanvas.mainBitmap,
                    screenRectShifted,
                    screenRect,
                    null
                )
                imageContainer.drawFrontLayers(builtCanvas, screenRect)
            }
        } else {
            measureTime("Draw and build shifted builtImage") {
                builtCanvas.drawBitmap(
                    imageContainer.builtImage,
                    screenRectShifted,
                    screenRect,
                    null
                )
            }
        }
        return dirtyChangesBitmap
    }
    
    private val drawMatrix: Matrix get() = drawDataHandler.scaleHandler.drawMatrix
    
    
    /** Including current layer. */
    fun drawUntilCurrentLayer(screenCanvas: Canvas, withCurrentLayer: Boolean = true) {
        if (drawDataHandler.isAnimation) {
            screenCanvas.drawBitmap(backgroundBitmap, drawMatrix)
        }
        
        val count = imageContainer.selectedLayerPosition + if (withCurrentLayer) 1 else 0
        imageContainer.layerContainers.take(count)
            .forEach { layerContainer ->
                if (layerContainer.layer.isVisible) {
                    screenCanvas.drawBitmap(layerContainer.restorableCanvas.mainBitmap, drawMatrix)
                }
            }
    }
    
    
    fun drawAfterCurrentLayer(screenCanvas: Canvas) {
        imageContainer.layerContainers.drop(imageContainer.selectedLayerPosition + 1)
            .forEach { layerContainer ->
                if (layerContainer.layer.isVisible) {
                    screenCanvas.drawBitmap(layerContainer.restorableCanvas.mainBitmap, drawMatrix)
                }
            }
    }
    
    fun clearDirtyChangesCanvas() {
        measureTime("Clear dirtyChangesCanvas") {
            val screenRect = scaleHandler.screenRectTransformed
            dirtyChangesCanvas.clear(screenRect)
        }
    }
    
    /** Draws the current layer on the [dirtyChangesCanvas], but only where the screen is visible. */
    fun setDirtyCanvasCopyOfCurrentLayer() {
        val screenRect = scaleHandler.screenRectTransformed
        dirtyChangesCanvas.clear(screenRect)
        dirtyChangesCanvas.drawBitmap(currentRestorableCanvas.mainBitmap, screenRect, screenRect, null)
    }
}