package com.flamingo.animator.editors.spineEditor.dataHolders

import com.flamingo.animator.adapters.spine.TimelineAdapter.Companion.TIMESTAMP_PRECISION
import com.flamingo.animator.model.spine.Bone
import com.flamingo.animator.model.spine.SpineAnimation
import com.flamingo.animator.model.spine.animation.timelines.BoneTimeline
import com.flamingo.animator.model.spine.animation.timelines.BoneTimestamp
import com.flamingo.animator.utils.commonUtils.makeNameUnique
import com.flamingo.animator.utils.commonUtils.uniqueName
import com.flamingo.animator.utils.realmUtils.transaction

class SpineAnimationDataHolder(val spineDataHolder: SpineDataHolder) {
    private val spine get() = spineDataHolder.spine
    private val bonesContainer get() = spineDataHolder.bonesContainer
    private val spineRepo get() = spineDataHolder.surfaceView.activity.spineRepo
    val animationContainers = mutableListOf<SpineAnimationContainer>()
    
    // TODO make not nullable
    val currentAnimationContainer
        get() = animationContainers.getOrNull(currentAnimationContainerPosition)
    
    var currentAnimationContainerPosition = 0
        private set
    
    /** Time in millis. */
    var animationTime = 0
    
    /** Timestamp should store time which is truncated to a quarter of second. */
    val animationTimeTruncated get() = animationTime / TIMESTAMP_PRECISION * TIMESTAMP_PRECISION
    
    val editedBoneIdsToHighlight = mutableSetOf<Int>()
    
    init {
        animationContainers += spine.animations.map { SpineAnimationContainer(it) }
        
        if (animationContainers.isEmpty()) {
            addAnimation(uniqueName(spine.animations.map { it.name }, "anim"))
        }
    }
    
    fun addAnimation(name: String) {
        spine.transaction {
            val anim = SpineAnimation.init(realm)
            anim.name = name
            animations.add(anim)
            animationContainers.add(SpineAnimationContainer(anim))
        }
    }
    
    fun removeAnimation(position: Int) {
        if (animationContainers.size == 1) {
            return
        }
        animationContainers.removeAt(position)
        spine.transaction { animations.removeAt(position) }
        if (position < currentAnimationContainerPosition) {
            currentAnimationContainerPosition--
        }
        if (currentAnimationContainerPosition == animationContainers.size) {
            currentAnimationContainerPosition--
        }
    }
    
    fun duplicateAnimation(position: Int) {
        val oldAnimationContainer = animationContainers[position]
        val oldAnimation = oldAnimationContainer.spineAnimation
        spine.transaction {
            val anim = SpineAnimation.init(realm)
            anim.name = makeNameUnique(
                animationContainers.map { it.spineAnimation.name }, oldAnimation.name
            )
            anim.timelinesJson = oldAnimationContainer.toJson().toString()
            animations.add(position + 1, anim)
            animationContainers.add(position + 1, SpineAnimationContainer(anim))
        }
        currentAnimationContainerPosition = position + 1
    }
    
    fun updatePosition(position: Int) {
        currentAnimationContainerPosition = position
    }
    
    // when we change a timeline frame
    fun updateAllBonesFromTimeline() {
        for (bone in bonesContainer.traverseBones()) {
            val timeline = findTimeline(bone)
            val time = animationTime
            bone.apply {
                timelineX = x + (timeline?.findInterpolatedValue(time, BoneTimestamp::x) ?: 0f)
                timelineY = y + (timeline?.findInterpolatedValue(time, BoneTimestamp::y) ?: 0f)
                timelineRotation = rotation +
                        (timeline?.findInterpolatedValue(time, BoneTimestamp::rotation) ?: 0f)
                // timeline is bound to bone, but attachment position is stored in slot!
                slotContainer?.apply {
                    selectedAttachmentPositionTimeline =
                        selectedAttachmentPosition + (timeline?.findAttachmentSelection(time) ?: 0)
                }
                timelineVisible = timeline?.findVisibilitySelection(time) ?: true
            }
            bone.updateAbsolute(true)
            bone.slotContainer?.updateAbsolute(true)
        }
    }
    
    fun createOrUpdateTimestampForBone(bone: Bone) {
        val anim = currentAnimationContainer ?: return
        val time = animationTimeTruncated
        val timeline = anim.boneTimelines.getOrPut(bone.id, ::BoneTimeline)
        val timestamp = timeline.boneTimestamps.getOrPut(time, ::BoneTimestamp)
        with(timestamp) {
            x = bone.timelineX - bone.x
            y = bone.timelineY - bone.y
            rotation = bone.timelineRotation - bone.rotation
        }
    }
    
    /** Increments selectedAttachmentPosition override on timeline.
     * Or Removes it, if it equals previous. */
    fun createOrUpdateSelectedAttachment(bone: Bone) {
        val anim = currentAnimationContainer ?: return
        val time = animationTimeTruncated
        val timeline = anim.boneTimelines.getOrPut(bone.id, ::BoneTimeline)
        val timestamps = timeline.attachmentSelectionTimestamps
        val slotContainer = bone.slotContainer ?: return
        val maxSize = slotContainer.attachmentContainers.size
        
        val previous = timeline.findAttachmentSelection(time - 1)
        val position = timestamps.getOrPut(time, { previous }) + 1
        
        if (position % maxSize == previous % maxSize) {
            timestamps.remove(time)
        } else {
            timestamps[time] = position
        }
    }
    
    /** Increments selectedAttachmentPosition override on timeline.
     * Or Removes it, if it equals previous. */
    fun disableOrEnableSubtree(bone: Bone) {
        val anim = currentAnimationContainer ?: return
        val time = animationTimeTruncated
        val timeline = anim.boneTimelines.getOrPut(bone.id, ::BoneTimeline)
        val timestamps = timeline.visibilityTimestamps
        
        val previous = timeline.findVisibilitySelection(time - 1)
        val visibility = !timestamps.getOrPut(time, { previous })
        
        if (visibility == previous) {
            timestamps.remove(time)
        } else {
            timestamps[time] = visibility
        }
    }
    
    fun clearCurrentTimestampForBone(bone: Bone) {
        val anim = currentAnimationContainer ?: return
        val time = animationTimeTruncated
        val timeline = anim.boneTimelines[bone.id] ?: return
        timeline.boneTimestamps.remove(time)
        timeline.attachmentSelectionTimestamps.remove(time)
        timeline.visibilityTimestamps.remove(time)
    }
    
    private fun findTimeline(bone: Bone): BoneTimeline? {
        val anim = currentAnimationContainer ?: return null
        return anim.boneTimelines[bone.id]
    }
    
    fun saveAll() {
        animationContainers.forEach { it.save() }
    }
    
    fun findTimestampsCount(time: Int): Int {
        val anim = currentAnimationContainer ?: return 0
        return anim.boneTimelines.values.count { it.boneTimestamps.contains(time) }
    }
    
    /** Attachment swap timestamps for time. */
    fun findAttachmentSelectionTimestampsCount(time: Int): Int {
        val anim = currentAnimationContainer ?: return 0
        return anim.boneTimelines.values.count { it.attachmentSelectionTimestamps.contains(time) }
    }
    
    /** Visibility timestamps for time. */
    fun findSubtreeVisibilityTimestampsCount(time: Int): Int {
        val anim = currentAnimationContainer ?: return 0
        return anim.boneTimelines.values.count { it.visibilityTimestamps.contains(time) }
    }
    
    
    fun updateEditedBonesToHighlight() {
        val anim = currentAnimationContainer ?: return
        val time = animationTimeTruncated
        editedBoneIdsToHighlight.clear()
        editedBoneIdsToHighlight += anim.boneTimelines
            .filterValues { it.boneTimestamps.contains(time) }
            .map { it.key }
    }
    
    fun addZeroTimestamp(bone: Bone) {
        val anim = currentAnimationContainer ?: return
        val time = animationTimeTruncated
        val timeline = anim.boneTimelines.getOrPut(bone.id, ::BoneTimeline)
        val timestamp = timeline.boneTimestamps.getOrPut(time, ::BoneTimestamp)
        with(timestamp) {
            x = 0f
            y = 0f
            rotation = 0f
        }
    }
    
    /** If no previous timestamp, put 0, 0, 0 */
    fun repeatTimestamp(bone: Bone) {
        val anim = currentAnimationContainer ?: return
        val time = animationTimeTruncated
        val timeline = anim.boneTimelines.getOrPut(bone.id, ::BoneTimeline)
        val timestamp = timeline.boneTimestamps.getOrPut(time, ::BoneTimestamp)
        val timePoints = timeline.boneTimestamps.keys.toList()
        val prevIndex = timePoints.indexOf(time) - 1
        if (prevIndex < 0) {
            with(timestamp) {
                x = 0f
                y = 0f
                rotation = 0f
            }
        } else {
            val prevTimestamp = timeline.boneTimestamps.getValue(timePoints[prevIndex])
            with(timestamp) {
                x = prevTimestamp.x
                y = prevTimestamp.y
                rotation = prevTimestamp.rotation
            }
        }
    }
    
    /** Removes timelines without bones, and attachment selection timestamps for simple slots. */
    fun clearInvalidTimestamps(): Boolean {
        var cleared = false
        val bonesByIds = bonesContainer.traverseBones().map { it.id to it }.toMap()
        animationContainers.forEach { animationContainer ->
            val boneTimelines = animationContainer.boneTimelines
            val invalidKeys = boneTimelines.keys.minus(bonesByIds.keys)
            invalidKeys.forEach { boneTimelines.remove(it) }
            if (invalidKeys.isNotEmpty()) {
                cleared = true
            }
            boneTimelines.forEach { (boneId, timeline) ->
                val bone = bonesByIds.getValue(boneId)
                if ((bone.slotContainer?.attachmentContainers?.size ?: 0) < 2
                    && timeline.attachmentSelectionTimestamps.isNotEmpty()
                ) {
                    timeline.attachmentSelectionTimestamps.clear()
                    cleared = true
                }
            }
        }
        return cleared
    }
}