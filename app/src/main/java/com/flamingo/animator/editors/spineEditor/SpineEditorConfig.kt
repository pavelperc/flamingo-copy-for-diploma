package com.flamingo.animator.editors.spineEditor

import com.flamingo.animator.editors.spineEditor.tools.BoneToolType
import com.flamingo.animator.editors.spineEditor.tools.SlotToolType
import com.flamingo.animator.utils.commonUtils.optFloat
import org.json.JSONObject

/** Setups spine editor window config. */
class SpineEditorConfig(json: JSONObject) {
    
    val camera = Camera(json.optJSONObject("camera") ?: JSONObject())
    
    var inBonesMode = json.optBoolean("inBonesMode", false)
    
    var inAnimationMode = json.optBoolean("inAnimationMode", false)
    
    var selectedBoneTool = BoneToolType.byNameOrDefault(
        json.optString("selectedBoneTool"), BoneToolType.DRAW_BONE
    )
    
    var selectedSlotTool = SlotToolType.byNameOrDefault(
        json.optString("selectedSlotTool"), SlotToolType.TRANSFORM_TOOL
    )
    
    fun toJson() = JSONObject().apply {
        put("camera", camera.toJson())
        put("inBonesMode", inBonesMode)
        put("inAnimationMode", inAnimationMode)
        put("selectedBoneTool", selectedBoneTool.toString())
        put("selectedSlotTool", selectedSlotTool.toString())
    }
    
    class Camera(json: JSONObject) {
        var scale = json.optFloat("scale", 1f)
        var translateX = json.optFloat("translateX")
        var translateY = json.optFloat("translateY")
        fun toJson() = JSONObject().apply {
            put("scale", scale)
            put("translateX", translateX)
            put("translateY", translateY)
        }
    }
}