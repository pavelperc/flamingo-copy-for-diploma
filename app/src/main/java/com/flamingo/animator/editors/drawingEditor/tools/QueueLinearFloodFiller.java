package com.flamingo.animator.editors.drawingEditor.tools;

import android.graphics.Bitmap;
import android.graphics.Color;

import java.util.LinkedList;
import java.util.Queue;

public class QueueLinearFloodFiller {
    protected int[] tolerance = new int[]{50, 50, 50, 50};
    protected int width = 0;
    protected int height = 0;
    protected int[] pixels = null;
    protected int[] startColor = new int[]{0, 0, 0, 0}; // pavel: target color
    protected int[] pixelsChecked; // pavel: use as mask 0 or Color.Black
    protected Queue<FloodFillRange> ranges;

    // Construct using an image and a copy will be made to fill into,
    // Construct with BufferedImage and flood fill will write directly to
    // provided BufferedImage
    public QueueLinearFloodFiller(Bitmap img) {
        useImage(img);
    }

    private void setTargetColor(int targetColor) {
        startColor[0] = Color.alpha(targetColor);
        startColor[1] = Color.red(targetColor);
        startColor[2] = Color.green(targetColor);
        startColor[3] = Color.blue(targetColor);
    }

    public int[] getTolerance() {
        return tolerance;
    }

    public void setTolerance(int[] value) {
        tolerance = value;
    }

    /** If pixel alpha is less than 255 we will fill it. */
    public void setFillTransparentPixels(boolean fillTransparentPixels) {
        this.fillTransparentPixels = fillTransparentPixels;
    }

    private boolean fillTransparentPixels = false;

    public void setTolerance(int value) {
        tolerance = new int[]{value, value, value, value};
    }


    public void useImage(Bitmap img) {
        // Use a pre-existing provided BufferedImage and write directly to it
        // cache data in member variables to decrease overhead of property calls
        width = img.getWidth();
        height = img.getHeight();

        pixels = new int[width * height];

        img.getPixels(pixels, 0, width, 0, 0, width, height);
    }

    protected void prepare() {
        // Called before starting flood-fill
        pixelsChecked = new int[pixels.length];
        ranges = new LinkedList<FloodFillRange>();
    }

    // Fills the specified point on the bitmap with the currently selected fill
    // color.
    // int x, int y: The starting coords for the fill
    public Bitmap floodFill(int x, int y) {
        // Setup
        prepare();

        // ***Get starting color.
        int startPixel = pixels[(width * y) + x];
        setTargetColor(startPixel); // set start color


        // ***Do first call to floodfill.
        LinearFill(x, y);

        // ***Call floodfill routine while floodfill ranges still exist on the
        // queue
        FloodFillRange range;

        while (ranges.size() > 0) {
            // **Get Next Range Off the Queue
            range = ranges.remove();

            // **Check Above and Below Each Pixel in the Floodfill Range
            int downPxIdx = (width * (range.Y + 1)) + range.startX;
            int upPxIdx = (width * (range.Y - 1)) + range.startX;
            int upY = range.Y - 1;// so we can pass the y coord by ref
            int downY = range.Y + 1;

            for (int i = range.startX; i <= range.endX; i++) {
                // *Start Fill Upwards
                // if we're not above the top of the bitmap and the pixel above
                // this one is within the color tolerance
                if (range.Y > 0 && (pixelsChecked[upPxIdx] == 0)
                        && checkPixel(upPxIdx))
                    LinearFill(i, upY);

                // *Start Fill Downwards
                // if we're not below the bottom of the bitmap and the pixel
                // below this one is within the color tolerance
                if (range.Y < (height - 1) && (pixelsChecked[downPxIdx] == 0)
                        && checkPixel(downPxIdx))
                    LinearFill(i, downY);

                downPxIdx++;
                upPxIdx++;
            }
        }

        // pavel: we will use mask instead!!
//        image.setPixels(pixels, 0, width, 1, 1, width - 1, height - 1);
        Bitmap mask = Bitmap.createBitmap(pixelsChecked, width, height, Bitmap.Config.ALPHA_8);

        return mask;
    }

    // Finds the furthermost left and right boundaries of the fill area
    // on a given y coordinate, starting from a given x coordinate, filling as
    // it goes.
    // Adds the resulting horizontal range to the queue of floodfill ranges,
    // to be processed in the main loop.

    // int x, int y: The starting coords
    protected void LinearFill(int x, int y) {
        // ***Find Left Edge of Color Area
        int lFillLoc = x; // the location to check/fill on the left
        int pxIdx = (width * y) + x;

        while (true) {
            // **fill with the color
//            pixels[pxIdx] = fillColor; // pavel: no fill

            // **indicate that this pixel has already been checked and filled
            pixelsChecked[pxIdx] = Color.BLACK;

            // **de-increment
            lFillLoc--; // de-increment counter
            pxIdx--; // de-increment pixel index

            // **exit loop if we're at edge of bitmap or color area
            if (lFillLoc < 0 || (pixelsChecked[pxIdx] != 0) || !checkPixel(pxIdx)) {
                break;
            }
        }

        lFillLoc++;

        // ***Find Right Edge of Color Area
        int rFillLoc = x; // the location to check/fill on the left

        pxIdx = (width * y) + x;

        while (true) {
            // **fill with the color
//            pixels[pxIdx] = fillColor; // pavel: no fill

            // **indicate that this pixel has already been checked and filled
            pixelsChecked[pxIdx] = Color.BLACK;

            // **increment
            rFillLoc++; // increment counter
            pxIdx++; // increment pixel index

            // **exit loop if we're at edge of bitmap or color area
            if (rFillLoc >= width || pixelsChecked[pxIdx] != 0 || !checkPixel(pxIdx)) {
                break;
            }
        }

        rFillLoc--;

        // add range to queue
        FloodFillRange r = new FloodFillRange(lFillLoc, rFillLoc, y);

        ranges.offer(r);
    }

    // Sees if a pixel is within the color tolerance range.
    private boolean checkPixel(int px) {
        int a = Color.alpha(pixels[px]);
        int r = Color.red(pixels[px]);
        int g = Color.green(pixels[px]);
        int b = Color.blue(pixels[px]);

        if (fillTransparentPixels && a < 255) {
            return true;
        }

        return (a >= startColor[0] - tolerance[0]
                && a <= startColor[0] + tolerance[0]
                && r >= (startColor[1] - tolerance[1])
                && r <= (startColor[1] + tolerance[1])
                && g >= (startColor[2] - tolerance[2])
                && g <= (startColor[2] + tolerance[2])
                && b >= (startColor[3] - tolerance[3])
                && b <= (startColor[3] + tolerance[3]));
    }

    // Represents a linear range to be filled and branched from.
     private static class FloodFillRange {
        public int startX;
        public int endX;
        public int Y;

        public FloodFillRange(int startX, int endX, int y) {
            this.startX = startX;
            this.endX = endX;
            this.Y = y;
        }
    }
}