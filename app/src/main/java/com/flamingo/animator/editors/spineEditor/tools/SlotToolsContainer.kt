package com.flamingo.animator.editors.spineEditor.tools

import com.flamingo.animator.editors.drawingEditor.tools.Tool
import com.flamingo.animator.editors.spineEditor.SpineSurfaceView

enum class SlotToolType {
    TRANSFORM_TOOL, SLOT_MERGE_TOOL;
    
    companion object {
        private val names = values().map { it.toString() }
        
        fun byNameOrDefault(name: String, default: SlotToolType) =
            if (name in names) valueOf(name) else default
    }
}

class SlotToolsContainer(val surfaceView: SpineSurfaceView) {
    
    val slotSelector = SlotSelector(surfaceView)
    
    val transformTool = TransformSlotTool(surfaceView, false)
    val slotMergeTool = TransformSlotTool(surfaceView, true)
    
    var selectedTool: Tool? = transformTool
    
    fun selectSlotTool(type: SlotToolType) {
        selectedTool = when (type) {
            SlotToolType.TRANSFORM_TOOL -> transformTool
            SlotToolType.SLOT_MERGE_TOOL -> slotMergeTool
        }
    }
    
    var selectedToolType: SlotToolType
        get() = when (selectedTool) {
            transformTool -> SlotToolType.TRANSFORM_TOOL
            slotMergeTool -> SlotToolType.SLOT_MERGE_TOOL
            else -> throw IllegalStateException("Unknown slot tool")
        }
        set(value) {
            selectedTool = when (value) {
                SlotToolType.TRANSFORM_TOOL -> transformTool
                SlotToolType.SLOT_MERGE_TOOL -> slotMergeTool
            }
        }
}