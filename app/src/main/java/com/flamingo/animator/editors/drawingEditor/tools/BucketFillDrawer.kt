package com.flamingo.animator.editors.drawingEditor.tools

import android.graphics.*
import android.view.MotionEvent
import com.flamingo.animator.editors.drawUtils.copy
import com.flamingo.animator.editors.drawingEditor.DrawingSurfaceView
import com.flamingo.animator.editors.drawingEditor.dataHolders.RestorableAction

class BucketFillDrawer(
    val surfaceView: DrawingSurfaceView
) : Drawer {
    /** Target Color. */
    val fillPaint = Paint().apply { color = Color.BLACK }
    
    private val fillPaintTransparent = Paint().apply {
        xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_OUT)
    }
    
    var backgroundEraser = false
    /** Smaller tolerance is more precise. */
    var tolerance = 50
    
    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_UP) {
            val coord = surfaceView.getScaledCoord(event.x, event.y)
            val startX = coord.x.toInt()
            val startY = coord.y.toInt()
            
            val mainBitmap = surfaceView.currentRestorableCanvas.mainBitmap
            if (startX < 0 || startX >= mainBitmap.width || startY < 0 || startY >= mainBitmap.height) {
                return true
            }
            
            val pixel = mainBitmap.getPixel(startX, startY)
            if (backgroundEraser && Color.alpha(pixel) == 0
                || !backgroundEraser && pixel == fillPaint.color
            ) {
                return true
            }
            
            val queueLinearFloodFiller = QueueLinearFloodFiller(mainBitmap)
            queueLinearFloodFiller.setTolerance(tolerance)
            
            if (Color.alpha(pixel) == 0) {
                queueLinearFloodFiller.setFillTransparentPixels(true)
                fillPaint.xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_OVER)
            } else {
                fillPaint.xfermode = null
            }
            
            val maskBitmap = queueLinearFloodFiller.floodFill(startX, startY)!!
            
            val paint = if (backgroundEraser) fillPaintTransparent else fillPaint
            
            surfaceView.currentRestorableCanvas.performAction(
                FillerAction(paint.copy(), maskBitmap)
            )
            surfaceView.updateState()
        }
        return true
    }
}

class FillerAction(
    private val fillPaint: Paint,
    maskBitmap: Bitmap
) : RestorableAction {
    
    init {
        fillPaint.shader = BitmapShader(maskBitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT)
    }
    
    override fun perform(canvas: Canvas, bitmap: Bitmap) {
        canvas.drawPaint(fillPaint)
    }
    
}