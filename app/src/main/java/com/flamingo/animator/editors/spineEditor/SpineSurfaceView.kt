package com.flamingo.animator.editors.spineEditor

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import com.flamingo.animator.R
import com.flamingo.animator.activity.spine.SpineEditorActivity
import com.flamingo.animator.editors.drawUtils.imageCropper.AttachmentCropper
import com.flamingo.animator.editors.drawUtils.scale
import com.flamingo.animator.editors.drawUtils.x
import com.flamingo.animator.editors.drawUtils.y
import com.flamingo.animator.editors.drawingEditor.FitScreenScale
import com.flamingo.animator.editors.drawingEditor.ScaleDetectorCallback
import com.flamingo.animator.editors.drawingEditor.ScaleHandler
import com.flamingo.animator.editors.spineEditor.dataHolders.SlotContainer
import com.flamingo.animator.editors.spineEditor.dataHolders.SpineDataHolder
import com.flamingo.animator.editors.spineEditor.tools.BoneToolsContainer
import com.flamingo.animator.editors.spineEditor.tools.SlotToolsContainer
import com.flamingo.animator.utils.commonUtils.toInt
import com.flamingo.animator.utils.realmUtils.transaction
import org.jetbrains.anko.dip
import org.jetbrains.anko.progressDialog
import org.jetbrains.anko.sp
import org.jetbrains.anko.toast
import java.util.concurrent.Semaphore
import kotlin.concurrent.thread


/** This surface view gets touch event and can draw a canvas with [updateScreen] method.*/
@SuppressLint("ViewConstructor")
class SpineSurfaceView(val activity: SpineEditorActivity) : SurfaceView(activity),
    ScaleDetectorCallback {
    private val backgroundColor = activity.getColor(R.color.backgroundLightGray)
    
    val assetRepo get() = activity.assetRepo
    
    override fun onScaleStart() {}
    
    override fun onScaleUpdate(scale: Float) {
        updateScreen()
        activity.updateScaleText(scale)
    }
    
    val scaleHandler: ScaleHandler get() = scaleHandlerOpt!!
    private var scaleHandlerOpt: ScaleHandler? = null
    
    val inBonesMode get() = activity.inBonesMode
    val inAnimationMode get() = activity.inAnimationMode
    
    val spine get() = activity.spine
    
    val spineDataHolder = SpineDataHolder(this)
    val bonesContainer get() = spineDataHolder.bonesContainer
    val animationDataHolder get() = spineDataHolder.animationDataHolder
    
    val slotToolsContainer = SlotToolsContainer(this)
    val boneToolsContainer = BoneToolsContainer(this)
    
    val slotDrawer = SlotDrawer(this)
    
    val boneDrawer = BoneDrawer(this)
    
    private val attachmentCropper = AttachmentCropper(this)
    
    val isAnimationPlaying get() = activity.isAnimationPlaying
    
    // todo refactor to scale handler...
    private fun importCameraState(config: SpineEditorConfig) {
        // stay centered on the screen, if there is no config
        if (config.camera.translateX == 0.0f) {
            return
        }
        // update screen
        scaleHandler.set(
            config.camera.scale,
            config.camera.translateX,
            config.camera.translateY
        )
    }
    
    fun exportCameraState(config: SpineEditorConfig) {
        scaleHandlerOpt ?: return
        config.camera.scale = scaleHandler.drawMatrix.scale
        config.camera.translateX = scaleHandler.drawMatrix.x
        config.camera.translateY = scaleHandler.drawMatrix.y
    }
    
    init {
        holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(holder: SurfaceHolder) {
                val fitScreenScale = FitScreenScale.centerCoord(width, height)
                scaleHandlerOpt =
                    ScaleHandler(this@SpineSurfaceView, activity, fitScreenScale, width, height)
                
                importCameraState(activity.spineDrawConfig) // updates screen or not
                updateScreen()
                
                cropAllSlots()
            }
            
            override fun surfaceChanged(
                holder: SurfaceHolder?,
                format: Int,
                width: Int,
                height: Int
            ) {
                scaleHandler.fitScreenScale.updateCenterCoord(width, height)
                // todo refactor: update only once if need.
                scaleHandler.reset(width, height) // callback here
                importCameraState(activity.spineDrawConfig) // and here
            }
            
            override fun surfaceDestroyed(holder: SurfaceHolder?) {}
        })
    }
    
    
    fun resetScale() {
        scaleHandler.reset(width, height)
    }
    
    private val axesPaint = Paint().apply {
        color = Color.DKGRAY
        strokeWidth = 3f
        textSize = sp(10).toFloat()
    }
    
    private fun drawAxes(canvas: Canvas) {
        val centerPoint = floatArrayOf(0f, 0f)
        // real --> screen
        scaleHandler.drawMatrix.mapPoints(centerPoint)
        val (centerX, centerY) = centerPoint
        
        val screenEndX = canvas.width.toFloat() - dip(32)
        val screenEndY = canvas.height.toFloat() - dip(16)
        val screenEndPoints = floatArrayOf(
            centerX,
            screenEndY,
            screenEndX,
            centerY
        )
        // screen -> real
        scaleHandler.inverseDrawMatrix.mapPoints(screenEndPoints)
        val realEndX = screenEndPoints[2].toInt()
        val realEndY = screenEndPoints[1].toInt()
        
        canvas.drawLine(centerX, 0f, centerX, canvas.height.toFloat(), axesPaint)
        canvas.drawLine(0f, centerY, canvas.width.toFloat(), centerY, axesPaint)
        
        
        canvas.drawText(realEndY.toString(), centerX + dip(16), screenEndY, axesPaint)
        canvas.drawText(realEndX.toString(), screenEndX, centerY + dip(16), axesPaint)
        
        canvas.drawLine(centerX - dip(8), screenEndY, centerX + dip(8), screenEndY, axesPaint)
        canvas.drawLine(screenEndX, centerY - dip(8), screenEndX, centerY + dip(8), axesPaint)
    }
    
    fun updateScreen() {
        val screenCanvas = holder.lockCanvas() ?: return // if the activity was closed
        
        // just surface
        screenCanvas.drawColor(backgroundColor)
        drawAxes(screenCanvas)
        
        for (slotContainer in spineDataHolder.visibleSlotContainers) {
            slotDrawer.drawSlot(slotContainer, screenCanvas)
        }
        
        if (isAnimationPlaying) {
            bonesContainer.visibleBones.forEach { bone ->
                boneDrawer.drawBone(screenCanvas, bone, ghostMode = true)
            }
        } else if (inBonesMode || inAnimationMode) {
            // highlight slot for selected bone
            val contanerOfSelectedBone = bonesContainer.selectedBone?.slotContainer
            if (contanerOfSelectedBone != null && contanerOfSelectedBone.isVisible) {
                slotDrawer.drawSlotSelection(
                    contanerOfSelectedBone, screenCanvas, SlotDrawer.HIGHLIGHT_COLOR
                )
            }
            
            bonesContainer.visuallyVisibleBones.forEach { bone ->
                val outline = bone.isInvisibleBranchStart
                boneDrawer.drawBone(
                    screenCanvas, bone,
                    selected = bonesContainer.selectedBone == bone,
                    timestampEdited = inAnimationMode
                            && bone.id in animationDataHolder.editedBoneIdsToHighlight,
                    outline = outline
                )
            }
        } else { // slots mode
            val selectedSlot = spineDataHolder.selectedSlot
            val secondSelectedSlot = spineDataHolder.secondSelectedSlot
            if (secondSelectedSlot != null && secondSelectedSlot.isVisible) {
                slotDrawer.drawSlotSelection(
                    secondSelectedSlot, screenCanvas, SlotDrawer.SECOND_SELECTION_COLOR
                )
            }
            if (selectedSlot != null && selectedSlot.isVisible) {
                // draw buttons and rectangle
                slotDrawer.drawSlotSelection(
                    selectedSlot, screenCanvas, SlotDrawer.SELECTION_COLOR, drawButtons = true
                )
            }
            bonesContainer.visuallyVisibleBones.forEach { bone ->
                boneDrawer.drawBone(screenCanvas, bone, ghostMode = true)
            }
        }
        holder.unlockCanvasAndPost(screenCanvas)
    }
    
    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (scaleHandler.onTouchEvent(event)) {
            return true
        }
        if (isAnimationPlaying) {
            return true
        }
        val selectedTool = when {
            inAnimationMode -> boneToolsContainer.movingAnimationPoseTool
            inBonesMode -> boneToolsContainer.selectedTool
            else -> slotToolsContainer.selectedTool
        }
        
        return selectedTool?.onTouchEvent(event) ?: true
    }
    
    fun cropAllSlots(forceCropped: Boolean = false) {
        val attachmentContainers =
            if (forceCropped) spineDataHolder.slotContainers.flatMap { it.attachmentContainers }
            else spineDataHolder.slotContainers.flatMap { it.attachmentContainers }
                .filter { !it.attachment.isCropped }
        
        attachmentContainers.forEach { it.updateShiftAfterCrop() }
        
        if (attachmentContainers.isEmpty()) {
            return
        }
        
        val pd = activity.progressDialog("Cropping images")
        pd.max = attachmentContainers.size
        pd.progress = 0
        pd.setCancelable(false)
        pd.show()
        
        // can not change realm objects in other thread
        thread {
            val count = attachmentContainers.size
            var croppedCount = 0
            
            for (i in 0 until count) {
                val bitmap = attachmentContainers[i].bitmap
                croppedCount += attachmentCropper.cropAttachment(attachmentContainers[i], bitmap)
                    .toInt()
                pd.progress++
            }
            activity.runOnUiThread {
                // transaction is allowed only in main thread
                assetRepo.realm.transaction {
                    attachmentContainers.forEach {
                        // mark all attachments as cropped
                        it.attachment.isCropped = true
                        // update sizes of all images
                        attachmentCropper.updateImageSizesFromBitmaps(it)
                    }
                }
                
                pd.dismiss()
                activity.toast("$croppedCount/$count cropped.")
                updateScreen()
                
            }
        }
    }
    
    fun deleteSlot(slotContainer: SlotContainer) {
        spineDataHolder.deleteSlot(slotContainer)
        updateScreen()
    }
    
    fun onSlotSelectionChanged() {
        activity.onSlotSelectionChanged()
    }
    
    fun onBoneSelectionChanged() {
        activity.onBoneSelectionChanged()
    }
    
    fun moveSelectedSlotToBack() {
        val slotContainer = spineDataHolder.selectedSlot ?: return
        val slot = slotContainer.slot
        
        spineDataHolder.spine.transaction {
            slots.remove(slot)
            slots.add(0, slot)
        }
        spineDataHolder.slotContainers.remove(slotContainer)
        spineDataHolder.slotContainers.add(0, slotContainer)
        updateScreen()
    }
    
    fun moveSelectedSlotToFront() {
        val slotContainer = spineDataHolder.selectedSlot ?: return
        val slot = slotContainer.slot
        
        spineDataHolder.spine.transaction {
            slots.remove(slot)
            slots.add(slot)
        }
        spineDataHolder.slotContainers.remove(slotContainer)
        spineDataHolder.slotContainers.add(slotContainer)
        updateScreen()
    }
    
    fun swapAttachmentForSelectedSlot() {
        spineDataHolder.selectedSlot?.swapAttachment()
        updateScreen()
    }
    
    fun swapAttachmentForSelectedBone() {
        spineDataHolder.bonesContainer.selectedBone?.slotContainer?.swapAttachment()
        updateScreen()
    }
    
    
    fun deleteSelectedBone() {
        bonesContainer.removeSelectedBone()
        updateScreen()
    }
    
    fun saveAll() {
        spineDataHolder.saveAll()
    }
    
    fun changeAnimationTimeAndUpdate(time: Int) {
        val oldTruncatedTime = animationDataHolder.animationTimeTruncated
        animationDataHolder.animationTime = time
        val newTruncatedTime = animationDataHolder.animationTimeTruncated
        if (newTruncatedTime != oldTruncatedTime) {
            animationDataHolder.updateEditedBonesToHighlight()
        }
        updateAnimation()
    }
    
    fun updateAnimation() {
        animationDataHolder.updateAllBonesFromTimeline()
        updateScreen()
    }
    
    // animation or pose setup mode
    fun enableAnimation(enabled: Boolean) {
        if (enabled) {
            animationDataHolder.updateEditedBonesToHighlight()
            updateAnimation()
        } else {
            bonesContainer.traverseBones().forEach { bone ->
                bone.updateAbsolute(false)
                bone.slotContainer?.updateAbsolute(false)
            }
            updateScreen()
        }
    }
    
    /** For selected bone or for all */
    fun clearCurrentTimestamp() {
        val bones = bonesContainer.selectedBone?.let { listOf(it) }
            ?: bonesContainer.traverseBones().toList()
        bones.forEach { animationDataHolder.clearCurrentTimestampForBone(it) }
        animationDataHolder.updateEditedBonesToHighlight()
        updateAnimation()
    }
    
    /** For selected bone or for all */
    fun addZeroTimestamp() {
        val bones = bonesContainer.selectedBone?.let { listOf(it) }
            ?: bonesContainer.traverseBones().toList()
        bones.forEach { animationDataHolder.addZeroTimestamp(it) }
        animationDataHolder.updateEditedBonesToHighlight()
        updateAnimation()
    }
    
    fun repeatTimestamp() {
        val bones = bonesContainer.selectedBone?.let { listOf(it) }
            ?: bonesContainer.traverseBones().toList()
        bones.forEach { animationDataHolder.repeatTimestamp(it) }
        animationDataHolder.updateEditedBonesToHighlight()
        updateAnimation()
    }
    
    fun swapAttachmentOnTimeline() {
        val bone = bonesContainer.selectedBone ?: return
        if (bone.slotContainer == null) {
            return
        }
        animationDataHolder.createOrUpdateSelectedAttachment(bone)
        updateAnimation()
    }
    
    fun disableOrEnableSubtreeOnTimeline() {
        val bone = bonesContainer.selectedBone ?: return
        animationDataHolder.disableOrEnableSubtree(bone)
        updateAnimation()
    }
    
    fun startPlayingAnimation() {
        val initialAnimTime = animationDataHolder.animationTime
        var currentAnimTime = 0
        val maxAnimTime = animationDataHolder.currentAnimationContainer
            ?.boneTimelines?.values?.map { it.boneTimestamps.keys.max() ?: 0 }
            ?.max() ?: 0
        var lastFpsTime = System.currentTimeMillis()
        
        val mutex = Semaphore(0)
        
        thread(isDaemon = true) {
            while (isAnimationPlaying) {
                val delta = (System.currentTimeMillis() - lastFpsTime).toInt()
                lastFpsTime += delta
                if (maxAnimTime == 0) {
                    continue
                }
                currentAnimTime = (currentAnimTime + delta) % maxAnimTime
                activity.runOnUiThread {
                    animationDataHolder.animationTime = currentAnimTime
                    updateAnimation()
                    mutex.release()
                }
                mutex.acquire()
            }
            activity.runOnUiThread {
                animationDataHolder.animationTime = initialAnimTime
                updateAnimation()
            }
        }
    }
    
    fun dismantleSelectedSlot() {
        val selectedSlot = spineDataHolder.selectedSlot ?: return
        spineDataHolder.dismantleSlot(selectedSlot)
        updateScreen()
    }
    
    fun disableOrEnableSubtreeForSelectedBone() {
        val bone = bonesContainer.selectedBone ?: return
        bonesContainer.disableOrEnableSubtree(bone)
        updateScreen()
    }
}



