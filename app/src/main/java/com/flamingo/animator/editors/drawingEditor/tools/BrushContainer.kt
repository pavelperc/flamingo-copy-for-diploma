package com.flamingo.animator.editors.drawingEditor.tools

import android.graphics.Color
import android.graphics.CornerPathEffect
import android.graphics.Paint
import android.view.MotionEvent
import com.flamingo.animator.editors.drawingEditor.BrushType
import com.flamingo.animator.editors.drawingEditor.DrawingSurfaceView
import com.flamingo.animator.utils.commonUtils.boolOrNull
import com.flamingo.animator.utils.commonUtils.intOrNull
import com.flamingo.animator.utils.commonUtils.safeJson
import org.json.JSONObject


// needs initialized background color in [surfaceView] for eraser brush!!
class BrushContainer(surfaceView: DrawingSurfaceView) {
    val brushPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        strokeWidth = 12.0F
        color = Color.BLACK
//        pathEffect = CornerPathEffect(500f) // just to smooth any corners
        strokeCap = Paint.Cap.ROUND
    }
    
    val filledBrushPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
        strokeWidth = 1.0F
        color = Color.BLACK
        pathEffect = CornerPathEffect(500f) // just to smooth any corners
    }
    
    
    val brushDrawer = CurveDrawer(surfaceView, brushPaint, false)
    val filledBrushDrawer = OldCurveDrawer(surfaceView, filledBrushPaint)
    val eraserDrawer = EraserDrawer(surfaceView)
    val bucketFillDrawer = BucketFillDrawer(surfaceView)
    val bucketFillPaint = bucketFillDrawer.fillPaint
    
    var currentDrawer: Drawer = brushDrawer
    
    fun setDrawer(brushType: BrushType) {
        currentDrawer = when (brushType) {
            BrushType.BRUSH -> brushDrawer
            BrushType.FILL_BRUSH -> filledBrushDrawer
            BrushType.ERASER -> eraserDrawer
            BrushType.BUCKET_FILL -> bucketFillDrawer
        }
    }
    
    private fun withAlpha(color: Int, alpha: Int) =
        Color.argb(alpha, Color.red(color), Color.green(color), Color.blue(color))
    
    /** Returns the color of default brush, without alpha. (All other colors should be equal.) */
    val currentColor: Int
        get() {
            val color = brushDrawer.paint.color
            return withAlpha(color, 255)
        }
    
    /** Sets the same color for all brushes, but alpha is different. */
    fun setColor(newColor: Int) {
        val paints = listOf(brushPaint, filledBrushPaint, bucketFillPaint)
        paints.forEach { p ->
            p.color = withAlpha(newColor, Color.alpha(p.color)) // saving the original alpha
        }
    }
    
    fun importBrushes(brushStateJson: String) {
        val jo = brushStateJson.safeJson()
        
        jo.optJSONObject("brush")?.apply {
            intOrNull("alpha")?.also { brushPaint.alpha = it }
            intOrNull("strokeWidth")?.also { brushPaint.strokeWidth = it.toFloat() }
            boolOrNull("usePressure")?.also { brushDrawer.usePressure = it }
        }
        jo.optJSONObject("filledBrush")?.apply {
            intOrNull("alpha")?.also { filledBrushPaint.alpha = it }
        }
        
        jo.optJSONObject("eraser")?.apply {
            intOrNull("eraserPadDp")?.also { eraserDrawer.eraserPadDp = it }
            boolOrNull("isSquare")?.also { eraserDrawer.isSquare = it }
        }
        jo.optJSONObject("bucketFill")?.apply {
            intOrNull("alpha")?.also { bucketFillPaint.alpha = it }
            intOrNull("tolerance")?.also { bucketFillDrawer.tolerance = it }
            // don't ask backgroundEraser option!
        }
    }
    
    /** Exports brushes to json string */
    fun exportBrushes() = JSONObject().apply {
        put("brush", JSONObject().apply {
            put("alpha", brushPaint.alpha)
            put("strokeWidth", brushPaint.strokeWidth.toInt())
            put("usePressure", brushDrawer.usePressure)
        })
        put("filledBrush", JSONObject().apply {
            put("alpha", filledBrushPaint.alpha)
        })
        put("eraser", JSONObject().apply {
            put("eraserPadDp", eraserDrawer.eraserPadDp)
            put("isSquare", eraserDrawer.isSquare)
        })
        put("bucketFill", JSONObject().apply {
            put("alpha", bucketFillPaint.alpha)
            put("tolerance", bucketFillDrawer.tolerance)
            // don't save backgroundEraser option!
        })
    }.toString()
    
    private var tempEraser = false
    
    fun handleEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN || event.action == 211) {
            tempEraser = event.isButtonPressed(MotionEvent.BUTTON_STYLUS_PRIMARY)
        }
        if (tempEraser) {
            return eraserDrawer.onTouchEvent(event)
        } else {
            return currentDrawer.onTouchEvent(event)
        }
        
    }
}