package com.flamingo.animator.editors.sceneEditor

import android.graphics.*
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import com.flamingo.animator.activity.SceneActivity
import com.flamingo.animator.editors.drawUtils.expand
import com.flamingo.animator.editors.drawingEditor.DrawingSurfaceView
import com.flamingo.animator.editors.drawingEditor.FitScreenScale
import com.flamingo.animator.editors.drawingEditor.ScaleDetectorCallback
import com.flamingo.animator.editors.drawingEditor.ScaleHandler
import com.flamingo.animator.model.Scene
import com.flamingo.animator.utils.commonUtils.measureTime


class SceneSurfaceView(val activity: SceneActivity) : SurfaceView(activity), ScaleDetectorCallback {
    override fun onScaleStart() {
    }
    
    override fun onScaleUpdate(scale: Float) {
        updateScreen()
        activity.updateScaleText(scale)
    }
    
    /** Translation parameters to put the image on the screen center. */
    lateinit var fitScreenScale: FitScreenScale
    
    lateinit var scaleHandler: ScaleHandler
    
    /** Main matrix, to translate draw image to screen. */
    val drawMatrix: Matrix
        get() = scaleHandler.drawMatrix
    
    /** Matrix, to translate screen shapes to draw image. */
    val inverseDrawMatrix: Matrix
        get() = scaleHandler.inverseDrawMatrix
    
    
    private val sceneDataHandler = SceneDataHandler(activity.scene, this)
    private val scene get() = sceneDataHandler.scene
    private val background get() = scene.backgroundReq
    
    
    /** Background width */
    val pictureWidth = scene.backgroundReq.imageReq.width
    /** Background height */
    val pictureHeight = scene.backgroundReq.imageReq.height
    
    init {
        holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(holder: SurfaceHolder) {
                fitScreenScale =
                    FitScreenScale.fitCanvas(
                        width, height, pictureWidth, pictureHeight,
                        DrawingSurfaceView.RESET_SCALE_PADDING
                    )
                scaleHandler =
                    ScaleHandler(this@SceneSurfaceView, activity, fitScreenScale, width, height)
                updateScreen()
            }
            
            override fun surfaceChanged(
                holder: SurfaceHolder?,
                format: Int,
                width: Int,
                height: Int
            ) {
                fitScreenScale.updateFitCanvas(
                    width, height, pictureWidth, pictureHeight,
                    DrawingSurfaceView.RESET_SCALE_PADDING
                )
                scaleHandler.reset(width, height)
            }
            
            override fun surfaceDestroyed(holder: SurfaceHolder?) = Unit
        })
    }

//    private val checkeredPaint = CheckerBoard.createCheckeredPaint()
    
    private val shadowRect = RectF()
    
    private val shadowPaint = Paint().apply {
        color = Color.GRAY
        strokeWidth = 6f
        style = Paint.Style.STROKE
        strokeJoin = Paint.Join.ROUND
    }
    
    // border around the image
    private fun drawShadow(canvas: Canvas) {
        shadowRect.set(0f, 0f, pictureWidth.toFloat(), pictureHeight.toFloat())
        drawMatrix.mapRect(shadowRect)
        shadowRect.expand(shadowPaint.strokeWidth / 2)
        canvas.drawRect(shadowRect, shadowPaint)
    }
    
    fun resetScale() {
        scaleHandler.reset(width, height)
    }
    
    fun updateScreen(
        additionalDraw: (screenCanvas: Canvas) -> Unit = {}
    ) {
        measureTime("Update screen in common") {
            val screenCanvas =
                holder.lockCanvas() ?: return@measureTime // if the activity was closed 
            
            // just surface
            screenCanvas.drawColor(Color.LTGRAY)
            
            measureTime("Draw shadow") {
                drawShadow(screenCanvas)
            }
            
            measureTime("Draw with translation matrix") {
                screenCanvas.drawBitmap(sceneDataHandler.backgroundBitmap, drawMatrix, null)
            }
            
            additionalDraw(screenCanvas)
            holder.unlockCanvasAndPost(screenCanvas)
        }
    }
    
    override fun onTouchEvent(event: MotionEvent): Boolean {
//        activity.onSurfaceTouched()
        if (scaleHandler.onTouchEvent(event)) {
            return true
        }
        return true
    }
}


/** Holds all loaded bitmaps. */
class SceneDataHandler(
    val scene: Scene,
    val surfaceView: SceneSurfaceView
) {
    private val assetRepo get() = surfaceView.activity.assetRepo
    
    private val backgroundFile =
        scene.backgroundReq.imageReq.builtImageReq.getFile(assetRepo.assetsDir)
    
    val backgroundBitmap = BitmapFactory.decodeFile(backgroundFile.toString())
    
}