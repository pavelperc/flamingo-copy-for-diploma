package com.flamingo.animator.editors.spineEditor.tools

import android.graphics.Matrix
import android.view.MotionEvent
import com.flamingo.animator.editors.drawingEditor.tools.Tool
import com.flamingo.animator.editors.spineEditor.SpineSurfaceView
import com.flamingo.animator.editors.spineEditor.dataHolders.SlotContainer
import com.flamingo.animator.utils.dialogUtils.fastAlert
import com.flamingo.animator.utils.dialogUtils.fastYesNoAlert
import com.flamingo.animator.utils.realmUtils.transaction
import kotlin.math.atan2


/** Handles transforming images and all actions from image corner buttons. */
class TransformSlotTool(
    val surfaceView: SpineSurfaceView,
    val mergeSlotMode: Boolean
) : Tool {
    
    private val slotDrawer get() = surfaceView.slotDrawer
    private val spineDataHolder get() = surfaceView.spineDataHolder
    
    /** In screen coords. */
    private var startX = 0f
    private var startY = 0f
    
    private val matrix = Matrix()
    
    /** Copy of selected slot. Can be rejected in case of two fingers scaling start. */
    private var selectedSlot: SlotContainer? = null
    private var secondSelectedSlot: SlotContainer? = null
    
    private val inverseDrawMatrix get() = surfaceView.scaleHandler.inverseDrawMatrix
    
    private var startClickTime = 0L
    
    private val slotSelector get() = surfaceView.slotToolsContainer.slotSelector
    
    private val activity get() = surfaceView.activity
    
    private val spineRepo get() = activity.spineRepo
    
    enum class Mode {
        TRANSLATE, ROTATE, DELETE
    }
    
    private var mode = Mode.TRANSLATE
    
    private val isMergingNow get() = mergeSlotMode && mode == Mode.TRANSLATE
    
    // selected attachment coordinates
    // all in absolute coordinates
    private var initialX = 0f
    private var initialY = 0f
    private var initialRotation = 0f
    private var initialSelectedSlot: SlotContainer? = null
    
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                startX = event.x
                startY = event.y
                startClickTime = System.currentTimeMillis()
                
                // start from old selected slot
                selectedSlot = spineDataHolder.selectedSlot
                
                mode = when {
                    slotDrawer.isInsideRotateButton(event.x, event.y) -> Mode.ROTATE
                    slotDrawer.isInsideDeleteButton(event.x, event.y) -> Mode.DELETE
                    else -> Mode.TRANSLATE
                }
                
                when (mode) {
                    Mode.DELETE -> {
                        showDeleteSlotDialog()
                    }
                    Mode.TRANSLATE -> {
                        // update selection
                        selectedSlot = slotSelector.findSelectedSlot(
                            event.x, event.y,
                            preferSelectedSlotBoundingBox = true
                        )
                    }
                }
                
                selectedSlot?.selectedAttachment?.apply {
                    initialX = absoluteX
                    initialY = absoluteY
                    initialRotation = absoluteRotation
                }
                initialSelectedSlot = spineDataHolder.selectedSlot
            }
            MotionEvent.ACTION_MOVE -> {
                // prevent accidental clicks
                if (System.currentTimeMillis() - startClickTime < Tool.CLICK_WAIT_TIME) {
                    return true
                }
                val selectedSlot = selectedSlot ?: return true
                
                updateSlot(selectedSlot, event)
                surfaceView.updateScreen()
                restoreSlot(selectedSlot)
            }
            
            MotionEvent.ACTION_UP -> {
                
                // on short click:
                if (System.currentTimeMillis() - startClickTime < Tool.CLICK_WAIT_TIME) {
                    // update selection and return
                    surfaceView.spineDataHolder.selectedSlot =
                        slotSelector.findSelectedSlot(
                            startX, startY,
                            preferSelectedSlotBoundingBox = false
                        )
                    surfaceView.updateScreen()
                    return true
                }
                
                // save and draw nothing if the selection was null
                val selectedSlot = selectedSlot ?: return true
                // update selection if it was not null
                
                if (isMergingNow) {
                    showMergeSlotsDialog()
                } else {
                    updateSlot(selectedSlot, event)
                }
                surfaceView.updateScreen()
            }
        }
        return true
    }
    
    private fun showDeleteSlotDialog() {
        val slotContainer = selectedSlot ?: return
        val boundLayers =
            slotContainer.slot.attachments.mapNotNull { it.boundLayer }
        activity.fastAlert("Delete this slot?") {
            surfaceView.deleteSlot(slotContainer)
            if (boundLayers.isNotEmpty()) {
                activity.fastYesNoAlert(
                    title = "Never import this layer again?",
                    message = "(You can reset this from menu.)"
                ) {
                    activity.spine.transaction {
                        ignoredUncroppedLayersForImport.addAll(boundLayers)
                    }
                }
            }
        }
    }
    
    
    private fun updateSlot(selectedSlot: SlotContainer, event: MotionEvent) {
        val selectedAttachment = selectedSlot.selectedAttachment
        val line = floatArrayOf(startX, startY, event.x, event.y)
        inverseDrawMatrix.mapPoints(line)
        
        when (mode) {
            Mode.TRANSLATE -> {
                selectedAttachment.absoluteX += line[2] - line[0]
                selectedAttachment.absoluteY += line[3] - line[1]
            }
            Mode.ROTATE -> {
                matrix.reset()
                matrix.postRotate(selectedAttachment.absoluteRotation)
                matrix.postTranslate(selectedAttachment.absoluteX, selectedAttachment.absoluteY)
                
                val center = floatArrayOf(selectedSlot.width / 2f, selectedSlot.height / 2f)
                matrix.mapPoints(center)
                
                
                val ax = line[0] - center[0]
                val ay = line[1] - center[1]
                val bx = line[2] - center[0]
                val by = line[3] - center[1]
                val angle = Math.toDegrees(atan2(ax * by - ay * bx, ax * bx + ay * by).toDouble())
                    .toFloat()
                // TODO round angle
                
                selectedAttachment.absoluteRotation += angle
                matrix.reset()
                matrix.postRotate(angle, center[0], center[1])
                val newStart = floatArrayOf(
                    selectedAttachment.absoluteX, selectedAttachment.absoluteY
                )
                matrix.mapPoints(newStart)
                selectedAttachment.absoluteX = newStart[0]
                selectedAttachment.absoluteY = newStart[1]
            }
        }
        selectedAttachment.updateFromAbsolute()
        spineDataHolder.selectedSlot = selectedSlot
        
        if (isMergingNow) {
            val slotToMerge =
                slotSelector.findSelectedSlot(event.x, event.y, ignoredSlot = selectedSlot)
            spineDataHolder.secondSelectedSlot = slotToMerge
            secondSelectedSlot = slotToMerge
        }
    }
    
    private fun restoreSlot(selectedSlot: SlotContainer) {
        val selectedAttachment = selectedSlot.selectedAttachment
        selectedAttachment.absoluteX = initialX
        selectedAttachment.absoluteY = initialY
        selectedAttachment.absoluteRotation = initialRotation
        selectedAttachment.updateFromAbsolute()
        spineDataHolder.selectedSlot = initialSelectedSlot
        spineDataHolder.secondSelectedSlot = null
    }
    
    private fun showMergeSlotsDialog() {
        // selectedSlot will be deleted
        val selectedSlot = selectedSlot ?: return
        val secondSelectedSlot = secondSelectedSlot ?: return
        activity.fastAlert("Merge two slots?") {
            spineDataHolder.mergeSlots(selectedSlot, secondSelectedSlot)
            surfaceView.updateScreen()
        }
    }
}