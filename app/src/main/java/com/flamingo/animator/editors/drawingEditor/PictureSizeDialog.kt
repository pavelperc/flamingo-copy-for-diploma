package com.flamingo.animator.editors.drawingEditor

import android.graphics.Bitmap
import android.text.InputType
import android.view.View
import android.widget.*
import com.flamingo.animator.R
import com.flamingo.animator.utils.SomeUsefulActivity
import com.flamingo.animator.utils.dialogUtils.nonClosingPositiveButton
import com.flamingo.animator.utils.importUtils.importImage
import org.jetbrains.anko.*


/** onSuccess can return image from gallery, or new image width and height */
fun showPictureSizeDialog(
    activity: SomeUsefulActivity,
    allowImportFromGallery: Boolean = true,
    onSuccess: (width: Int, height: Int, bitmap: Bitmap?) -> Unit
) {
    
    var etWidth: EditText? = null
    var etHeight: EditText? = null
    var spinner: Spinner? = null
    var btnImport: Button? = null
    val dialog = activity.alert("Select picture resolution:") {
        customView {
            verticalLayout {
                padding = dip(16)
                
                val predefined = arrayOf(
                    "1000x1000",
                    "3000x1000",
                    "5000x1000",
                    "500x500",
                    "300x300",
                    "900x300"
                )
                if (allowImportFromGallery) {
                    btnImport = button("Load image from gallery") {
                        setCompoundDrawablesWithIntrinsicBounds(
                            0, 0, R.drawable.ic_background, 0
                        )
                        padding = dip(8)
                        elevation = 0f
                        compoundDrawablePadding = dip(8)
                        background = activity.getDrawable(R.drawable.bg_import_image_button)
                    }.lparams(wrapContent, wrapContent)
                }
                linearLayout {
                    textView("Predefined: ")
                    spinner = spinner {
                        val arrayAdapter = ArrayAdapter<String>(
                            context,
                            android.R.layout.simple_spinner_item,
                            predefined
                        )
                        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        adapter = arrayAdapter
                        onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View?,
                                position: Int,
                                id: Long
                            ) {
                                val (w, h) = predefined[position].split("x")
                                etWidth?.setText(w)
                                etHeight?.setText(h)
                                (view as TextView).text = null // do not show current selection.
                            }
                            
                            override fun onNothingSelected(parent: AdapterView<*>?) = Unit
                        }
                    }
                }
                
                linearLayout {
                    textView("Width:  ")
                    etWidth = editText {
                        inputType = InputType.TYPE_CLASS_NUMBER
                        width = dip(100)
                    }
                }
                linearLayout {
                    textView("Height: ")
                    etHeight = editText {
                        inputType = InputType.TYPE_CLASS_NUMBER
                        width = dip(100)
                    }
                }
                spinner?.setSelection(0)
            }
        }
        yesButton { /* override below. */ }
    }.show()
    
    // may be null
    btnImport?.setOnClickListener {
        activity.importImage { bitmap ->
            dialog.dismiss()
            onSuccess(bitmap.width, bitmap.height, bitmap)
        }
    }
    dialog.nonClosingPositiveButton {
        onSuccess(
            etWidth?.text?.toString()?.toIntOrNull() ?: return@nonClosingPositiveButton false,
            etHeight?.text?.toString()?.toIntOrNull() ?: return@nonClosingPositiveButton false,
            null
        )
        true
    }
}