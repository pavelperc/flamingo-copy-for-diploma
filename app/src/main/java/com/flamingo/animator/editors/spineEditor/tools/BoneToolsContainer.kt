package com.flamingo.animator.editors.spineEditor.tools

import com.flamingo.animator.editors.drawingEditor.tools.Tool
import com.flamingo.animator.editors.spineEditor.SpineSurfaceView

enum class BoneToolType {
    DRAW_BONE, MOVE_ONE_BONE, MOVE_BONE_TREE;
    
    companion object {
        private val names = values().map { it.toString() }
        
        fun byNameOrDefault(name: String, default: BoneToolType) =
            if (name in names) valueOf(name) else default
    }
}

interface BoneTool : Tool {
    val toolType: BoneToolType
}

class BoneToolsContainer(val surfaceView: SpineSurfaceView) {
    val boneSelector = BoneSelector(surfaceView)
    
    val movingBoneTreeTool = MovingBoneTool(surfaceView)
    val movingOneBoneTool = MovingBoneTool(surfaceView, moveOneBoneMode = true)
    val movingAnimationPoseTool = MovingBoneTool(surfaceView, animationMode = true)
    
    val drawingBoneTool = DrawingBoneTool(surfaceView)
    
    var selectedTool: BoneTool = drawingBoneTool
    
    
    var selectedToolType
        get() = selectedTool.toolType
        set(value) {
            selectedTool = when (value) {
                BoneToolType.DRAW_BONE -> drawingBoneTool
                BoneToolType.MOVE_ONE_BONE -> movingOneBoneTool
                BoneToolType.MOVE_BONE_TREE -> movingBoneTreeTool
            }
        }
}