package com.flamingo.animator.editors.spineEditor

import android.app.ProgressDialog
import android.content.DialogInterface
import android.graphics.*
import android.graphics.drawable.Drawable
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import com.flamingo.animator.R
import com.flamingo.animator.adapters.spine.TimelineAdapter
import com.flamingo.animator.editors.drawUtils.AnimatedGifEncoder
import com.flamingo.animator.editors.drawUtils.clear
import com.flamingo.animator.editors.spineEditor.dataHolders.SlotContainer
import com.flamingo.animator.model.spine.Spine
import com.flamingo.animator.utils.commonUtils.log
import org.jetbrains.anko.dip
import java.io.File
import kotlin.concurrent.thread
import kotlin.math.max
import kotlin.math.min

/** Contains methods to draw a slot and border rect. Has temp transform matrices and border points. */
class SlotDrawer(val surfaceView: SpineSurfaceView) {
    
    private val scaleHandler get() = surfaceView.scaleHandler
    private val skinDataHandler get() = surfaceView.spineDataHolder
    
    private val tempMatrix = Matrix()
    
    /** Used to fill the selected slot.
     * left-top, right-top, right-down, left-down*/
    private val border = floatArrayOf(0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f)
    
    val selectedSlotPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = Color.argb(50, 0, 255, 0)
        strokeWidth = 5f
        style = Paint.Style.FILL
    }
    
    companion object {
        val SELECTION_COLOR = Color.argb(50, 0, 255, 0)
        val SECOND_SELECTION_COLOR = Color.argb(50, 255, 167, 38) // orange
        val HIGHLIGHT_COLOR = Color.argb(50, 255, 0, 0)
    }
    
    private val rotateButtonDrawable =
        VectorDrawableCompat.create(
            surfaceView.resources,
            R.drawable.ic_rotate,
            null
        )!!
    private val deleteButtonDrawable =
        VectorDrawableCompat.create(
            surfaceView.resources,
            R.drawable.ic_close,
            null
        )!!
//    private val cropButtonDrawable =
//        VectorDrawableCompat.create(
//            surfaceView.resources,
//            R.drawable.ic_crop,
//            null
//        )!!
    
    private val buttonPaint = Paint().apply {
        color = Color.WHITE
        style = Paint.Style.FILL
    }
    private val buttonBorderPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = Color.GRAY
        style = Paint.Style.STROKE
        strokeWidth = 1 * surfaceView.resources.displayMetrics.density // 1 dp
    }
    
    private val buttonRadius = surfaceView.dip(16).toFloat()
    
    private val bitmapBlurDrawPaint = Paint(Paint.FILTER_BITMAP_FLAG)
    
    fun drawSlot(
        slotContainer: SlotContainer,
        canvas: Canvas,
        scaleScreen: Boolean = true,
        blurBitmap: Boolean = false
    ) {
        prepareTempMatrixForSlot(slotContainer, scaleScreen)
        
        val bitmap = slotContainer.selectedAttachment.bitmap
        canvas.drawBitmap(bitmap, tempMatrix, if (blurBitmap) bitmapBlurDrawPaint else null)
    }
    
    /** all parameters are additional to default slot data!!! */
    private fun prepareTempMatrixForSlot(slotContainer: SlotContainer, scaleScreen: Boolean) {
        val attachmentContainer = slotContainer.selectedAttachment
        tempMatrix.reset()
        tempMatrix.postRotate(attachmentContainer.absoluteRotation)
        tempMatrix.postTranslate(attachmentContainer.absoluteX, attachmentContainer.absoluteY)
        if (scaleScreen) {
            tempMatrix.postConcat(scaleHandler.drawMatrix)
        }
    }
    
    private val borderPath = Path()
    
    fun drawSlotSelection(
        slotContainer: SlotContainer,
        canvas: Canvas,
        color: Int = SELECTION_COLOR,
        drawButtons: Boolean = false
    ) {
        prepareTempMatrixForSlot(slotContainer, true)
        
        slotContainer.resetBorderRect(border)
        tempMatrix.mapPoints(border)
        
        borderPath.reset()
        borderPath.moveTo(border[0], border[1])
        borderPath.lineTo(border[2], border[3])
        borderPath.lineTo(border[4], border[5])
        borderPath.lineTo(border[6], border[7])
        borderPath.lineTo(border[0], border[1])
        
        selectedSlotPaint.color = color
        canvas.drawPath(borderPath, selectedSlotPaint)
        
        if (drawButtons) {
            drawButton(canvas, border[0], border[1], deleteButtonDrawable)
            drawButton(canvas, border[2], border[3], rotateButtonDrawable)
        }
//        drawButton(canvas, border[6], border[7], cropButtonDrawable)
        
    }
    
    fun createPreview(slotContainers: List<SlotContainer>): Bitmap? {
        if (slotContainers.isEmpty()) {
            return null
        }
        var minX = Float.MAX_VALUE
        var minY = Float.MAX_VALUE
        var maxX = Float.MIN_VALUE
        var maxY = Float.MIN_VALUE
        slotContainers.forEach { slotContainer ->
            prepareTempMatrixForSlot(slotContainer, false)
            slotContainer.resetBorderRect(border)
            tempMatrix.mapPoints(border)
            minX = min5(minX, border[0], border[2], border[4], border[6])
            minY = min5(minY, border[1], border[3], border[5], border[7])
            maxX = max5(maxX, border[0], border[2], border[4], border[6])
            maxY = max5(maxY, border[1], border[3], border[5], border[7])
        }
        var width = maxX - minX
        var height = maxY - minY
        log("preview borders: ($minX, $minY), ($maxX, $maxY), size: ($width, $height)")
        val scale = Spine.MAX_PREVIEW_SIZE.toFloat() / max(width, height)
        height *= scale
        width *= scale
        val bitmap = Bitmap.createBitmap(width.toInt(), height.toInt(), Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        canvas.scale(scale, scale)
        canvas.translate(-minX, -minY)
        slotContainers.forEach {
            drawSlot(it, canvas, scaleScreen = false)
        }
        return bitmap
    }
    
    fun createGif(file: File, onSuccess: () -> Unit) {
        val activity = surfaceView.activity
        val animationDataHolder = surfaceView.animationDataHolder
        val spineDataHolder = surfaceView.spineDataHolder
        val animationContainer = animationDataHolder.currentAnimationContainer ?: return
        
        val initialTime = animationDataHolder.animationTime
        
        val maxAnimTime = animationContainer
            .boneTimelines.values.map { it.boneTimestamps.keys.max() ?: 0 }
            .max() ?: 0
        
        if (maxAnimTime == 0) {
            return
        }
    
        // bounding box max width and height
        val SIZE = 750
        val FRAMERATE = 18
        // from 1 to 20. 20 is the lowest and fastest, default is 10.
        val GIF_QUALITY = 20
        
        val stepSize = 1000 / FRAMERATE
        val sizeCheckStepSize = TimelineAdapter.TIMESTAMP_PRECISION
        val framesCount = maxAnimTime / stepSize
        
        
        val pd = ProgressDialog(activity)
        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
        pd.setTitle("Export to gif")
        pd.max = framesCount
        pd.progress = 0
        pd.setCancelable(false)
        var cancelled = false
        pd.setButton(
            DialogInterface.BUTTON_POSITIVE,
            activity.getString(android.R.string.cancel)
        ) { _, _ ->
            cancelled = true
        }
        pd.show()
        
        thread(isDaemon = true) {
            val slotContainers = spineDataHolder.slotContainers
            
            var minX = Float.MAX_VALUE
            var minY = Float.MAX_VALUE
            var maxX = Float.MIN_VALUE
            var maxY = Float.MIN_VALUE
            
            for (time in 0..maxAnimTime step sizeCheckStepSize) {
                animationDataHolder.animationTime = time
                animationDataHolder.updateAllBonesFromTimeline()
                
                slotContainers.forEach { slotContainer ->
                    prepareTempMatrixForSlot(slotContainer, false)
                    slotContainer.resetBorderRect(border)
                    tempMatrix.mapPoints(border)
                    minX = min5(minX, border[0], border[2], border[4], border[6])
                    minY = min5(minY, border[1], border[3], border[5], border[7])
                    maxX = max5(maxX, border[0], border[2], border[4], border[6])
                    maxY = max5(maxY, border[1], border[3], border[5], border[7])
                }
            }
            var width = maxX - minX
            var height = maxY - minY
            log("gif borders: ($minX, $minY), ($maxX, $maxY), size: ($width, $height)")
            
            val scale = SIZE.toFloat() / max(width, height)
            height *= scale
            width *= scale
            val bitmap = Bitmap.createBitmap(width.toInt(), height.toInt(), Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            canvas.scale(scale, scale)
            canvas.translate(-minX, -minY)
            
            
            val encoder = AnimatedGifEncoder()
            encoder.setDelay(stepSize)
            encoder.setRepeat(0)
            encoder.setQuality(GIF_QUALITY)
            
            
            encoder.start(file.outputStream())
            
            for (time in 0..maxAnimTime step stepSize) {
                if (cancelled) {
                    animationDataHolder.animationTime = initialTime
                    animationDataHolder.updateAllBonesFromTimeline()
                    activity.runOnUiThread {
                        pd.dismiss()
                    }
                    return@thread
                }
                canvas.clear(Color.WHITE)
                
                animationDataHolder.animationTime = time
                animationDataHolder.updateAllBonesFromTimeline()
                
                for (slotContainer in spineDataHolder.visibleSlotContainers) {
                    drawSlot(slotContainer, canvas, scaleScreen = false, blurBitmap = true)
                }
                encoder.addFrame(bitmap)
                pd.progress++
            }
            encoder.finish()
            
            animationDataHolder.animationTime = initialTime
            animationDataHolder.updateAllBonesFromTimeline()
            
            activity.runOnUiThread {
                pd.dismiss()
                onSuccess()
            }
        }
    }
    
    private fun min5(a: Float, b: Float, c: Float, d: Float, e: Float): Float {
        return min(a, min(b, min(c, min(d, e))))
    }
    
    private fun max5(a: Float, b: Float, c: Float, d: Float, e: Float): Float {
        return max(a, max(b, max(c, max(d, e))))
    }
    
    private fun SlotContainer.resetBorderRect(border: FloatArray) {
        // left-top, right-top, right-down, left-down
        border[0] = 0f
        border[1] = 0f
        border[2] = width.toFloat()
        border[3] = 0f
        border[4] = width.toFloat()
        border[5] = height.toFloat()
        border[6] = 0f
        border[7] = height.toFloat()
    }
    
    
    private fun drawButton(canvas: Canvas, x: Float, y: Float, drawable: Drawable) {
        canvas.drawCircle(x, y, buttonRadius, buttonPaint)
        canvas.drawCircle(x, y, buttonRadius, buttonBorderPaint)
        
        // TODO rotate drawable
        drawable.setBounds(
            x.toInt() - drawable.intrinsicWidth / 2,
            y.toInt() - drawable.intrinsicHeight / 2,
            x.toInt() + drawable.intrinsicWidth / 2,
            y.toInt() + drawable.intrinsicHeight / 2
        )
        drawable.draw(canvas)
    }
    
    private val hasSelection get() = skinDataHandler.selectedSlot != null
    
    fun isInsideRotateButton(x: Float, y: Float): Boolean {
        if (!hasSelection) {
            return false
        }
        val radSq = (x - border[2]) * (x - border[2]) +
                (y - border[3]) * (y - border[3])
        
        return radSq <= buttonRadius * buttonRadius;
    }
    
    fun isInsideDeleteButton(x: Float, y: Float): Boolean {
        if (!hasSelection) {
            return false
        }
        val radSq = (x - border[0]) * (x - border[0]) +
                (y - border[1]) * (y - border[1])
        
        return radSq <= buttonRadius * buttonRadius;
        
    }
    
    fun isInsideCropButton(x: Float, y: Float): Boolean {
        if (!hasSelection) {
            return false
        }
        val radSq = (x - border[6]) * (x - border[6]) +
                (y - border[7]) * (y - border[7])
        
        return radSq <= buttonRadius * buttonRadius;
        
    }
}