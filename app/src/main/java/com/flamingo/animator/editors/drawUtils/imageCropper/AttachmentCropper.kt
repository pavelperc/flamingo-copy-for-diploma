package com.flamingo.animator.editors.drawUtils.imageCropper

import android.graphics.*
import com.flamingo.animator.editors.spineEditor.SpineSurfaceView
import com.flamingo.animator.editors.spineEditor.dataHolders.AttachmentContainer
import java.io.File

class AttachmentCropper(val surfaceView: SpineSurfaceView) {
    
    private val matrix = Matrix()
    
    // can work from another thread and doesn't use realm objects.
    fun cropAttachment(attachmentContainer: AttachmentContainer, bitmap: Bitmap): Boolean {
        val cropRect = ImageCropper.cropImage(bitmap) ?: return false
        updateAttachmentWithCrop(attachmentContainer, cropRect)
        attachmentContainer.reloadImage()
        return true
    }
    
    /** Should be launched from realm transaction. */
    fun updateImageSizesFromBitmaps(attachmentContainer: AttachmentContainer) {
        with(attachmentContainer) {
            attachment.width = bitmap.width
            attachment.height = bitmap.height
        }
    }
    
    private fun updateAttachmentWithCrop(attachmentContainer: AttachmentContainer, cropRect: Rect) {
        if (attachmentContainer.shiftAfterCropCached) {
            shiftAttachmentForCrop(attachmentContainer, cropRect)
        }
        
        updateFile(attachmentContainer.imageFile, cropRect)
    }
    
    /** Shifts slot to the old position, as it was before crop. */
    private fun shiftAttachmentForCrop(attachmentContainer: AttachmentContainer, cropRect: Rect) {
        // transform with old width
        matrix.reset()
        matrix.postRotate(attachmentContainer.absoluteRotation)
        
        val newCropCenterX = cropRect.width() / 2f
        val newCropCenterY = cropRect.height() / 2f
        
        // transform the position of the new crop center.
        val cropCenter = floatArrayOf(
            cropRect.left + newCropCenterX, cropRect.top + newCropCenterY
        )
        matrix.mapPoints(cropCenter)
        
        // shift to the transformed new center position
        attachmentContainer.absoluteX += (cropCenter[0] - newCropCenterX).toInt()
        attachmentContainer.absoluteY += (cropCenter[1] - newCropCenterY).toInt()
        attachmentContainer.updateFromAbsolute()
    }
    
    private fun updateFile(file: File, cropRect: Rect) {
        val bitmap = BitmapFactory.decodeFile(file.absolutePath)
        
        val croppedBitmap =
            Bitmap.createBitmap(
                cropRect.width(),
                cropRect.height(),
                Bitmap.Config.ARGB_8888
            )
        val croppedCanvas = Canvas(croppedBitmap)
        
        val dstRect = Rect(0, 0, croppedBitmap.width, croppedBitmap.height)
        croppedCanvas.drawBitmap(bitmap, cropRect, dstRect, null)
        
        croppedBitmap.compress(Bitmap.CompressFormat.PNG, 100, file.outputStream())
    }
}