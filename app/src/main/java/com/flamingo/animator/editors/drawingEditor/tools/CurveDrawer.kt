package com.flamingo.animator.editors.drawingEditor.tools

import android.graphics.*
import android.view.MotionEvent
import com.flamingo.animator.editors.drawUtils.copy
import com.flamingo.animator.editors.drawUtils.drawBitmap
import com.flamingo.animator.editors.drawUtils.isNotStylus
import com.flamingo.animator.editors.drawUtils.pressurePath.PressurePathHelper
import com.flamingo.animator.editors.drawUtils.pressurePath.PressurePoint
import com.flamingo.animator.editors.drawingEditor.DrawingSurfaceView
import com.flamingo.animator.editors.drawingEditor.dataHolders.DrawDataHandler
import com.flamingo.animator.editors.drawingEditor.dataHolders.PathAction
import com.flamingo.animator.editors.drawingEditor.dataHolders.WidthPathAction
import com.flamingo.animator.utils.commonUtils.measureTime
import org.jetbrains.anko.dip
import kotlin.math.abs
import kotlin.math.min


interface Tool {
    companion object {
        val CLICK_WAIT_TIME = 150 // in millis
    }
    
    fun onTouchEvent(event: MotionEvent): Boolean
}

interface Drawer : Tool {
}


/** Draws a brush path. Use [CornerPathEffect] to make the line smooth. Doesn't support pressure. */
class OldCurveDrawer(
    val surfaceView: DrawingSurfaceView,
    val paint: Paint
) : Drawer {
    
    private var path = Path()
    
    private val drawDataHandler: DrawDataHandler
        get() = surfaceView.drawDataHandler
    
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> { // 0
                
                val coord = surfaceView.getScaledCoord(event.x, event.y)
                path = Path()
                path.moveTo(coord.x, coord.y)
            }
            MotionEvent.ACTION_MOVE -> { // 2
                val coord = surfaceView.getScaledCoord(event.x, event.y)
                path.lineTo(coord.x, coord.y)
                
                // reset to the last saved state.
                drawDataHandler.clearDirtyChangesCanvas()
                
                // not saveable action
                drawDataHandler.dirtyChangesCanvas.drawPath(path, paint)
                surfaceView.updateScreen(drawDirtyChanges = true)
            }
            MotionEvent.ACTION_UP -> { // 1
                val coord = surfaceView.getScaledCoord(event.x, event.y)
                path.lineTo(coord.x, coord.y)
                
                // saveable action
                drawDataHandler.currentRestorableCanvas.performAction(
                    PathAction(
                        path,
                        paint.copy() // to be able to change the paint later.
                    )
                )
                
                surfaceView.updateState(updateScreenBackWithDirtyChanges = true)
            }
        }
        return true
    }
}


/** Draws a brush path. Supports pressure. */
class CurveDrawer(
    val surfaceView: DrawingSurfaceView,
    val paint: Paint,
    var usePressure: Boolean
) : Drawer {
    
    private var points = mutableListOf<PressurePoint>()
    
    
    private val drawDataHandler: DrawDataHandler
        get() = surfaceView.drawDataHandler
    
    private var millis = 0L
    
    private var lastX = 0f
    private var lastY = 0f
    
    override fun onTouchEvent(event: MotionEvent): Boolean {
        var calibratedPressure = if (usePressure) min(1f, event.pressure * 1.3f) else 1f
        // pressure is speed
        val speedPressure = usePressure && event.isNotStylus
        
        when (event.action) {
            MotionEvent.ACTION_DOWN -> { // 0
                if (speedPressure) {
                    calibratedPressure = 0.3f
                }
                val coord =
                    surfaceView.getScaledCoordPressured(event.x, event.y, calibratedPressure)
                
                points = mutableListOf()
                points.add(coord)
                lastX = event.x
                lastY = event.y
            }
            MotionEvent.ACTION_MOVE -> { // 2
                // if timeDelta is 0????
                val timeDelta = System.currentTimeMillis() - millis
                
                val xSpeed = abs(event.x - lastX) / timeDelta
                val ySpeed = abs(event.y - lastY) / timeDelta
                // speed in screen pixels per ms
                // usually from 0.01 to 40
                val speed = xSpeed + ySpeed
                
                // fix for samsung pencil: double points make sharp edges
                // 20 fps for small speed
//                if (timeDelta < 50 && speed  < 1) {
//                    return true
//                }
                millis += timeDelta
                lastX = event.x
                lastY = event.y
                
                if (speedPressure) {
                    calibratedPressure = min(1f, 1.5f / speed)
                }
                
                
                val coord =
                    surfaceView.getScaledCoordPressured(event.x, event.y, calibratedPressure)
                points.add(coord)
                
                drawDataHandler.clearDirtyChangesCanvas()
                
                // not saveable action
                PressurePathHelper.drawWithWidth(points, paint, drawDataHandler.dirtyChangesCanvas)
                surfaceView.updateScreen(drawDirtyChanges = true)
            }
            MotionEvent.ACTION_UP -> { // 1
                // for thin endings
//                if (points.size > 3 && !speedPressure) {
//                    points[points.size - 2].pressure = points.last().pressure
//                }
                
                // ---> thin edges work bad with updateScreenBackWithDirtyChanges
                
                // saveable action
                drawDataHandler.currentRestorableCanvas.performAction(
                    WidthPathAction(
                        points,
                        paint.copy() // to be able to change the paint later.
                    )
                )
                
                // screenBack cache now holds the last dirtyChanges state.
                surfaceView.updateState(updateScreenBackWithDirtyChanges = true)
            }
        }
        return true
    }
}


/** Draws a brush path. Use [CornerPathEffect] to make the line smooth. */
class EraserDrawer(
    val surfaceView: DrawingSurfaceView
) : Drawer {
    
    /** Square or circle. */
    var isSquare: Boolean = false
    
    // https://www.gcssloop.com/customview/paint-base
    private val eraserPaintSquare = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
    }
    
    private val eraserPaintCircle = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        
        xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
        pathEffect = CornerPathEffect(500f) // just to smooth any corners
        strokeCap = Paint.Cap.ROUND
    }
    
    private var path = Path()
    
    private val eraserBorderPaint = Paint().apply {
        style = Paint.Style.STROKE
        strokeWidth = dip(1).toFloat()
        color = Color.RED
    }
    
    /** Converts dp to px. */
    private fun dip(dp: Int) = surfaceView.context.dip(dp)
    
    private val drawDataHandler: DrawDataHandler
        get() = surfaceView.drawDataHandler
    
    
    /** Padding for rectangle in dp for screen pixels, not image pixels.
     * Padding means half rect size. (or circle radius) */
    var eraserPadDp: Int = 24
        set(value) {
            field = value
            eraserPadPix = dip(value).toFloat()
        }
    
    /** Padding for rectangle in pix for screen pixels, not image pixels.
     * Padding means half rect size. (or circle radius) */
    private var eraserPadPix: Float = dip(eraserPadDp).toFloat()
    
    
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val paint = if (isSquare) eraserPaintSquare else eraserPaintCircle
        when (event.action) {
            MotionEvent.ACTION_DOWN, 211 -> { // 0
                // setup eraser shape
                // scale to transform size in picture to size in pixels.
                val scale = surfaceView.scaleHandler.realScale
                val padImg = eraserPadPix / scale
                
                if (isSquare) {
                    val eraserShape = Path()
                    eraserShape.addRect(-padImg, -padImg, padImg, padImg, Path.Direction.CW)
                    // rectangle shift. Larger shifts for better performance
                    val shift =
                        when {
                            padImg > 100 -> 4f
                            padImg > 60 -> 3f
                            padImg > 30 -> 2f
                            else -> 1f
                        }
                    eraserPaintSquare.pathEffect =
                        PathDashPathEffect(
                            eraserShape,
                            shift,
                            0f,
                            PathDashPathEffect.Style.TRANSLATE
                        )
                } else {
                    eraserPaintCircle.strokeWidth = padImg * 2
                }
                
                val coord = surfaceView.getScaledCoord(event.x, event.y)
                path = Path()
                path.moveTo(coord.x, coord.y)
            }
            MotionEvent.ACTION_MOVE, 213 -> { // 2
                val coord = surfaceView.getScaledCoord(event.x, event.y)
                path.lineTo(coord.x, coord.y)
                
                
                drawDataHandler.setDirtyCanvasCopyOfCurrentLayer()
                
                // not saveable action
                drawDataHandler.dirtyChangesCanvas.drawPath(path, paint)
                
                
                surfaceView.updateScreenWithCustomBuild { screenCanvas ->
                    buildImage(screenCanvas)
                    
                    // eraser border
                    if (isSquare) {
                        screenCanvas.drawRect(
                            event.x - eraserPadPix,
                            event.y - eraserPadPix,
                            event.x + eraserPadPix,
                            event.y + eraserPadPix,
                            eraserBorderPaint
                        )
                    } else {
                        screenCanvas.drawCircle(
                            event.x,
                            event.y,
                            eraserPadPix,
                            eraserBorderPaint
                        )
                    }
                }
            }
            MotionEvent.ACTION_UP, 212 -> { // 1
                val coord = surfaceView.getScaledCoord(event.x, event.y)
                path.lineTo(coord.x, coord.y)
                
                // saveable action
                drawDataHandler.currentRestorableCanvas.performAction(
                    PathAction(
                        path,
                        paint.copy() // to be able to change the paint later.
                    )
                )
                surfaceView.updateState()
            }
        }
        return true
    }
    
    private fun buildImage(screenCanvas: Canvas) {
        measureTime("Drawing all layers") {
            val imageBuilder = surfaceView.imageBuilder
//            val imageContainer = surfaceView.drawDataHandler.currentImageContainer
//            val canvas = imageBuilder.secondDirtyChangesCanvas
//            val bitmap = imageBuilder.secondDirtyChangesBitmap
//            val screenRect = surfaceView.scaleHandler.screenRectTransformed
//            canvas.clear(screenRect)
//            imageContainer.drawBackLayers(canvas, screenRect)
//            canvas.drawBitmap(
//                drawDataHandler.dirtyChangesBitmap,
//                screenRect,
//                screenRect,
//                null
//            )
//            imageContainer.drawFrontLayers(canvas, screenRect)
//            
//            screenCanvas.drawBitmap(
//                bitmap,
//                surfaceView.drawMatrix
//            )
            
            imageBuilder.drawUntilCurrentLayer(screenCanvas, false)
            screenCanvas.drawBitmap(
                drawDataHandler.dirtyChangesBitmap,
                surfaceView.drawMatrix
            )
            imageBuilder.drawAfterCurrentLayer(screenCanvas)
        }
        
        
    }
}

