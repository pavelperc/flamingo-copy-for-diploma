package com.flamingo.animator.editors.drawingEditor

import android.content.Context
import android.graphics.Matrix
import android.graphics.Rect
import android.graphics.RectF
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import com.flamingo.animator.editors.drawUtils.scale

interface ScaleDetectorCallback {
    fun onScaleStart()
    
    fun onScaleUpdate(scale: Float)
}


/** Canvas scale factor and translate to fit the surface. */
class FitScreenScale private constructor() {
    companion object {
        fun centerCoord(surfaceWidth: Int, surfaceHeight: Int) =
            FitScreenScale().apply { updateCenterCoord(surfaceWidth, surfaceHeight) }
        
        fun fitCanvas(
            surfaceWidth: Int,
            surfaceHeight: Int,
            canvasWidth: Int,
            canvasHeight: Int,
            padding: Int = 0
        ) = FitScreenScale().apply {
            updateFitCanvas(
                surfaceWidth,
                surfaceHeight,
                canvasWidth,
                canvasHeight,
                padding
            )
        }
    }
    
    /** What factor should we apply to canvas to fit the surface. */
    var scaleFactor: Float = 1f
    var translateX: Int = 0
    var translateY: Int = 0
    
    /** Default scale is 1.0. (0,0) is on the screen center. */
    fun updateCenterCoord(surfaceWidth: Int, surfaceHeight: Int) {
        scaleFactor = 1f
        translateX = surfaceWidth / 2
        translateY = surfaceHeight / 2
    }
    
    fun updateFitCanvas(
        surfaceWidth: Int,
        surfaceHeight: Int,
        canvasWidth: Int,
        canvasHeight: Int,
        padding: Int = 0
    ) {
        require(surfaceWidth > 0 && surfaceHeight > 0 && canvasHeight > 0 && canvasWidth > 0) {
            "All parameters for FitScreenScale should be positive!"
        }
        
        val sh = surfaceHeight.toFloat()
        val sw = surfaceWidth.toFloat()
        var ch = canvasHeight.toFloat()
        var cw = canvasWidth.toFloat()
        
        // which dimension should be equal for canvas and surface  
        val fitWidth = cw / ch > sw / sh // canvas is wider, than surface
        
        scaleFactor = if (fitWidth) (sw - padding * 2) / cw else (sh - padding * 2) / ch
        ch *= scaleFactor
        cw *= scaleFactor
        
        translateX = ((sw - cw) / 2).toInt()
        translateY = ((sh - ch) / 2).toInt()
    }
    
}

/**
 * This class creates a transform matrix [drawMatrix] from scale, detects pinch zooming touch events.
 * Scale is ended after the last finger is up. And started on two (or more) fingers touch.
 */
class ScaleHandler(
    val callback: ScaleDetectorCallback,
    context: Context,
    val fitScreenScale: FitScreenScale,
    private var width: Int,
    private var height: Int
) {
    private var lastFocusX = 0f
    private var lastFocusY = 0f
    
    val drawMatrix = Matrix()
    val inverseDrawMatrix = Matrix()
    
    /** Screen size rectangle. */
    private val screenRect = RectF(0f, 0f, width.toFloat(), height.toFloat())
    private val screenRectTransformedFloat = RectF()
    
    /** Screen rectangle, transformed with inverseDrawMatrix. */
    val screenRectTransformed = Rect()
    
    /** Resets draw matrix to [fitScreenScale] parameters and calls onScaleUpdate. */
    fun reset(screenWidth: Int, screenHeight: Int) {
        this.width = screenWidth
        this.height = screenHeight
        screenRect.set(0f, 0f, screenWidth.toFloat(), screenHeight.toFloat())
        
        resetMatrix()
        transformScreenRect()
        callback.onScaleUpdate(1f)
    }
    
    /** Sets screen scale. Scale is real!! not the one is passed to callback. */
    fun set(scale: Float, translateX: Float, translateY: Float) {
        drawMatrix.reset()
        drawMatrix.postScale(scale, scale)
        drawMatrix.postTranslate(translateX, translateY)
        drawMatrix.invert(inverseDrawMatrix)
        transformScreenRect()
    
        val realScale = drawMatrix.scale
        val fitScreenScale = realScale / fitScreenScale.scaleFactor
        callback.onScaleUpdate(fitScreenScale)
    }
    
    private fun transformScreenRect() {
        inverseDrawMatrix.mapRect(screenRectTransformedFloat, screenRect)
        screenRectTransformedFloat.roundOut(screenRectTransformed)
    }
    
    /** Reset matrix to fit the screen. */
    private fun resetMatrix() {
        drawMatrix.reset()
        drawMatrix.postScale(fitScreenScale.scaleFactor, fitScreenScale.scaleFactor)
        drawMatrix.postTranslate(
            fitScreenScale.translateX.toFloat(),
            fitScreenScale.translateY.toFloat()
        )
        drawMatrix.invert(inverseDrawMatrix)
    }
    
    init {
        // don't call onScaleUpdate on init
        resetMatrix()
    }
    
    
    private val mScrollDetector = object : GestureDetector.SimpleOnGestureListener() {
        override fun onScroll(
            e1: MotionEvent?,
            e2: MotionEvent?,
            distanceX: Float,
            distanceY: Float
        ): Boolean {
            drawMatrix.postTranslate(-distanceX, -distanceY);
            return true;
        }
    }
    
    //https://stackoverflow.com/questions/19418878/implementing-pinch-zoom-and-drag-using-androids-build-in-gesture-listener-and-s
    private val mScaleDetector = ScaleGestureDetector(context,
        object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
            override fun onScaleBegin(detector: ScaleGestureDetector): Boolean {
                lastFocusX = detector.focusX
                lastFocusY = detector.focusY
                return true
            }
            
            override fun onScale(detector: ScaleGestureDetector): Boolean {
                val transformationMatrix = Matrix()
                val focusX = detector.focusX
                val focusY = detector.focusY
                
                //Zoom focus is where the fingers are centered, 
                transformationMatrix.postTranslate(-focusX, -focusY)
                
                transformationMatrix.postScale(detector.scaleFactor, detector.scaleFactor)
                
                // Adding focus shift to allow for scrolling with two pointers down.
                val focusShiftX = focusX - lastFocusX
                val focusShiftY = focusY - lastFocusY
                transformationMatrix.postTranslate(focusX + focusShiftX, focusY + focusShiftY)
                drawMatrix.postConcat(transformationMatrix)
                lastFocusX = focusX
                lastFocusY = focusY
                
                drawMatrix.invert(inverseDrawMatrix)
                transformScreenRect()
                
                val realScale = drawMatrix.scale
                val fitScreenScale = realScale / fitScreenScale.scaleFactor
                callback.onScaleUpdate(fitScreenScale)
                return true
            }
        })
    
    /** Main draw matrix scale: value to transform real picture size to drawn size in pixels. */
    val realScale: Float
        get() = drawMatrix.scale
    
    /** This is true, since two fingers touched and until the last finger is up. */
    private var isScaling = false
    
    fun onTouchEvent(event: MotionEvent): Boolean {
        mScaleDetector.onTouchEvent(event);
        
        if (mScaleDetector.isInProgress) {
            // scaling start
            if (!isScaling) {
                // remove unfinished line before scaling start.
                callback.onScaleStart()
                
            }
            isScaling = true
            return true
        }
        // scaling is not in progress, but we are raising a finger after it
        if (isScaling && event.action == MotionEvent.ACTION_UP) {
            isScaling = false
            return true
        }
        if (isScaling) {
            return true
        }
        return false
    }
    
}