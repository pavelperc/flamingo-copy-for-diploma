package com.flamingo.animator.editors.spineEditor.dataHolders

import android.graphics.Bitmap
import com.flamingo.animator.model.spine.Bone
import com.flamingo.animator.model.spine.Slot

/** Slot wrapper with loaded images and temp coordinates. */
class SlotContainer constructor(
    val slot: Slot,
    val spineDataHolder: SpineDataHolder,
    cachedBitmaps: List<Bitmap>? = null,
    cachedAttachmentContainers: List<AttachmentContainer>? = null
) {
    val attachmentContainers = mutableListOf<AttachmentContainer>()
    // stored in db
    var selectedAttachmentPosition = 0
        private set // update together with Absolute
    
    // used for drawing
    var selectedAttachmentPositionAbsolute = 0
        private set
    
    // is updated when animation frame changes. Not a delta! Actual value
    var selectedAttachmentPositionTimeline = 0
    
    val isVisible get() = bone?.absoluteVisible ?: true
    
    val selectedAttachment: AttachmentContainer
        get() = attachmentContainers[selectedAttachmentPositionAbsolute]
    
    
    var bone: Bone? = null
    val boneIdOnLoad: Int
    
    val width get() = selectedAttachment.width
    val height get() = selectedAttachment.height
    
    init {
        if (cachedAttachmentContainers != null) {
            require(cachedAttachmentContainers.size == slot.attachments.size) {
                "invalid cached attachmentContainers count"
            }
            cachedAttachmentContainers.forEach { it.parent = this }
            attachmentContainers += cachedAttachmentContainers
            
        } else if (cachedBitmaps != null) {
            require(cachedBitmaps.size == slot.attachments.size) {
                "cachedBitmaps size ${cachedBitmaps.size} should be the same as " +
                        "the number of attachments: ${slot.attachments.size}."
            }
            attachmentContainers += cachedBitmaps.zip(slot.attachments).map { (bm, atch) ->
                AttachmentContainer(atch, spineDataHolder, this, bm)
            }
        } else {
            attachmentContainers += slot.attachments.map {
                AttachmentContainer(it, spineDataHolder, this)
            }
            
        }
        updateSelectedAttachmentPositionWithAbsolute(slot.selectedAttachmentPosition)
        boneIdOnLoad = slot.boneId
    }
    
    // needs transaction
    fun save() {
        slot.boneId = bone?.id ?: 0
        slot.selectedAttachmentPosition = selectedAttachmentPosition
        attachmentContainers.forEach { it.save() }
    }
    
    fun updateAbsolute(timelineMode: Boolean) {
        selectedAttachmentPositionAbsolute = if (timelineMode) {
            selectedAttachmentPositionTimeline % attachmentContainers.size
        } else {
            selectedAttachmentPosition % attachmentContainers.size
        }
        
        attachmentContainers.forEach { it.updateAbsolute() }
    }
    
    fun updateFromAbsolute(timelineMode: Boolean) {
        if (timelineMode) {
            selectedAttachmentPositionTimeline = selectedAttachmentPositionAbsolute
        } else {
            selectedAttachmentPosition = selectedAttachmentPositionAbsolute
        }
        attachmentContainers.forEach { it.updateFromAbsolute() }
    }
    
    fun swapAttachment() {
        updateSelectedAttachmentPositionWithAbsolute(selectedAttachmentPosition + 1)
    }
    
    /** Should be used from setup pose mode. Updates stored in db and drawn position. */
    fun updateSelectedAttachmentPositionWithAbsolute(position: Int) {
        selectedAttachmentPositionAbsolute = position % attachmentContainers.size
        selectedAttachmentPosition = position % attachmentContainers.size
    }
}
