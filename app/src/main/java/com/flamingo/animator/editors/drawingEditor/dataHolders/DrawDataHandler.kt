package com.flamingo.animator.editors.drawingEditor.dataHolders

import com.flamingo.animator.activity.DrawingSetup
import com.flamingo.animator.editors.drawingEditor.ScaleHandler
import com.flamingo.animator.repository.AssetRepo

/**
 * This is a main class, containing all structure of layers, undo states and save files.
 * [drawingContainer] - is either animation or just one layered image.
 * Stores a dirtyChangesCanvas for temporal changes.
 *
 * Should be used as a facade for all drawing data.
 */
class DrawDataHandler(
    drawingSetup: DrawingSetup,
    val assetRepo: AssetRepo,
    private val scaleHandlerReceiver: () -> ScaleHandler,
    backgroundColor: Int
) {
    val isAnimation get() = drawingContainer is AnimContainer
    
    /** Holds either animation or one image.
     * Knows image size and current restorableCanvas where to draw. */
    val drawingContainer: DrawingContainer
    
    val scaleHandler: ScaleHandler get() = scaleHandlerReceiver()
    
    /** Container for animation. It is valid if drawingSetup.isAnimation = true*/
    val animContainer: AnimContainer?
        get() = drawingContainer as? AnimContainer
    
    init {
        if (drawingSetup.isAnimation) {
            val anim = assetRepo.findAnimById(drawingSetup.animationId)
            drawingContainer = AnimContainer(anim, backgroundColor, assetRepo)
            
        } else {
            val layeredImage = assetRepo.findLayeredImageById(drawingSetup.layeredImageId)
            drawingContainer = ImageContainer(layeredImage, backgroundColor, assetRepo)
        }
    }
    
    /** Helper class to collect all layers and a background. */
    val imageBuilder = ImageBuilder(this)
    
    val currentRestorableCanvas: RestorableCanvas
        get() = drawingContainer.restorableCanvas
    
    val currentImageContainer: ImageContainer
        get() = drawingContainer.currentImageContainer
    
    val currentLayerContainer: LayerContainer
        get() = currentImageContainer.selectedLayerContainer
    
    val width: Int
        get() = drawingContainer.width
    val height: Int
        get() = drawingContainer.height
    
    
    /** Invalidate the background image. */
    fun invalidateBackground() {
        imageBuilder.invalidateBackground()
    }
    
    
    val dirtyChangesCanvas get() = imageBuilder.dirtyChangesCanvas
    val dirtyChangesBitmap get() = imageBuilder.dirtyChangesBitmap
    
    fun saveAll() = drawingContainer.saveAll()
    
    fun clearDirtyChangesCanvas() {
        imageBuilder.clearDirtyChangesCanvas()
    }
    
    fun setDirtyCanvasCopyOfCurrentLayer() {
        imageBuilder.setDirtyCanvasCopyOfCurrentLayer()
    }
}

