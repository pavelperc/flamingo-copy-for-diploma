package com.flamingo.animator.editors.drawingEditor.dataHolders

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.flamingo.animator.model.Animation
import com.flamingo.animator.model.Frame
import com.flamingo.animator.model.Layer
import com.flamingo.animator.repository.AssetRepo
import com.flamingo.animator.utils.commonUtils.log
import com.flamingo.animator.utils.realmUtils.transaction
import java.io.File
import java.io.FileOutputStream


/** Animation or one layered image. */
interface DrawingContainer {
    /** Returns the number of saved images. */
    fun saveAll(): Int
    
    val restorableCanvas: RestorableCanvas
    val width: Int
    val height: Int
    
    val currentImageContainer: ImageContainer
}

class LayerContainer(
    val layer: Layer,
    val assetsFolder: File,
    /** Should be null to take the default from file, or some preloaded image. */
    preloadedBitmap: Bitmap? = null
) {
    val restorableCanvas: RestorableCanvas
    
    private val file = layer.imageReq.getFile(assetsFolder)
    
    private var lastSavedAction = 0
    
    fun setUnsaved() {
        lastSavedAction = -1
    }
    
    init {
        val bitmap = preloadedBitmap
            ?: BitmapFactory.decodeFile(file.absolutePath)
            ?: throw IllegalArgumentException("Can not load image ${file.name}")
        
        restorableCanvas = RestorableCanvas(
            bitmap.width,
            bitmap.height
        ) { canvas ->
            canvas.drawBitmap(bitmap, 0f, 0f, null)
        }
    }
    
    
    val isSaved: Boolean
        get() = lastSavedAction == restorableCanvas.actionCounter
    
    fun saveImage(): Int {
        if (!isSaved) {
            restorableCanvas.mainBitmap.compress(Bitmap.CompressFormat.PNG, 100, FileOutputStream(file))
            lastSavedAction = restorableCanvas.actionCounter
            layer.transaction { lastEdited = System.currentTimeMillis() }
            log("Saved image ${file.name}")
            return 1
        }
        return 0
    }
}

class AnimContainer(
    val animation: Animation,
    val backgroundColor: Int,
    val assetRepo: AssetRepo
) : DrawingContainer {
    val imageContainers = mutableListOf<ImageContainer>()
    
    val size: Int
        get() = imageContainers.size
    
    init {
        imageContainers += animation.frames.map {
            ImageContainer(
                it.imageReq,
                backgroundColor,
                assetRepo
            )
        }
    }
    
    var currentFramePosition = size - 1
        private set
    
    val currentImage: ImageContainer
        get() = imageContainers[currentFramePosition]
    
    val currentFrame: Frame
        get() = animation.frames[currentFramePosition]!!
    
    val previousFrameImage: Bitmap?
        get() = if (currentFramePosition == 0) {
            null
        } else {
            imageContainers[currentFramePosition - 1].builtImage
        }
    
    override val currentImageContainer: ImageContainer
        get() = currentImage
    
    override val restorableCanvas: RestorableCanvas
        get() = currentImage.restorableCanvas
    
    override val width: Int
        get() = currentImage.layeredImage.width
    override val height: Int
        get() = currentImage.layeredImage.height
    
    override fun saveAll(): Int {
        val savedCount = imageContainers.map { it.saveAll() }.sum()
        if (savedCount > 0) {
            animation.transaction { spriteSaved = false }
        }
        return savedCount
    }
    
    fun addFrame(duplicate: Boolean) {
        val imageToDuplicate = if (duplicate) currentImage.layeredImage else null
        // save all to copy the files.
        if (duplicate) {
            saveAll()
        }
        
        val frame = assetRepo.transaction {
            // insert to animation inside.
            addNewFrame(animation, width, height, currentFramePosition + 1, imageToDuplicate)
        } 
        
        val imageContainer = ImageContainer(frame.imageReq, backgroundColor, assetRepo)
        
        // add after current
        imageContainers.add(currentFramePosition + 1, imageContainer)
        currentFramePosition++
        animation.transaction { spriteSaved = false }
    }
    
    fun prevFrame() {
        if (currentFramePosition > 0) {
            currentFramePosition--
        }
    }
    
    fun nextFrame() {
        if (currentFramePosition < size - 1) {
            currentFramePosition++
        }
    }
    
    fun deleteCurrFrame() {
        if (size == 1) {
            return
        }
        val frame = currentFrame
        animation.transaction { frames.removeAt(currentFramePosition) }
        assetRepo.removeFrame(frame)
        imageContainers.removeAt(currentFramePosition)
        
        if (currentFramePosition == size) {
            currentFramePosition--
        }
        
        animation.transaction { spriteSaved = false }
    }
    
    fun setFramePosition(position: Int) {
        if (position in 0 until size) {
            currentFramePosition = position
        }
    }
}