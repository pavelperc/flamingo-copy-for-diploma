package com.flamingo.animator.editors.drawUtils.pressurePath

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import kotlin.math.abs


class PressurePoint(
    val x: Float,
    val y: Float,
    /** Pressure or velocity of the brush. Usually from 0 (no pressure) to 1, but may be sometimes larger.*/
    var pressure: Float
)

object PressurePathHelper {
    private val path = Path()
    
    
    /** Changes [paint] width on pressure. */
    fun drawWithWidth(points: List<PressurePoint>, paint: Paint, canvas: Canvas) {
        val initWidth = paint.strokeWidth
        val pressureDelta = when {
            initWidth < 16 -> 0.07f
            initWidth < 40 -> 1 / initWidth
            initWidth < 60 -> 1.5f / initWidth
            else -> 3 / initWidth
        }
        
        
        if (points.isEmpty()) {
            return
        }
        var pressure = points[0].pressure
        var x = points[0].x
        var y = points[0].y
        // extra points for bezier
        path.reset()
        path.moveTo(x, y)
//        val pointPaint = Paint().apply {
//            strokeWidth = 10f
//        }
//        val middlePaint = Paint().apply {
//            strokeWidth = 10f
//            color = Color.BLUE
//        }
        
        for (i in 1 until points.size) {
            val point = points[i]
            val middleX = (point.x + x) / 2
            val middleY = (point.y + y) / 2
            
            // add path
            path.quadTo(x, y, middleX, middleY)
            
            val desiredPressure = point.pressure
            if (abs(pressure - desiredPressure) > 0.05) {
                // draw last and reset
                paint.strokeWidth = initWidth * pressure
//                paint.color = if (paint.color == Color.RED) Color.GREEN else Color.RED
                canvas.drawPath(path, paint)
                path.reset()
                path.moveTo(middleX, middleY)
                if (desiredPressure > pressure) {
                    pressure += pressureDelta
                } else {
                    pressure -= pressureDelta
                }
            }
//            canvas.drawPoint(point.x, point.y, pointPaint)
//            canvas.drawPoint(middleX, middleY, middlePaint)
            
            // update points
            x = point.x
            y = point.y
        }
        // draw last
        paint.strokeWidth = initWidth * pressure
        path.lineTo(points.last().x, points.last().y)
//        paint.color = if (paint.color == Color.RED) Color.GREEN else Color.RED
        canvas.drawPath(path, paint)
        
        // restore width
        paint.strokeWidth = initWidth
    }
}