package com.flamingo.animator.editors.spineEditor.dataHolders

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import com.flamingo.animator.model.spine.Attachment
import com.flamingo.animator.model.spine.Bone
import com.flamingo.animator.utils.commonUtils.optFloat
import com.flamingo.animator.utils.commonUtils.safeJson
import org.json.JSONObject

class AttachmentContainer(
    val attachment: Attachment,
    val spineDataHolder: SpineDataHolder,
    var parent: SlotContainer,
    cachedBitmap: Bitmap? = null
) {
    companion object {
        private val matrix = Matrix()
        private val point = floatArrayOf(0f, 0f)
    }
    
    val width: Int get() = bitmap.width
    val height: Int get() = bitmap.height
    
    // this copy is used in AttachmentCropper, because it can not access realm from the other thread!!
    var imageFile = attachment.imageReq.getFile(spineDataHolder.assetRepo.assetsDir)
    
    var bitmap: Bitmap
        private set
    
    // relative coordinates from bone. If no bone was attached, they are equal absolute
    var x: Float = 0f
    var y: Float = 0f
    var rotation: Float = 0f
    
    var absoluteX: Float = 0f
    var absoluteY: Float = 0f
    var absoluteRotation: Float = 0f
    
    var shiftAfterCropCached = attachment.shiftAfterCrop
    
    val bone: Bone? get() = parent.bone
    
    init {
        bitmap = cachedBitmap ?: BitmapFactory.decodeFile(imageFile.absolutePath)
        
        // init json data (it may be unset if the slot is just created)
        val jo = attachment.jsonData.safeJson()
        x = jo.optFloat("x")
        y = jo.optFloat("y")
        rotation = jo.optFloat("rotation")
    
        // set absolute without bone.
        // if boneId is not 0, it will be updated again
        updateAbsolute()
    }
    
    fun updateAbsolute() {
        val bone = bone
        if (bone == null) {
            absoluteX = x
            absoluteY = y
            absoluteRotation = rotation
            return
        }
        matrix.reset()
        matrix.postRotate(bone.absoluteRotation)
        matrix.postTranslate(bone.absoluteX, bone.absoluteY)
        
        point[0] = x
        point[1] = y
        matrix.mapPoints(point)
        
        absoluteX = point[0]
        absoluteY = point[1]
        absoluteRotation = bone.absoluteRotation + rotation
    }
    
    fun updateFromAbsolute() {
        val bone = bone
        if (bone == null) {
            x = absoluteX
            y = absoluteY
            rotation = absoluteRotation
            return
        }
        
        matrix.reset()
        // what about center point???
        matrix.postRotate(bone.absoluteRotation)
        matrix.postTranslate(bone.absoluteX, bone.absoluteY)
        matrix.invert(matrix)
        
        point[0] = absoluteX
        point[1] = absoluteY
        matrix.mapPoints(point)
        
        x = point[0]
        y = point[1]
        rotation = absoluteRotation - bone.absoluteRotation
    }
    
    // needs transaction
    fun save() {
        attachment.jsonData = JSONObject().apply {
            put("x", x)
            put("y", y)
            put("rotation", rotation)
            put("boneId", bone?.id ?: 0)
        }.toString()
    }
    
    fun reloadImage() {
        bitmap = BitmapFactory.decodeFile(imageFile.absolutePath)
    }
    
    fun updateCachedImageFile() {
        imageFile = attachment.imageReq.getFile(spineDataHolder.assetRepo.assetsDir)
    }
    
    /** Updated cached realm value shiftAfterCrop */
    fun updateShiftAfterCrop() {
        shiftAfterCropCached = attachment.shiftAfterCrop
    }
}
