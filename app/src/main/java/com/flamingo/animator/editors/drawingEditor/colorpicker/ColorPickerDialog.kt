package com.flamingo.animator.editors.drawingEditor.colorpicker

import android.content.Context
import android.content.DialogInterface
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.graphics.ColorUtils
import androidx.fragment.app.DialogFragment
import com.flamingo.animator.editors.drawUtils.CheckerBoard
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.customView
import org.jetbrains.anko.support.v4.UI
import yuku.ambilwarna.AmbilWarnaDialog
import kotlin.math.min

/** Colored circle button. */
class ColorButton : View {
    constructor(context: Context, attrSet: AttributeSet, defStyleAttr: Int, defStyleRes: Int)
            : super(context, attrSet, defStyleAttr, defStyleRes)
    
    constructor(context: Context, attrSet: AttributeSet, defStyleAttr: Int) : super(context, attrSet, defStyleAttr)
    constructor(context: Context, attrSet: AttributeSet) : super(context, attrSet)
    constructor(context: Context) : super(context)
    
    var color: Int
        get() = fillPaint.color
        set(value) {
            fillPaint.color = value
            
            setBorderDarkness(value)
            
            invalidate()
        }
    
    private fun setBorderDarkness(newColor: Int) {
        val hsl = FloatArray(3)
        ColorUtils.colorToHSL(newColor, hsl)
        
        if (hsl[2] < 0.4f) { // lightness
            hsl[2] = 0.9f
        } else {
            hsl[2] = 0.2f
        }
        borderPaint.color = ColorUtils.HSLToColor(hsl)
        selectBorderPaint.color = ColorUtils.HSLToColor(hsl)
    }
    
    private val fillPaint = Paint().apply {
        color = Color.RED
    }
    
    private val borderPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        color = Color.BLACK
        strokeWidth = dip(1).toFloat()
    }
    
    private val selectBorderPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        color = Color.BLACK
        strokeWidth = dip(4).toFloat()
    }
    
    private val checkeredPaint = CheckerBoard.createCheckeredPaint()
    
    private val pad = dip(4).toFloat()
    
    override fun onDraw(canvas: Canvas) {
        
        val centerX = width / 2
        val centerY = height / 2
        val radius = min(width, height) / 2 - pad
        
        fun drawShape(paint: Paint) {
            canvas.drawRoundRect(
                centerX - radius,
                centerY - radius,
                centerX + radius,
                centerY + radius,
                pad * 2,
                pad * 2,
                paint
            )
        }
        if (fillPaint.alpha < 255) {
            drawShape(checkeredPaint)
        }
        drawShape(fillPaint)
        if (isSelected) {
            drawShape(selectBorderPaint)
        } else {
            drawShape(borderPaint)
        }
        
    }
}

class ColorPickerDialog(
    /** It is used only on dialog dismiss! */
    val onSelected: (newColor: Int) -> Unit
) : DialogFragment() {
    // https://clrs.cc/ with some edits
    private val paletteColors = arrayOf(
        0xff00264d, // NAVY - lighter
        0xff0074D9, // BLUE
        0xff7FDBFF, // AQUA
        0xff39CCCC, // TEAL
        0xff3D9970, // OLIVE
        0xff2ECC40, // GREEN
        0xff01FF70, // LIME
        0xffFFDC00, // YELLOW
        0xffFF851B, // ORANGE
        0xffFF4136, // RED
        0xff69300C, // brown - added
        0xff85144b, // MAROON
        0xffF012BE, // FUCHSIA
        0xffB10DC9, // PURPLE
        0xFF000000, // BLACK - edited
        0xffAAAAAA, // GRAY
        0xffDDDDDD, // SILVER
        0xffFFFFFF  // WHITE
    ).map { it.toInt() }
    
    /** Button, which has a selection mark. */
    private var selectedButton: ColorButton? = null
    
    
    /** This color shows current selected color.
     * By default it is latestColor[0]. (if no on selected). */
    var currentColor: Int = Color.WHITE
        private set
    
    private var latestColorButtons: Array<ColorButton>? = null
    val latestColors = IntArray(10) { Color.WHITE }
    
    /** Adds the latest color to the menu. */
    fun addLatestColor(newColor: Int) {
        var replaceIndex = latestColors.indexOf(newColor)
        if (replaceIndex == -1) {
            replaceIndex = latestColors.size - 1
        }
        for (i in replaceIndex downTo 1) {
            latestColors[i] = latestColors[i - 1]
        }
        latestColors[0] = newColor
        
        latestColorButtons?.forEachIndexed { i, btn -> btn.color = latestColors[i] }
        currentColor = latestColors[0]
    }
    
    fun loadLatestColors(colors: IntArray) {
        (0 until min(latestColors.size, colors.size)).forEach { i ->
            latestColors[i] = colors[i]
        }
        latestColorButtons?.forEachIndexed { i, btn -> btn.color = latestColors[i] }
        currentColor = latestColors[0]
    }
    
    
    private val onColorClickListener = View.OnClickListener { colorButton ->
        colorButton as ColorButton
        
        selectedButton?.isSelected = false
        colorButton.isSelected = true
        selectedButton = colorButton
        
        currentColor = colorButton.color
    }
    
    override fun onDestroyView() {
        super.onDestroyView()
        latestColorButtons = null
    }
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return UI {
            customView<LinearLayout> {
                scrollView {
                    verticalLayout {
                        padding = dip(8)
                        textView("Palette:") {
                            textSize = 16f
                        }
        
                        gridLayout {
                            columnCount = 5
            
                            repeat(paletteColors.size) { i ->
                                val colorButton = ColorButton(context)
                                val lp = ViewGroup.MarginLayoutParams(dip(48), dip(48)).apply {
                                    margin = dip(4)
                                }
                                this.addView(colorButton, i, lp)
                                colorButton.color = paletteColors[i]
                
                                colorButton.setOnClickListener(onColorClickListener)
                            }
                        }
                        textView("Latest Colors:") {
                            textSize = 16f
                        }
                        gridLayout {
                            columnCount = 5
            
                            latestColorButtons = Array(latestColors.size) { i ->
                                val colorButton = ColorButton(context)
                                colorButton.color = latestColors[i]
                
                                val lp = ViewGroup.MarginLayoutParams(dip(48), dip(48)).apply {
                                    margin = dip(4)
                                }
                                this.addView(colorButton, lp)
                                colorButton.setOnClickListener(onColorClickListener)
                
                                colorButton
                            }
                        }
        
                        button("Custom") {
                            setOnClickListener {
                                AmbilWarnaDialog(context, currentColor, object : AmbilWarnaDialog.OnAmbilWarnaListener {
                                    override fun onOk(dialog: AmbilWarnaDialog, color: Int) {
                                        selectedButton?.isSelected = false
                                        selectedButton = null
                                        currentColor = color
                                        dismiss()
                                    }
                    
                                    override fun onCancel(dialog: AmbilWarnaDialog?) = Unit
                                }).show()
                            }
            
                        }
                    }
                }
            }
        }.view
    }
    
    override fun onDismiss(dialog: DialogInterface?) {
        if (currentColor != latestColors[0]) {
            onSelected(currentColor)
        }
        super.onDismiss(dialog)
    }
}