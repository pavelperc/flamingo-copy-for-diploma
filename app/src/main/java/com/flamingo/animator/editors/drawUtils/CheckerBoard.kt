package com.flamingo.animator.editors.drawUtils

import android.graphics.*
import android.graphics.drawable.PaintDrawable

object CheckerBoard {
    fun createCheckeredBitmap(pixelSize: Int = 20): Bitmap {
        val bitmap = Bitmap.createBitmap(pixelSize * 2, pixelSize * 2, Bitmap.Config.ARGB_8888)
        
        val fill1 = Paint().apply {
            style = Paint.Style.FILL
            color = Color.parseColor("#D6D6D6")
        }
        val fill2 = Paint().apply {
            style = Paint.Style.FILL
            color = Color.WHITE
        }
        
        val pad = pixelSize.toFloat()
        
        val canvas = Canvas(bitmap)
        canvas.drawRect(0f, 0f, pad, pad, fill1)
        canvas.drawRect(pad, 0f, pad * 2, pad, fill2)
        canvas.drawRect(0f, pad, pad, pad * 2, fill2)
        canvas.drawRect(pad, pad, pad * 2, pad * 2, fill1)
        return bitmap
    }
    
    private val cachedCheckeredBitmap = createCheckeredBitmap()
    
    fun createCheckeredPaint(): Paint {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.shader = BitmapShader(cachedCheckeredBitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT)
        return paint
    }
    
    fun createCheckeredDrawable(): PaintDrawable {
        val drawable = PaintDrawable()
        val checkeredPaint = createCheckeredPaint()
        drawable.paint.set(checkeredPaint)
        return drawable
    }
    
}

fun Paint.copy() = Paint(this)