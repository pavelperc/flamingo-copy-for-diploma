package com.flamingo.animator.editors.drawingEditor.tools

import android.graphics.*
import android.view.MotionEvent
import com.flamingo.animator.editors.drawUtils.clear
import com.flamingo.animator.editors.drawUtils.expand
import com.flamingo.animator.editors.drawUtils.scale
import com.flamingo.animator.editors.drawingEditor.DrawingSurfaceView
import com.flamingo.animator.editors.drawingEditor.dataHolders.RestorableAction

class CompoundToolsContainer(val surfaceView: DrawingSurfaceView) {
    
    val isCompoundToolEnabled: Boolean get() = currentDrawer != null
    var currentDrawer: CompoundDrawer? = null
    
    private val imageShifter = ImageShifter(surfaceView)
    fun setImageShifter() {
        imageShifter.reset()
        currentDrawer = imageShifter
    }
    
    fun submitDrawer() {
        currentDrawer?.submit()
        currentDrawer = null
    }
    
    fun cancelDrawer() {
        currentDrawer = null
        surfaceView.updateScreen()
    }
    
    val isImageShiftEnabled get() = currentDrawer === imageShifter
}

interface CompoundDrawer : Drawer {
    
    /** This function should be used instead of default update, but not in onTouch event.
     * For example it can be used after scaling. */
    fun updateScreen()
    
    /** Submits the performable action to the current restorable canvas. */
    fun submit()
    
    fun reset()
}


class ImageShifter(val surfaceView: DrawingSurfaceView) : CompoundDrawer {
    // fingers drag start (in screen coords)
    private var startX: Float = 0f
    private var startY: Float = 0f
    
    // saved shift after the last fingers drag (in image coords)
    private var savedShiftX: Int = 0
    private var savedShiftY: Int = 0
    
    private val shiftRectPaint = Paint().apply {
        color = Color.GREEN
        strokeWidth = 6.0f
        style = Paint.Style.STROKE
        strokeJoin = Paint.Join.ROUND
    }
    
    var shiftAllLayers: Boolean = false
    
    private val inverseDrawMatrix get() = surfaceView.inverseDrawMatrix
    
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                startX = event.x
                startY = event.y
            }
            MotionEvent.ACTION_MOVE -> {
                val currShiftX = ((event.x - startX) * inverseDrawMatrix.scale + savedShiftX).toInt()
                val currShiftY = ((event.y - startY) * inverseDrawMatrix.scale + savedShiftY).toInt()
                updateScreen(currShiftX, currShiftY)
                
            }
            MotionEvent.ACTION_UP -> {
                savedShiftX += ((event.x - startX) * inverseDrawMatrix.scale).toInt()
                savedShiftY += ((event.y - startY) * inverseDrawMatrix.scale).toInt()
                updateScreen(savedShiftX, savedShiftY)
                
            }
        }
        return true
    }
    
    // used to update from the outside (after scaling, for example)
    override fun updateScreen() {
        updateScreen(savedShiftX, savedShiftY)
    }
    
    // cached border in picture coords
    private val shiftImageRect = RectF()
    
    // shifts in image coords
    private fun updateScreen(shiftX: Int, shiftY: Int) {
        // rect to draw on the screen
        shiftImageRect.set(0f, 0f, surfaceView.pictureWidth.toFloat(), surfaceView.pictureHeight.toFloat())
        shiftImageRect.offset(
            shiftX.toFloat(),
            shiftY.toFloat()
        )
        surfaceView.drawMatrix.mapRect(shiftImageRect)
        shiftImageRect.expand(shiftRectPaint.strokeWidth / 2)
        
        surfaceView.updateScreenWithCustomBuild(
            customBuild = { imageBuilder ->
                imageBuilder.buildImageWithShift(shiftX, shiftY, shiftAllLayers)
            },
            additionalDraw = { canvas ->
                canvas.drawRect(shiftImageRect, shiftRectPaint)
            })
    }
    
    override fun submit() {
        // saveable action
        
        val shiftAction = ShiftAction(
            savedShiftX,
            savedShiftY,
            surfaceView.drawDataHandler.dirtyChangesCanvas,
            surfaceView.drawDataHandler.dirtyChangesBitmap
        )
        
        if (shiftAllLayers) {
            surfaceView.currentImageContainer.layerContainers.forEach {
                it.restorableCanvas.performAction(shiftAction)
            }
        } else {
            surfaceView.currentRestorableCanvas.performAction(shiftAction)
        }
        
        surfaceView.updateState()
    }
    
    override fun reset() {
        savedShiftX = 0
        savedShiftY = 0
    }
}


class ShiftAction(
    val shiftX: Int,
    val shiftY: Int,
    val dirtyChangesCanvas: Canvas,
    val dirtyChangesBitmap: Bitmap
) : RestorableAction {
    
    override fun perform(canvas: Canvas, bitmap: Bitmap) {
        dirtyChangesCanvas.clear()
        dirtyChangesCanvas.drawBitmap(bitmap, shiftX.toFloat(), shiftY.toFloat(), null)
        canvas.clear()
        canvas.drawBitmap(dirtyChangesBitmap, 0f, 0f, null)
    }
}