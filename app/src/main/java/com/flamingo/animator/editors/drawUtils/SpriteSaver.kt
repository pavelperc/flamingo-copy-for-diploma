package com.flamingo.animator.editors.drawUtils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import com.flamingo.animator.model.Animation
import com.flamingo.animator.repository.AssetRepo
import com.flamingo.animator.utils.realmUtils.transaction
import java.io.FileOutputStream

object SpriteSaver {
    
    fun saveSprite(anim: Animation, assetRepo: AssetRepo) {
        val frameWidth = anim.width
        val frameHeight = anim.height
        
        val spriteBitmap = Bitmap.createBitmap(
            frameWidth * anim.frames.size,
            frameHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(spriteBitmap)
        canvas.clear()
        
        var i = 0
        anim.frames.map { it.imageReq.builtImageReq.getFile(assetRepo.assetsDir) }.forEach { file ->
            val frameBitmap = BitmapFactory.decodeFile(file.absolutePath)
            canvas.drawBitmap(frameBitmap, (frameWidth * i).toFloat(), 0f, null)
            i++
        }
        
        val spriteFile = anim.spriteImageReq.getFile(assetRepo.assetsDir)
        spriteBitmap.compress(Bitmap.CompressFormat.PNG, 100, FileOutputStream(spriteFile))
        anim.transaction { spriteSaved = true }
    }
}