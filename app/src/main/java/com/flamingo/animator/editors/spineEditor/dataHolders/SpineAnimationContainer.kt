package com.flamingo.animator.editors.spineEditor.dataHolders

import com.flamingo.animator.model.spine.SpineAnimation
import com.flamingo.animator.model.spine.animation.timelines.BoneTimeline
import com.flamingo.animator.utils.commonUtils.safeJson
import com.flamingo.animator.utils.realmUtils.transaction
import org.json.JSONObject

/** Wrapper for database animation. Holds timeline objects, parsed from json. */
class SpineAnimationContainer(val spineAnimation: SpineAnimation) {
    
    /** Map bone id to bone timeline. */
    val boneTimelines = mutableMapOf<Int, BoneTimeline>()
    
    init {
        val timelinesJson = spineAnimation.timelinesJson.safeJson()
        
        val boneTimelinesJson = timelinesJson.optJSONObject("boneTimelines") ?: JSONObject()
        
        boneTimelinesJson.keys().forEach { key ->
            val id = key.toInt()
            boneTimelines[id] = BoneTimeline.fromJson(
                boneTimelinesJson.getJSONObject(key)
            )
        }
    }
    
    fun save() {
        val jo = toJson()
        spineAnimation.transaction {
            timelinesJson = jo.toString()
        }
    }
    
    fun toJson(): JSONObject {
        val jo = JSONObject()
        val boneTimelinesJson = JSONObject()
    
        boneTimelines.forEach { (boneId, timeline) ->
            if (timeline.isNotEmpty) {
                boneTimelinesJson.put(boneId.toString(), timeline.toJson())
            }
        }
        jo.put("boneTimelines", boneTimelinesJson)
        return jo
    }
}