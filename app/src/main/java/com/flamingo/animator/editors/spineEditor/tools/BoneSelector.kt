package com.flamingo.animator.editors.spineEditor.tools

import android.graphics.Matrix
import com.flamingo.animator.editors.drawUtils.scale
import com.flamingo.animator.editors.spineEditor.SpineSurfaceView
import com.flamingo.animator.model.spine.Bone
import org.jetbrains.anko.dip
import kotlin.math.abs

class BoneSelector(val surfaceView: SpineSurfaceView) {
    val bonesContainer get() = surfaceView.bonesContainer
    val scaleHandler get() = surfaceView.scaleHandler
    
    val matrix = Matrix()
    
    val touchTolerance = surfaceView.dip(16).toFloat()
    
    // in screen coords
    private fun findTouchPartOrNull(bone: Bone, x: Float, y: Float): TouchPart? {
        val point = floatArrayOf(x, y)
        scaleHandler.inverseDrawMatrix.mapPoints(point)
        val tolerance = touchTolerance * scaleHandler.inverseDrawMatrix.scale
        
        matrix.reset()
        matrix.postRotate(bone.absoluteRotation)
        matrix.postTranslate(bone.absoluteX, bone.absoluteY)
        matrix.invert(matrix) // from field to bone
        matrix.mapPoints(point) // map point to bone coords.
    
        if (abs(point[1]) > tolerance) {
            return null
        }
        if (abs(point[0]) <= tolerance) {
            return TouchPart.Start
        }
        if (abs(point[0] - bone.length) <= tolerance) {
            return TouchPart.End
        }
    
        if (point[0] > -tolerance && point[0] < bone.length + tolerance) {
            return TouchPart.Middle
        }
        return null
    }
    
    // x, y in screen coords
    fun findSelectedBoneWithPart(x: Float, y: Float): Pair<Bone, TouchPart>? {
        // start from children
        bonesContainer.traverseBones()
            .toList().reversed().forEach { bone ->
                val touchPart = findTouchPartOrNull(bone, x, y)
                if (touchPart != null) {
                    return bone to touchPart
                }
            }
        return null
    }
    
    // x, y in screen coords
    fun findSelectedBone(x: Float, y: Float) = findSelectedBoneWithPart(x, y)?.first
    
    enum class TouchPart {
        Start, Middle, End
    }
}