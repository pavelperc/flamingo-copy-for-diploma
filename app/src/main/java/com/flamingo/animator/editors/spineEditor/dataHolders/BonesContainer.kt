package com.flamingo.animator.editors.spineEditor.dataHolders

import com.flamingo.animator.editors.spineEditor.SpineSurfaceView
import com.flamingo.animator.model.spine.Bone
import com.flamingo.animator.utils.commonUtils.safeJson
import com.flamingo.animator.utils.realmUtils.transaction
import java.util.*

class BonesContainer(val surfaceView: SpineSurfaceView) {
    val spineObject get() = surfaceView.activity.spine
    
    var selectedBone: Bone? = null
        set(value) {
            field = value
            surfaceView.onBoneSelectionChanged()
        }
    
    var root: Bone? = null
    
    /** Returns dfs with bones. Parent is always yielded before children. */
    fun traverseBones(): Sequence<Bone> {
        val root = root ?: return emptySequence()
        
        return sequence {
            val stack = Stack<Bone>()
            stack.add(root)
            while (stack.isNotEmpty()) {
                val bone = stack.pop()
                
                yield(bone)
                stack.addAll(bone.children)
            }
        }
    }
    
    /** Visible bones and starts of invisible branches. */
    val visuallyVisibleBones
        get() = traverseBones().filter {
            it.absoluteVisible || it.isInvisibleBranchStart
        }
    
    val visibleBones get() = traverseBones().filter { it.absoluteVisible }
    
    /** New selected is a bone's parent. */
    fun removeSelectedBone() {
        val bone = selectedBone ?: return
        removeBone(bone)
        selectedBone = bone.parent
    }
    
    fun removeBone(bone: Bone) {
        bone.parent?.children?.remove(bone)
        if (bone == root) {
            root = null
        }
        bone.slotContainer?.bone = null
        bone.slotContainer?.updateFromAbsolute(false)
        bone.childrenRecursive.forEach {
            it.slotContainer?.bone = null
            it.slotContainer?.updateFromAbsolute(false)
        }
    }
    
    fun saveBones() {
        spineObject.transaction {
            bonesJson = root?.toJson()?.toString()
        }
    }
    
    fun loadBones() {
        val jo = spineObject.bonesJson.safeJson()
        if (jo.length() == 0) {
            root = null
            return
        }
        root = Bone.fromJson(jo)
    }
    
    fun disableOrEnableSubtree(bone: Bone) {
        bone.visible = !bone.visible
        bone.updateAbsolute(false)
        bone.updateAllChildren(false)
    }
}