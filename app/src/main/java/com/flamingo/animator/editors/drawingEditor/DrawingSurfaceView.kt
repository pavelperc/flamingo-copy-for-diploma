package com.flamingo.animator.editors.drawingEditor

import android.annotation.SuppressLint
import android.graphics.*
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import androidx.annotation.ColorInt
import com.flamingo.animator.R
import com.flamingo.animator.activity.DrawingActivity
import com.flamingo.animator.activity.DrawingSetup
import com.flamingo.animator.editors.drawUtils.CheckerBoard
import com.flamingo.animator.editors.drawUtils.drawBitmap
import com.flamingo.animator.editors.drawUtils.expand
import com.flamingo.animator.editors.drawUtils.isNotStylus
import com.flamingo.animator.editors.drawUtils.pressurePath.PressurePoint
import com.flamingo.animator.editors.drawingEditor.colorpicker.ColorPickerDialog
import com.flamingo.animator.editors.drawingEditor.dataHolders.*
import com.flamingo.animator.editors.drawingEditor.tools.BrushContainer
import com.flamingo.animator.editors.drawingEditor.tools.CompoundToolsContainer
import com.flamingo.animator.model.DrawConfig
import com.flamingo.animator.utils.commonUtils.measureTime
import com.flamingo.animator.utils.realmUtils.transaction
import org.jetbrains.anko.toast
import kotlin.concurrent.thread

enum class BrushType {
    BRUSH, FILL_BRUSH, ERASER, BUCKET_FILL
}

/** Main class for drawing logic. All ui handlers are contained in [DrawingActivity]. */
@SuppressLint("ViewConstructor")
class DrawingSurfaceView(
    val activity: DrawingActivity,
    val drawingSetup: DrawingSetup,
    val drawConfig: DrawConfig
) : SurfaceView(activity), ScaleDetectorCallback {
    companion object {
        /** Padding, when centering image on scale text long click, and activity start. */
        const val RESET_SCALE_PADDING = 15
    }
    
    /** It is used to subscribe on events,
     * like changing the current frame, layer, changing undo/redo,
     * and performing any saved action. */
    var onStateChanged: (() -> Unit) = {}
    
    
    val currentColor: Int
        get() = brushContainer.currentColor
    
    val pictureWidth: Int
    val pictureHeight: Int
    @ColorInt
    val backgroundColor = drawConfig.backgroundColor
    
    private val backgroundSurfaceColor = activity.getColor(R.color.backgroundLightGray)
    
    val colorPicker = ColorPickerDialog { newColor ->
        setColor(newColor)
    }
    
    /** set brush color to button and to paints. And add it to latest. */
    fun setColor(newColor: Int) {
        colorPicker.addLatestColor(newColor)
        brushContainer.setColor(newColor)
        activity.updateColorButton(newColor)
        activity.updateMiniColorButton(colorPicker.latestColors[1])
    }
    
    val drawDataHandler =
        DrawDataHandler(drawingSetup, activity.assetRepo, { scaleHandler }, backgroundColor)
    
    val imageBuilder: ImageBuilder
        get() = drawDataHandler.imageBuilder
    
    val currentRestorableCanvas: RestorableCanvas
        get() = drawDataHandler.currentRestorableCanvas
    
    /** Container for current layered image. */
    val currentImageContainer: ImageContainer
        get() = drawDataHandler.currentImageContainer
    
    
    val brushContainer = BrushContainer(this)
    val compoundToolsContainer = CompoundToolsContainer(this)
    
    init {
        // set latest colors from config
        colorPicker.loadLatestColors(drawConfig.latestColors.toIntArray())
        setColor(colorPicker.latestColors[0])
        
        pictureWidth = drawDataHandler.width
        pictureHeight = drawDataHandler.height
        
        drawDataHandler.animContainer?.setFramePosition(drawConfig.latestFrame)
        brushContainer.importBrushes(drawConfig.brushStateJson)
    }
    
    override fun onScaleUpdate(scale: Float) {
        if (isAnimPlaying) {
            updatePlayingAnim()
        } else if (compoundToolsContainer.isCompoundToolEnabled) {
            compoundToolsContainer.currentDrawer?.updateScreen()
        } else {
            updateScreen()
        }
        activity.updateScaleText(scale)
    }
    
    override fun onScaleStart() {
    }
    
    /** Translation parameters to put the image on the screen center. */
    lateinit var fitScreenScale: FitScreenScale
    
    lateinit var scaleHandler: ScaleHandler
    
    /** Main matrix, to translate draw image to screen. */
    val drawMatrix: Matrix
        get() = scaleHandler.drawMatrix
    
    /** Matrix, to translate screen shapes to draw image. */
    val inverseDrawMatrix: Matrix
        get() = scaleHandler.inverseDrawMatrix
    
    
    fun saveAll(): Int {
        // will be saved to database automatically
        drawConfig.realm.transaction {
            drawConfig.backgroundColor = backgroundColor
            drawConfig.setLatestColors(colorPicker.latestColors)
            
            drawConfig.brushStateJson = brushContainer.exportBrushes()
            drawConfig.latestFrame = drawDataHandler.animContainer?.currentFramePosition ?: 0
        }
        
        val savedCount = drawDataHandler.saveAll()
    
        if (savedCount > 0) {
            if (savedCount == 1) {
                activity.toast("$savedCount image was saved.")
            } else {
                activity.toast("$savedCount images were saved.")
            }
        }
        return savedCount
    }
    
    init {
        holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(holder: SurfaceHolder) {
                fitScreenScale = FitScreenScale
                    .fitCanvas(width, height, pictureWidth, pictureHeight, RESET_SCALE_PADDING)
                scaleHandler =
                    ScaleHandler(this@DrawingSurfaceView, activity, fitScreenScale, width, height)
                
                imageBuilder.updateScreenSize(width, height)
                updateScreen()
            }
            
            override fun surfaceChanged(
                holder: SurfaceHolder?,
                format: Int,
                width: Int,
                height: Int
            ) {
                fitScreenScale.updateFitCanvas(
                    width, height, pictureWidth, pictureHeight, RESET_SCALE_PADDING
                )
                scaleHandler.reset(width, height)
                imageBuilder.updateScreenSize(width, height)
                compoundToolsContainer.currentDrawer?.updateScreen() ?: updateScreen()
            }
            
            override fun surfaceDestroyed(holder: SurfaceHolder?) = Unit
        })
    }
    
    private val shadowPaint = Paint().apply {
        color = Color.GRAY
        strokeWidth = 6f
        style = Paint.Style.STROKE
        strokeJoin = Paint.Join.ROUND
    }
    
    /** Paint to draw transparent background. */
    private val checkeredPaint = CheckerBoard.createCheckeredPaint()
    
    private val shadowRect = RectF()
    
    private val backgroundColorPaint = Paint().apply {
        style = Paint.Style.FILL
        color = backgroundColor
    }
    
    /** Border around the image and background color. */
    private fun drawShadow(canvas: Canvas) {
        shadowRect.set(0f, 0f, pictureWidth.toFloat(), pictureHeight.toFloat())
        drawMatrix.mapRect(shadowRect)
        if (backgroundColor != Color.TRANSPARENT) {
            canvas.drawRect(shadowRect, backgroundColorPaint)
        }
        shadowRect.expand(shadowPaint.strokeWidth / 2)
        canvas.drawRect(shadowRect, shadowPaint)
    }
    
    private fun drawCheckerboard(canvas: Canvas) {
        if (Color.alpha(backgroundColor) == 255) {
            return
        }
        val rect = RectF(0f, 0f, pictureWidth.toFloat(), pictureHeight.toFloat())
        drawMatrix.mapRect(rect)
        canvas.drawRect(rect, checkeredPaint)
    }
    
    /** UpdateScreen for playing anim. Should be called from the ui thread, because of realm. */
    private fun updatePlayingAnim() {
        val screenCanvas = holder.lockCanvas() ?: return // if the activity was closed
        // darker surface!!
        screenCanvas.drawColor(Color.GRAY)
        
        drawShadow(screenCanvas)
        drawCheckerboard(screenCanvas)
        
        val frameBitmap =
            drawDataHandler.animContainer?.imageContainers?.getOrNull(playingAnimFrameIndex)
                ?.builtImage
        
        if (frameBitmap != null) {
            screenCanvas.drawBitmap(frameBitmap, drawMatrix, null)
        }
        holder.unlockCanvasAndPost(screenCanvas)
    }
    
    @Volatile
    var isAnimPlaying = false
    @Volatile
    private var playingAnimFrameIndex = 0
    
    @Synchronized
    fun startAnim() {
        if (isAnimPlaying) {
            return
        }
        isAnimPlaying = true
        playingAnimFrameIndex = 0
        val animContainer = drawDataHandler.animContainer ?: return
        
        val fps = animContainer.animation.fps
        var delay = (1000 / fps).toLong()
        if (delay > 2000) {
            delay = 2000
        }
        // this realm object can be accessed only from the ui thread
        val framesSize = animContainer.animation.frames.size
        thread(isDaemon = true) {
            while (isAnimPlaying) {
                activity.runOnUiThread { updatePlayingAnim() }
                Thread.sleep(delay)
                // unsafe...
                playingAnimFrameIndex = (playingAnimFrameIndex + 1) % framesSize
            }
            activity.runOnUiThread { updateScreen() }
        }
    }
    
    fun stopAnim() {
        isAnimPlaying = false
    }
    
    /** Draws all layers, and background images, and any custom data with [additionalDraw].
     * [drawDirtyChanges] is used to draw temporal drawings from dirtyChangesBitmap.
     * Don't use it if you are scaling or changing layers.
     * Use [updateScreenBackWithDirtyChanges] if you finished drawing a line
     * and don't want to update the whole screenBack from saved layers.
     */
    fun updateScreen(
        drawDirtyChanges: Boolean = false,
        updateScreenBackWithDirtyChanges: Boolean = false,
        additionalDraw: (screenCanvas: Canvas) -> Unit = {}
    ) {
        measureTime("---Update screen in common") {
            
            // if we draw incrementally to canvas directly without updating,
            // we will have a flickering effect.
            // because surfaceView is double buffered, it uses two canvas: one is showing, second is drawing.
            
            val screenCanvas =
                holder.lockCanvas() ?: return@measureTime // if the activity was closed 
    
            val screenBack = imageBuilder.screenBackCanvas
    
    
            if (updateScreenBackWithDirtyChanges) {
                measureTime("Draw dirty changes on screenBack") {
                    screenBack.drawBitmap(imageBuilder.dirtyChangesBitmap, drawMatrix)
                }
            } else if (!drawDirtyChanges) {
                // update screenBack:
                measureTime("Draw shadow and checkers") {
                    screenBack.drawColor(backgroundSurfaceColor)
                    drawShadow(screenBack)
                    drawCheckerboard(screenBack)
                }
    
                measureTime("Build screenBack until current layer") {
                    imageBuilder.drawUntilCurrentLayer(screenBack)
                }
            }
            
            measureTime("Draw screenBack") {
                screenCanvas.drawBitmap(imageBuilder.screenBackBitmap)
            }
            if (drawDirtyChanges && !updateScreenBackWithDirtyChanges) {
                measureTime("Draw dirtyChanges on screen") {
                    screenCanvas.drawBitmap(imageBuilder.dirtyChangesBitmap, drawMatrix)
                }
            }
            measureTime("Draw front layers") {
                imageBuilder.drawAfterCurrentLayer(screenCanvas)
            }
            
            additionalDraw(screenCanvas)
            holder.unlockCanvasAndPost(screenCanvas)
        }
    }
    
    /** Draws a bitmap from [customBuild] with drawMatrix. */
    fun updateScreenWithCustomBuild(
        customBuild: ((ImageBuilder) -> Bitmap)? = null,
        additionalDraw: (screenCanvas: Canvas) -> Unit
    ) {
        measureTime("Update screen in common (custom build)") {
            val screenCanvas =
                holder.lockCanvas() ?: return@measureTime // if the activity was closed 
    
            measureTime("Draw shadow and checkers on screenCanvas") {
                screenCanvas.drawColor(backgroundSurfaceColor)
                drawShadow(screenCanvas)
                drawCheckerboard(screenCanvas)
            }
            
            if (customBuild != null) {
                val customBitmap = measureTime("Custom build") {
                    customBuild.invoke(imageBuilder)
                }
                measureTime("Draw custom build with draw matrix") {
                    screenCanvas.drawBitmap(customBitmap, drawMatrix)
                }
            }
            measureTime("Additional draw") {
                additionalDraw(screenCanvas)
            }
            holder.unlockCanvasAndPost(screenCanvas)
        }
    }
    
    
    fun resetScale() {
        scaleHandler.reset(width, height)
    }
    
    /** Returns a [PointF] with scaled point coords. */
    fun getScaledCoord(x: Float, y: Float): PointF {
        val pointArray = floatArrayOf(x, y)
        inverseDrawMatrix.mapPoints(pointArray)
        return PointF(pointArray[0], pointArray[1])
    }
    
    /** Returns a [PressurePoint] with scaled point coords. */
    fun getScaledCoordPressured(x: Float, y: Float, pressure: Float): PressurePoint {
        val pointArray = floatArrayOf(x, y)
        inverseDrawMatrix.mapPoints(pointArray)
        return PressurePoint(pointArray[0], pointArray[1], pressure)
    }
    
    
    override fun onTouchEvent(event: MotionEvent): Boolean {
        activity.onSurfaceTouched()
        
        // if not stylus and not scaling return
        if (event.isNotStylus && scaleHandler.onTouchEvent(event)) {
            return true
        }
        if (isAnimPlaying) {
            return true
        }
        
        return compoundToolsContainer.currentDrawer?.onTouchEvent(event)
            ?: brushContainer.handleEvent(event)
    }
    
    
    fun setBrush(brushType: BrushType) {
        brushContainer.setDrawer(brushType)
    }
    
    fun undo() {
        currentRestorableCanvas.undo()
        updateState()
    }
    
    fun redo() {
        currentRestorableCanvas.redo()
        updateState()
    }
    
    
    fun clearCanvas() {
        val lastAction = currentRestorableCanvas.lastAction
        // do not repeat equal actions.
        // layer background is always transparent
        if (lastAction is ColorAction && lastAction.color == Color.TRANSPARENT) {
            return
        }
        currentRestorableCanvas.performAction(ColorAction(Color.TRANSPARENT))
        updateState()
    }
    
    /** Updates the draw screen and the buttons.
     * Use [updateBuiltImage] if you performed some action to some layer.
     * For example you don't need [updateBuiltImage], if you only changed the background. */
    fun updateState(
        updateScreenBackWithDirtyChanges: Boolean = false,
        updateBuiltImage: Boolean = true
    ) {
        if (updateBuiltImage) {
            currentImageContainer.invalidateBuiltImageCache()
        }
        updateScreen(updateScreenBackWithDirtyChanges = updateScreenBackWithDirtyChanges)
        onStateChanged()
    }

// --- Anim frames:
    
    fun addFrame(duplicate: Boolean) {
        if (isAnimPlaying) {
            return
        }
        drawDataHandler.animContainer?.addFrame(duplicate)
        drawDataHandler.invalidateBackground()
        updateState(updateBuiltImage = false) // don't change the built image
    }
    
    fun nextFrame() {
        drawDataHandler.animContainer?.nextFrame()
        drawDataHandler.invalidateBackground()
        updateState(updateBuiltImage = false) // don't change the built image
    }
    
    fun prevFrame() {
        drawDataHandler.animContainer?.prevFrame()
        drawDataHandler.invalidateBackground()
        updateState(updateBuiltImage = false) // don't change the built image
    }
    
    /** Only for animation! */
    fun deleteCurrFrame() {
        if (isAnimPlaying) {
            return
        }
        val animContainer = drawDataHandler.animContainer
        if (animContainer != null) {
            animContainer.deleteCurrFrame()
            drawDataHandler.invalidateBackground()
            updateState(updateBuiltImage = false) // don't change the built image
        }
    }
    
    fun enterImageShiftMode() {
        compoundToolsContainer.setImageShifter()
        compoundToolsContainer.currentDrawer?.updateScreen()
    }
}


