package com.flamingo.animator.export.flampack

import com.flamingo.animator.dialog.showExportFileDialog
import com.flamingo.animator.model.Background
import com.flamingo.animator.model.Object
import com.flamingo.animator.model.spine.Spine
import com.flamingo.animator.repository.AssetRepo
import com.flamingo.animator.utils.SomeUsefulActivity
import com.flamingo.animator.utils.commonUtils.logError
import org.jetbrains.anko.*

fun SomeUsefulActivity.showExportFlampackDialog(
    assetRepo: AssetRepo,
    suggestedName: String,
    backgrounds: List<Background>,
    objects: List<Object>,
    spines: List<Spine>,
    onSuccess: () -> Unit
) {
    val zipFile = assetRepo.repo.cacheDir.resolve("temp.zip")
    
    FlampackExport.exportResourcePack(
        assetRepo,
        objects,
        backgrounds,
        spines,
        zipFile
    )
    
    showExportFileDialog(
        zipFile,
        suggestedName,
        "flampack.zip",
        "Export resource pack",
        "The resource pack will be exported with .flampack.zip extension.",
        "application/zip",
        onSuccess
    )
}

fun SomeUsefulActivity.importFlampackDialog(
    assetRepo: AssetRepo,
    onlySpines: Boolean = false,
    onSuccess: () -> Unit
) {
    alert {
        title = "Import resource pack?"
        message = "Select a flamingo resource pack in .flampack.zip extension."
        cancelButton {}
        yesButton {
            importFlampack(assetRepo, onlySpines, onSuccess)
        }
    }.show()
}

private fun SomeUsefulActivity.importFlampack(
    assetRepo: AssetRepo,
    onlySpines: Boolean = false,
    onSuccess: () -> Unit
) {
    val intent = android.content.Intent(android.content.Intent.ACTION_OPEN_DOCUMENT)
    intent.setType("application/zip")
    activityHelper.startForResult(
        android.content.Intent.createChooser(intent, "Select file")
    ) { resultCode, data ->
        val uri = data?.data
        val inputStream = uri?.let { contentResolver.openInputStream(it) }
        if (inputStream != null) {
            try {
                FlampackExport.importResourcePack(inputStream, assetRepo, onlySpines)
                toast("Done!")
                onSuccess()
            } catch (e: Exception) {
                logError("flampack import exception", e)
                longToast("Import error:\n" + e.message)
            }
        } else {
            toast("Import canceled")
        }
    }
}