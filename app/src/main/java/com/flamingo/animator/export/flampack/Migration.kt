package com.flamingo.animator.export.flampack

import com.flamingo.animator.utils.commonUtils.toJsonArray
import com.flamingo.animator.utils.commonUtils.toList
import org.json.JSONObject

// flampack migrations
object Migration {
    const val CURRENT_VERSION = 4
    
    fun migrate(jo: JSONObject) {
        var version = jo.getInt("version")
        
        if (version == 1) {
            migrate1_2(jo)
            version++
        }
        if (version == 2) {
            migrate2_3(jo)
            version++
        }
        if (version == 3) {
            migrate3_4(jo)
            version++
        }
        
    }
    
    private fun JSONObject.migrateJSONArray(name: String, func: (JSONObject) -> Unit) {
        val children = getJSONArray(name).toList()
        children.forEach(func)
        put(name, children.toJsonArray())
    }
    
    private fun migrate1_2(root: JSONObject) {
        if (!root.has("spines")) {
            root.put("spines", JSONObject())
            return
        }
        root.migrateJSONArray("spines") { spine ->
            spine.migrateJSONArray("slots") { slot ->
                val x = slot.getInt("x")
                val y = slot.getInt("y")
                val rotation = slot.getInt("rotation")
                
                slot.put("jsonData", JSONObject().apply {
                    put("x", x.toFloat())
                    put("y", y.toFloat())
                    put("rotation", rotation.toFloat())
                })
            }
        }
    }
    
    private fun migrate2_3(root: JSONObject) {
        // create one constant 'lastEdited' timestamp
        // add layers ids from 1 to n
        // make map: layer names to their ids
        // iterate slots with names, find their attachments,
        // add layer ids to them by slot names
        
        val lastEditedConstant = System.currentTimeMillis()
        var idCounter = 0L
        root.migrateJSONArray("spines") { spine ->
            val layerNamesToIds = mutableMapOf<String, Long>()
            spine.getJSONObject("uncroppedImage").migrateJSONArray("layers") { layer ->
                layer.put("id", ++idCounter)
                layer.put("lastEdited", lastEditedConstant)
                layerNamesToIds[layer.getString("name")] = idCounter
            }
            spine.migrateJSONArray("slots") { slot ->
                val slotName = slot.getString("name")
                slot.migrateJSONArray("attachments") { attachment ->
                    if (slotName in layerNamesToIds) {
                        attachment.put("boundLayerId", layerNamesToIds.getValue(slotName))
                        attachment.put("boundLayerLastEdited", lastEditedConstant)
                    }
                }
            }
        }
    }
    
    private fun migrate3_4(root: JSONObject) {
        // move slot coordinates to attachments
        // leave boneId in slot
        
        root.migrateJSONArray("spines") { spine ->
            spine.migrateJSONArray("slots") { slot ->
                val jsonData = slot.getJSONObject("jsonData")
                val boneId = jsonData.getInt("boneId")
                jsonData.remove("boneId")
                slot.put("boneId", boneId)
                slot.migrateJSONArray("attachments") { attachment ->
                    attachment.put("jsonData", jsonData)
                }
            }
        }
    }
}