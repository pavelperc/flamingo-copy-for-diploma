package com.flamingo.animator.export.flampack

import com.flamingo.animator.export.flampack.FlampackExport.exportJson
import com.flamingo.animator.model.spine.Attachment
import com.flamingo.animator.model.spine.Slot
import com.flamingo.animator.model.spine.Spine
import com.flamingo.animator.model.spine.SpineAnimation
import com.flamingo.animator.utils.commonUtils.*
import org.json.JSONObject

object SpineExport {
    private fun Attachment.exportJson(fileNames: MutableList<String>): JSONObject {
        val jo = JSONObject()
        jo.put("height", height)
        jo.put("width", width)
        jo.put("name", name)
        jo.put("jsonData", jsonData.safeJson())
        jo.put("boundLayerId", this.boundLayer?.id ?: 0L)
        jo.put("boundLayerLastEdited", this.boundLayerLastEdited)
        // with extension
        jo.put("imageName", imageReq.exportJson(fileNames))
        return jo
    }
    
    /** Should be launched inside transaction! */
    private fun importAttachment(
        ownerKey: String,
        jo: JSONObject,
        importParams: ImportParams
    ): Attachment {
        val attachment = Attachment.init(importParams.realm, ownerKey)
        attachment.name = jo.getString("name")
        attachment.height = jo.getInt("height")
        attachment.width = jo.getInt("width")
        
        attachment.boundLayerLastEdited = jo.optLong("boundLayerLastEdited")
        val layerId = jo.optLong("boundLayerId", -1)
        attachment.boundLayer = importParams.layerByOldId[layerId]
        attachment.jsonData = jo.getJSONObject("jsonData").toString()
        
        FlampackExport.importImageFile(
            attachment.imageReq, jo.getString("imageName"), importParams
        )
        return attachment
    }
    
    private fun Slot.exportJson(fileNames: MutableList<String>): JSONObject {
        val jo = JSONObject()
        jo.put("name", name)
        
        val attachmentsJson = attachments.map { it.exportJson(fileNames) }.toJsonArray()
        jo.put("attachments", attachmentsJson)
        jo.put("boneId", boneId)
        jo.put("selectedAttachPos", selectedAttachmentPosition)
        return jo
    }
    
    /** Should be launched inside transaction! */
    private fun importSlot(
        ownerKey: String,
        jo: JSONObject,
        importParams: ImportParams
    ): Slot {
        val slot = Slot.init(importParams.realm, ownerKey)
        slot.name = jo.getString("name")
        slot.boneId = jo.getInt("boneId")
        slot.selectedAttachmentPosition = jo.optInt("selectedAttachPos")
        val attachments = jo.getJSONArray("attachments").toList()
            .map { importAttachment(slot.key(), it, importParams) }
        slot.attachments.addAll(attachments)
        return slot
    }
    
    private fun SpineAnimation.exportJson(): JSONObject {
        val jo = JSONObject()
        jo.put("name", name)
        jo.put("timelines", timelinesJson.safeJson())
        return jo
    }
    
    /** Should be launched inside transaction! */
    private fun importSpineAnimation(
        jo: JSONObject,
        importParams: ImportParams
    ): SpineAnimation {
        val anim = SpineAnimation.init(importParams.realm)
        anim.name = jo.getString("name")
        anim.timelinesJson = jo.getJSONObject("timelines").toString()
        return anim
    }
    
    private fun Spine.exportJson(fileNames: MutableList<String>): JSONObject {
        val jo = JSONObject()
        jo.put("name", name)
        jo.put("riggingDrawConfigJson", riggingDrawConfigJson.safeJson())
        jo.put("bonesJson", bonesJson.safeJson())
        
        val slotsJson = slots.map { it.exportJson(fileNames) }.toJsonArray()
        jo.put("slots", slotsJson)
        val animationsJson = animations.map { it.exportJson() }.toJsonArray()
        jo.put("animations", animationsJson)
        
        jo.put("ignoredLayerIds", ignoredUncroppedLayersForImport.map { it.id }.toLongJsonArray())
        
        jo.put("uncroppedImageDrawConfig", uncroppedImageDrawConfigReq.exportJson())
        jo.put("uncroppedImage", uncroppedImageReq.exportJson(fileNames))
        jo.put("preview", previewReq.exportJson(fileNames))
        return jo
    }
    
    /** Should be launched inside transaction! */
    fun importSpine(
        jo: JSONObject,
        importParams: ImportParams,
        existingSpineNames: MutableSet<String>
    ) {
        val oldName = jo.getString("name")
        if (importParams.skipExisting && oldName in existingSpineNames) {
            return
        }
        
        val name = makeNameUnique(existingSpineNames, oldName)
        existingSpineNames.add(name)
        
        val spine = Spine.init(importParams.realm, name)
        spine.riggingDrawConfigJson = jo.getJSONObject("riggingDrawConfigJson").toString()
        spine.bonesJson = jo.getJSONObject("bonesJson").toString()
        
        // layer ids are used in attachments, so they are imported earlier
        FlampackExport.importLayeredImage(
            spine.uncroppedImageReq,
            jo.getJSONObject("uncroppedImage"), importParams
        )
        
        FlampackExport.importDrawConfig(
            spine.uncroppedImageDrawConfigReq,
            jo.getJSONObject("uncroppedImageDrawConfig")
        )
        
        val ignoredLayers = jo.optJSONArray("ignoredLayerIds")?.toLongList()
            ?.map { importParams.layerByOldId.getValue(it) }
            ?: emptyList()
        
        spine.ignoredUncroppedLayersForImport.addAll(ignoredLayers)
        
        val slots = jo.getJSONArray("slots").toList()
            .map { importSlot(spine.key(), it, importParams) }
        spine.slots.addAll(slots)
        
        val animations = jo.optJSONArray("animations")?.toList()
            ?.map { importSpineAnimation(it, importParams) }
            ?: emptyList()
        spine.animations.addAll(animations)
        
        if (jo.has("preview")) {
            FlampackExport.importImageFile(
                spine.previewReq, jo.getString("preview"), importParams
            )
        }
    }
    
    fun exportSpine(spine: Spine, fileNames: MutableList<String>) =
        spine.exportJson(fileNames)
}