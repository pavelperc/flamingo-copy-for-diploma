package com.flamingo.animator.export.flampack

import com.flamingo.animator.model.*
import com.flamingo.animator.model.spine.Spine
import com.flamingo.animator.repository.AssetRepo
import com.flamingo.animator.utils.commonUtils.*
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream

/**
 * Methods to export/import full jsons of objects and backgrounds, and make/load a resource pack zip file.
 *
 * Original image names will not be saved. They will be generated from the new ids.
 *
 * No documentation, minimum migration, default values in case of errors (not always).
 * All import/export methods are near to each other.
 *
 * Layered images are saved together with their built images to simplify the preview.
 *
 * There is a mutable string list, passed to all json export methods. It stores all file names, that should be copied.
 */
object FlampackExport {
    fun ImageFile.exportJson(fileNames: MutableList<String>): String {
        // with extension
        fileNames += fileName
        return fileName
    }
    
    /** Should be launched inside transaction! */
    fun importImageFile(
        initImageFile: ImageFile,
        jsonName: String,
        importParams: ImportParams
    ) {
        val dstFile = initImageFile.getFile(importParams.assetsDir)
        val srcFile = importParams.srcDir.resolve(jsonName)
        if (srcFile.exists()) {
            srcFile.copyTo(dstFile, overwrite = true)
        }
    }
    
    /** [fileNames] is a list, where all exported filenames should be put. */
    fun Layer.exportJson(fileNames: MutableList<String>): JSONObject {
        val jo = JSONObject()
        jo.put("isVisible", isVisible)
        jo.put("name", name)
        jo.put("lastEdited", lastEdited)
        jo.put("id", id)
        // with extension
        jo.put("imageName", imageReq.exportJson(fileNames))
        return jo
    }
    
    /** Should be launched inside transaction! */
    fun importLayer(
        ownerKey: String,
        jo: JSONObject,
        importParams: ImportParams
    ): Layer {
        val layer = Layer.init(importParams.realm, ownerKey)
        layer.name = jo.getString("name")
        layer.isVisible = jo.getBoolean("isVisible")
        
        // they may not exist for backgrounds and frame-by-frame animations
        if (jo.has("id")) {
            importParams.layerByOldId[jo.getLong("id")] = layer
        }
        if (jo.has("lastEdited")) {
            layer.lastEdited = jo.getLong("lastEdited")
        }
        importImageFile(layer.imageReq, jo.getString("imageName"), importParams)
        return layer
    }
    
    fun LayeredImage.exportJson(fileNames: MutableList<String>): JSONObject {
        val jo = JSONObject()
        // with extension
        jo.put("builtImageName", builtImageReq.exportJson(fileNames))
        jo.put("width", width)
        jo.put("height", height)
        
        val layersJson = layers.map { it.exportJson(fileNames) }.toJsonArray()
        jo.put("layers", layersJson)
        
        return jo
    }
    
    fun importLayeredImage(
        initLayeredImage: LayeredImage,
        jo: JSONObject,
        importParams: ImportParams
    ) {
        initLayeredImage.width = jo.getInt("width")
        initLayeredImage.height = jo.getInt("height")
        
        val layers = jo.getJSONArray("layers").toList()
            .map { importLayer(initLayeredImage.key(), it, importParams) }
        initLayeredImage.layers.addAll(layers)
        
        importImageFile(
            initLayeredImage.builtImageReq, jo.getString("builtImageName"), importParams
        )
    }
    
    fun DrawConfig.exportJson(): JSONObject {
        val jo = JSONObject()
        jo.put("latestColors", latestColors.toIntJsonArray())
        jo.put("latestFrame", latestFrame)
        jo.put("backgroundColor", backgroundColor)
        // skip brushState
//        val brushStateParsed = JSONObject(if (brushStateJson.isEmpty()) "{}" else brushStateJson)
//        jo.put("brushState", brushStateParsed)
        return jo
    }
    
    fun importDrawConfig(initDrawConfig: DrawConfig, jo: JSONObject) {
        val latestColors = jo.getJSONArray("latestColors").toIntList().toIntArray()
        
        initDrawConfig.setLatestColors(latestColors)
        initDrawConfig.latestFrame = jo.getInt("latestFrame")
        initDrawConfig.backgroundColor = jo.getInt("backgroundColor")
    }
    
    fun Background.exportJson(fileNames: MutableList<String>): JSONObject {
        val jo = JSONObject()
        jo.put("name", name)
        jo.put("drawConfig", drawConfigReq.exportJson())
        jo.put("image", imageReq.exportJson(fileNames))
        
        return jo
    }
    
    fun importBackground(
        jo: JSONObject,
        importParams: ImportParams,
        existingBackgroundNames: MutableSet<String>
    ) {
        val oldName = jo.getString("name")
        if (importParams.skipExisting && oldName in existingBackgroundNames) {
            return
        }
        val background = Background.init(importParams.realm)
        
        val name = makeNameUnique(existingBackgroundNames, oldName)
        existingBackgroundNames.add(name)
        background.name = name
        
        importDrawConfig(background.drawConfigReq, jo.getJSONObject("drawConfig"))
        importLayeredImage(background.imageReq, jo.getJSONObject("image"), importParams)
    }
    
    fun Frame.exportJson(fileNames: MutableList<String>): JSONObject {
        val jo = JSONObject()
        jo.put("image", imageReq.exportJson(fileNames))
        return jo
    }
    
    fun importFrame(
        ownerKey: String,
        jo: JSONObject,
        importParams: ImportParams
    ): Frame {
        val frame = Frame.init(importParams.realm, ownerKey)
        importLayeredImage(frame.imageReq, jo.getJSONObject("image"), importParams)
        return frame
    }
    
    fun Animation.exportJson(fileNames: MutableList<String>): JSONObject {
        val jo = JSONObject()
        jo.put("name", name)
        jo.put("drawConfig", drawConfigReq.exportJson())
        jo.put("fps", fps)
        jo.put("shiftX", shiftX)
        jo.put("shiftY", shiftY)
        jo.put("isLoop", isLoop)
        jo.put("frames", frames.map { it.exportJson(fileNames) }.toJsonArray())
        
        return jo
    }
    
    fun importAnimation(
        ownerKey: String,
        jo: JSONObject,
        importParams: ImportParams
    ): Animation {
        val animation = Animation.init(importParams.realm, ownerKey)
        animation.name = jo.getString("name")
        importDrawConfig(animation.drawConfigReq, jo.getJSONObject("drawConfig"))
        animation.fps = jo.getDouble("fps")
        animation.shiftX = jo.getInt("shiftX")
        animation.shiftY = jo.getInt("shiftY")
        animation.isLoop = jo.getBoolean("isLoop")
        val frames = jo.getJSONArray("frames").toList()
            .map { importFrame(animation.key(), it, importParams) }
        animation.frames.addAll(frames)
        
        return animation
    }
    
    fun Object.exportJson(fileNames: MutableList<String>): JSONObject {
        val jo = JSONObject()
        jo.put("objectType", objectType.name)
        jo.put("name", name)
        
        // skip states
        val animArr = animations.map { it.exportJson(fileNames) }.toJsonArray()
        jo.put("animations", animArr)
        
        return jo
    }
    
    fun importObject(
        jo: JSONObject,
        importParams: ImportParams,
        existingObjectNames: MutableSet<String>
    ) {
        val oldName = jo.getString("name")
        if (importParams.skipExisting && oldName in existingObjectNames) {
            return
        }
        val obj = Object.init(importParams.realm)
        obj.objectType = ObjectType.valueOf(jo.getString("objectType"))
        
        val name = makeNameUnique(existingObjectNames, oldName)
        existingObjectNames.add(name)
        obj.name = name
        
        val anims = jo.getJSONArray("animations").toList()
            .map { importAnimation(obj.key(), it, importParams) }
        
        obj.animations.addAll(anims)
    }
    
    
    fun exportResourcePackJson(
        fileNames: MutableList<String>,
        objects: List<Object>,
        backgrounds: List<Background>,
        spines: List<Spine>
    ): JSONObject {
        val jo = JSONObject()
        jo.put("version", Migration.CURRENT_VERSION)
        // order by ids to have the similar ids order in export
        val objJsons = objects.sortedBy { it.id }.map { it.exportJson(fileNames) }
        val bgJsons = backgrounds.sortedBy { it.id }.map { it.exportJson(fileNames) }
        val spineJsons = spines.sortedBy { it.id }.map { SpineExport.exportSpine(it, fileNames) }
        
        jo.put("objects", objJsons.toJsonArray())
        jo.put("backgrounds", bgJsons.toJsonArray())
        jo.put("spines", spineJsons.toJsonArray())
        return jo
    }
    
    fun importResourcePackJson(
        jo: JSONObject,
        importParams: ImportParams
    ) {
        Migration.migrate(jo)
        val assetRepo = importParams.assetRepo
        assetRepo.realm.executeTransaction {
            val existingObjectNames = assetRepo.getAllObjects().map { it.name }.toMutableSet()
            val existingBackgroundNames =
                assetRepo.getAllBackgrounds().map { it.name }.toMutableSet()
            val existingSpineNames = assetRepo.getAllSpines().map { it.name }.toMutableSet()
            
            if (!importParams.onlySpines) {
                jo.getJSONArray("objects").toList()
                    .forEach { importObject(it, importParams, existingObjectNames) }
                jo.getJSONArray("backgrounds").toList()
                    .forEach { importBackground(it, importParams, existingBackgroundNames) }
            }
            jo.optJSONArray("spines")?.toList()
                ?.forEach { SpineExport.importSpine(it, importParams, existingSpineNames) }
        }
    }
    
    /** Recommend for [zipFile] to have .flampack extension. */
    fun exportResourcePack(
        assetRepo: AssetRepo,
        objects: List<Object>,
        backgrounds: List<Background>,
        spines: List<Spine>,
        zipFile: File,
        progressCallback: (step: Int, max: Int) -> Unit = { _, _ -> }
    ) {
        val fileNames = mutableListOf<String>()
        val jo = exportResourcePackJson(fileNames, objects, backgrounds, spines)
        
        // some backgrounds and spines may contain fileName without real image
        val files = fileNames
            .map { fileName -> assetRepo.assetsDir.resolve(fileName) }
            .filter { file -> file.exists() }
        
        // run with auto closing
        ZipOutputStream(FileOutputStream(zipFile, false)).use { zipOut ->
            progressCallback(0, files.size)
            
            zipOut.putNextEntry(ZipEntry("resourcePack.json"))
            zipOut.write(jo.toString().toByteArray())
            
            for ((i, file) in files.withIndex()) {
                
                zipOut.putNextEntry(ZipEntry(file.name))
                file.forEachBlock { buffer, bytesRead ->
                    zipOut.write(buffer, 0, bytesRead)
                }
                progressCallback(i, files.size)
            }
        }
    }
    
    /** Expect zipFile with .flampack extension. */
    fun importResourcePack(
        inputStream: InputStream, assetRepo: AssetRepo,
        onlySpines: Boolean = false,
        skipExisting: Boolean = false
    ) {
        val zipDir = assetRepo.repo.cacheDir.resolve("flampack_unzip")
        val clearedOld = zipDir.deleteRecursively()
        log("deleted old import dir: $clearedOld")
        zipDir.mkdirs()
        // unzip to zipDir
        ZipInputStream(inputStream).use { zipIn ->
            var zipEntry: ZipEntry? = zipIn.nextEntry
            while (zipEntry != null) {
                if (zipEntry.isDirectory) {
                    zipIn.closeEntry()
                    zipEntry = zipIn.nextEntry
                    continue
                }
                val file = zipDir.resolve(zipEntry.name)
                // no buffering!!!
                file.writeBytes(zipIn.readBytes())
                
                zipEntry = zipIn.nextEntry
            }
        }
        // get json and export
        val jo = JSONObject(zipDir.resolve("resourcePack.json").readText())
        val importParams = ImportParams(zipDir, assetRepo, onlySpines, skipExisting)
        importResourcePackJson(jo, importParams)
    }
}


class ImportParams(
    val srcDir: File,
    val assetRepo: AssetRepo,
    val onlySpines: Boolean,
    val skipExisting: Boolean
) {
    val assetsDir get() = assetRepo.assetsDir
    val realm get() = assetRepo.realm
    
    // When objects are imported, new ids are assigned, but some branches of json may refer to old ids.
    // so, these maps convert old ids to layers with new ids.
    val layerByOldId = mutableMapOf<Long, Layer>()
}