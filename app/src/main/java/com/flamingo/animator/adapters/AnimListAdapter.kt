package com.flamingo.animator.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SortedList
import androidx.recyclerview.widget.SortedListAdapterCallback
import com.flamingo.animator.R
import com.flamingo.animator.activity.DrawingActivity
import com.flamingo.animator.activity.DrawingSetup
import com.flamingo.animator.activity.ObjectActivity
import com.flamingo.animator.activity.spine.SpineInfoActivity
import com.flamingo.animator.editors.drawUtils.CheckerBoard
import com.flamingo.animator.editors.drawUtils.GifSaver
import com.flamingo.animator.editors.drawUtils.SpriteSaver
import com.flamingo.animator.model.Animation
import com.flamingo.animator.repository.AssetRepo
import com.flamingo.animator.utils.commonUtils.setFilePreview
import com.flamingo.animator.utils.commonUtils.toList
import com.flamingo.animator.utils.dialogUtils.enterNameAlert
import com.flamingo.animator.utils.realmUtils.transaction
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class AnimSortedList(adapter: RecyclerView.Adapter<*>) :
    SortedList<Animation>(
        Animation::class.java,
        object : SortedListAdapterCallback<Animation>(adapter) {
            override fun areItemsTheSame(a1: Animation, a2: Animation) = a1.id == a2.id
            override fun compare(a1: Animation, a2: Animation) = a1.name.compareTo(a2.name)
            override fun areContentsTheSame(a1: Animation, a2: Animation) = a1.name == a2.name
        })

/** [assetType] represents folder for assets. Like [AssetRepo.BACKGROUND_DIR] */
class AnimListAdapter(
    private val activity: ObjectActivity
) :
    RecyclerView.Adapter<AnimListAdapter.MyViewHolder>() {
    
    val assetRepo get() = activity.assetRepo
    val spineRepo get() = activity.spineRepo
    val anims = AnimSortedList(this)
    
    inner class MyViewHolder(val root: View) : RecyclerView.ViewHolder(root) {
        val tvName = root.findViewById<TextView>(R.id.tvName)!!
        val ivImage = root.findViewById<ImageView>(R.id.ivImage)!!
        val btnOptions = root.findViewById<Button>(R.id.btnOptions)
        
        val menu = PopupMenu(activity, btnOptions)
        val menuRemove = menu.menu.add("Remove")
        val menuRename = menu.menu.add("Rename")
        val menuExportSprite = menu.menu.add("Export sprite")
        val menuExportGif = menu.menu.add("Export gif")
        val menuSpineFromLayers = menu.menu.add("Spine from layers")
        
        init {
            btnOptions.setOnClickListener {
                menu.show()
            }
        }
    }
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val root = LayoutInflater.from(parent.context).inflate(R.layout.state_item, parent, false)
        return MyViewHolder(root)
    }
    
    private val checkeredDrawable = CheckerBoard.createCheckeredDrawable()
    
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val tvName = holder.tvName
        val ivImage = holder.ivImage
        val anim = anims[position]
        tvName.text = anim.name
        
        
        val imageFile = anim.frames.getOrNull(0)?.image?.builtImage?.getFile(assetRepo.assetsDir)
        if (imageFile != null) {
            ivImage.setFilePreview(imageFile)
        }
        
        holder.menu.setOnMenuItemClickListener { item ->
            when (item) {
                holder.menuRename -> {
                    activity.enterNameAlert("Enter anim name:", anim.name) { newName ->
                        val names = anims.toList().map { it.name }
                        if (newName in names) {
                            activity.toast("Name $newName is already used.")
                            false
                        } else {
                            anim.transaction { name = newName }
                            tvName.text = newName
                            true
                        }
                    }
                }
                holder.menuRemove -> {
                    anims.remove(anim)
                    assetRepo.removeAnim(anim)
                }
                holder.menuExportSprite -> {
                    if (!anim.spriteSaved) {
                        SpriteSaver.saveSprite(anim, assetRepo)
                    }
                    val spriteFile = anim.spriteImageReq.getFile(assetRepo.assetsDir)
                    if (!spriteFile.exists()) {
                        return@setOnMenuItemClickListener true
                    }
                    
                    val contentUri =
                        FileProvider.getUriForFile(
                            activity,
                            "com.flamingo.animator.fileprovider",
                            spriteFile
                        )
                    
                    val share = Intent(Intent.ACTION_SEND)
                    share.type = "image/png"
                    share.putExtra(Intent.EXTRA_STREAM, contentUri)
                    activity.startActivity(Intent.createChooser(share, "Export Sprite"))
                }
                holder.menuExportGif -> {
                    val gifFile = activity.projectRepo.repo.cacheDir.resolve("${anim.name}.gif")
                    
                    GifSaver.saveGif(anim, gifFile, assetRepo)
                    
                    val contentUri =
                        FileProvider.getUriForFile(
                            activity,
                            "com.flamingo.animator.fileprovider",
                            gifFile
                        )
                    val share = Intent(Intent.ACTION_SEND)
                    share.type = "image/gif"
                    share.putExtra(Intent.EXTRA_STREAM, contentUri)
                    activity.startActivity(Intent.createChooser(share, "Export Sprite"))
                }
                holder.menuSpineFromLayers -> {
                    val spineObject = spineRepo.createSpineFromAnim(anim)
                    
                    activity.putResultMark("updateSpines")
                    activity.startActivity<SpineInfoActivity>(
                        "projectName" to activity.projectName,
                        "assetId" to spineObject.id
                    )
                }
            }
            true
        }
        
        ivImage.setOnClickListener {
            if (imageFile != null) {
                activity.activityHelper.startForResult<DrawingActivity>(
                    "drawingSetup" to DrawingSetup(
                        0,
                        activity.projectName,
                        anim.drawConfigReq.id,
                        anim.name,
                        anim.id
                    )
                ) { _, intent ->
                    ivImage.setFilePreview(imageFile)
                    activity.passResultMark("updateSpines", intent)
                }
            }
        }
    }
    
    override fun getItemCount() = anims.size()
}