package com.flamingo.animator.adapters.spine

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.flamingo.animator.R
import com.flamingo.animator.activity.spine.SpineEditorActivity
import com.flamingo.animator.utils.commonUtils.*
import com.flamingo.animator.utils.dialogUtils.enterNameAlert
import com.flamingo.animator.utils.dialogUtils.fastAlert
import kotlinx.android.synthetic.main.activity_spine_editor.*
import org.jetbrains.anko.toast

class AnimationListSpinnerAdapter(
    val activity: SpineEditorActivity
) : BaseAdapter(), SpinnerAdapter {
    val animationDataHolder get() = activity.surfaceView.animationDataHolder
    val animationContainers get() = animationDataHolder.animationContainers
    
    private var lastSelectedPosition = 0
    
    private val spnAnimations get() = activity.spnAnimations
    
    val onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {}
        
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            if (position == animationContainers.size) {
                val suggestedName = uniqueName(activity.spine.animations.map { it.name }, "anim")
                
                activity.enterNameAlert("Enter animation name", suggestedName) { newName ->
                    val existingNames = activity.spine.animations.map { it.name }
                    if (newName in existingNames) {
                        activity.toast("Name $newName already exists.")
                        return@enterNameAlert false
                    }
                    
                    lastSelectedPosition = position
                    createAnim(newName)
                    notifyDataSetChanged()
                    spnAnimations.setSelection(position)
                    true
                }
                // rollup if name not entered.
                spnAnimations.setSelection(lastSelectedPosition)
            } else {
                selectAnim(position)
                lastSelectedPosition = position
            }
        }
    }
    
    private fun createAnim(name: String) {
        animationDataHolder.addAnimation(name)
        activity.onAnimationSwapped()
    }
    
    private fun selectAnim(position: Int) {
        animationDataHolder.updatePosition(position)
        activity.onAnimationSwapped()
    }
    
    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createView(position, parent, isDropdown = false)
    }
    
    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createView(position, parent, isDropdown = true)
    }
    
    private fun createView(position: Int, parent: ViewGroup, isDropdown: Boolean): View {
        val root = LayoutInflater.from(parent.context)
            .inflate(R.layout.spine_anim_item, parent, false)
        val ivAdd = root.findViewById<ImageView>(R.id.ivAdd)
        val llContent = root.findViewById<LinearLayout>(R.id.llContent)
        val tvName = llContent.findViewById<TextView>(R.id.tvName)
        val btnDeleteAnim = llContent.findViewById<ImageView>(R.id.btnDeleteAnim)
        val btnDuplicateAnim = llContent.findViewById<ImageView>(R.id.btnDuplicateAnim)
        if (!isDropdown) {
            tvName.setUnderline()
        }
        ivAdd.setVisible(position == animationContainers.size)
        llContent.setVisible(position < animationContainers.size)
        btnDeleteAnim.setVisible(position < animationContainers.size && isDropdown)
        btnDuplicateAnim.setVisible(position < animationContainers.size && isDropdown)
        if (position == animationContainers.size) {
            return root
        }
        val animContainer = animationContainers[position]
        val anim = animContainer.spineAnimation
        tvName.text = anim.name
        
        btnDeleteAnim.setOnClickListener {
            if (animationContainers.size == 1) {
                return@setOnClickListener
            }
            activity.fastAlert("Delete animation ${anim.name}?") {
                removeAnim(position)
            }
        }
        btnDuplicateAnim.setOnClickListener {
            activity.fastAlert("Duplicate animation?") {
                duplicateAnim(position)
            }
        }
        
        return root
    }
    
    private fun removeAnim(position: Int) {
        animationDataHolder.removeAnimation(position)
        notifyDataSetChanged()
        spnAnimations.setSelection(animationDataHolder.currentAnimationContainerPosition)
        spnAnimations.hideDropdown()
        activity.onAnimationSwapped()
    }
    
    private fun duplicateAnim(position: Int) {
        animationDataHolder.duplicateAnimation(position)
        notifyDataSetChanged()
        spnAnimations.setSelection(animationDataHolder.currentAnimationContainerPosition)
        spnAnimations.hideDropdown()
        activity.onAnimationSwapped()
    }
    
    override fun getItem(position: Int): Any? {
        log("got item $position")
        if (position >= animationContainers.size) {
            return null
        }
        return animationContainers[position]
    }
    
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
    
    override fun getCount(): Int {
        return animationContainers.size + 1
    }
}