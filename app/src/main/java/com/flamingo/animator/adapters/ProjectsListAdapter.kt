package com.flamingo.animator.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SortedList
import androidx.recyclerview.widget.SortedListAdapterCallback
import com.flamingo.animator.R
import com.flamingo.animator.activity.MainActivity
import org.jetbrains.anko.alert
import org.jetbrains.anko.cancelButton
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton

class StringSortedList(adapter: RecyclerView.Adapter<*>) :
    SortedList<String>(String::class.java, object : SortedListAdapterCallback<String>(adapter) {
        override fun areItemsTheSame(s1: String, s2: String) = s1 === s2
        override fun compare(s1: String, s2: String) = s1.compareTo(s2)
        override fun areContentsTheSame(s1: String, s2: String) = s1 == s2
    }) {
    
    
    fun toList() = List(size()) { i -> get(i) }
}


class ProjectsListAdapter(private val activity: MainActivity) :
    RecyclerView.Adapter<ProjectsListAdapter.MyViewHolder>() {
    val repo = activity.repo
    
    val projects = StringSortedList(this)
    
    inner class MyViewHolder(val root: View) : RecyclerView.ViewHolder(root) {
        val tvName = root.findViewById<TextView>(R.id.tvName)!!
    }
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val root = LayoutInflater.from(parent.context)
            .inflate(R.layout.project_item, parent, false)
        return MyViewHolder(root)
    }
    
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val tv = holder.tvName
        val projectName = projects[position]
        tv.text = projectName
        
        holder.root.setOnLongClickListener {
            tv.context.alert {
                title = "Delete $projectName"
                message = "Are you sure?"
                cancelButton { }
                yesButton {
                    with(tv.context) {
                        val projectDir = repo.getProjectDir(projectName)
                        // we shouldn't use position here, because it becomes invalid!!!
                        val deleted = projectDir.deleteRecursively()
                        if (!deleted) {
                            toast("Directory ${projectDir.name} was not deleted.")
                        }
                        projects.remove(projectName)
                        
                        Unit
                    }
                }
            }.show()
            true
        }
        
        holder.root.setOnClickListener {
            activity.openProject(projectName)
        }
    }
    
    override fun getItemCount() = projects.size()
    
    /** Update or add a project to list. [oldName] may be invalid.*/
    fun updateProject(oldName: String, newName: String) {
        // default indexOf is invalid for some reason.
        val oldNameInd = projects.toList().indexOf(oldName)
        if (oldNameInd == -1) {
            projects.add(newName)
        } else {
            projects.updateItemAt(oldNameInd, newName)
        }
    }
}