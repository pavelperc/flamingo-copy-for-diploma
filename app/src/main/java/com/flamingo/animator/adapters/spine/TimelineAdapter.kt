package com.flamingo.animator.adapters.spine

import android.graphics.Color
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SmoothScroller
import com.flamingo.animator.R
import com.flamingo.animator.activity.spine.SpineEditorActivity
import com.flamingo.animator.utils.commonUtils.colorWithAlpha
import com.flamingo.animator.utils.commonUtils.setVisible
import kotlinx.android.synthetic.main.activity_spine_editor.*


class TimelineAdapter(
    val activity: SpineEditorActivity
) : RecyclerView.Adapter<TimelineAdapter.MyViewHolder>() {
    companion object {
        val SECONDS_COUNT = 10
        val MILLIS_COUNT = SECONDS_COUNT * 1000
        val TIMESTAMP_PRECISION = 250
        val TIMESTAMPS_PER_SECOND = 1000 / TIMESTAMP_PRECISION
    }
    
    private var lastPosition = -1
    
    private val highlightColor = colorWithAlpha(activity.getColor(R.color.colorAccent), 68)
    private val notHighlightColor = Color.TRANSPARENT
    
    private var lastHighlighted: View? = null
    
    private val rvTimeline get() = activity.rvTimeline
    
    val animationDataHolder get() = activity.surfaceView.animationDataHolder
    
    val snapHelper = StartSnapHelper()
    val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
    
    
    inner class MyViewHolder(val root: View) : RecyclerView.ViewHolder(root) {
        val tvTime = root.findViewById<TextView>(R.id.tvTime)
        /** Shows the number of animations for component */
        val tvMark = root.findViewById<TextView>(R.id.tvMark)
        val tvMarkAttachmentSwaps = root.findViewById<TextView>(R.id.tvMarkAttachmentSwaps)
        val tvMarkDisabledSubtrees = root.findViewById<TextView>(R.id.tvMarkDisabledSubtrees)
        // duplicate if tvMark is empty
        val tvMarkDisabledSubtreesVertical =
            root.findViewById<TextView>(R.id.tvMarkDisabledSubtreesVertical)
    }
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val root = LayoutInflater.from(parent.context)
            .inflate(R.layout.timeline_item, parent, false)
        return MyViewHolder(root)
    }
    
    override fun getItemCount() = SECONDS_COUNT * TIMESTAMPS_PER_SECOND
    
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tvTime.text =
            if (position % TIMESTAMPS_PER_SECOND == 0)
                (position / TIMESTAMPS_PER_SECOND).toString()
            else "|"
        
        if (position == lastPosition) {
            highlightItem(holder.root)
        }
        
        holder.root.setOnClickListener {
            smoothScroller.targetPosition = position
            layoutManager.startSmoothScroll(smoothScroller)
        }
        
        val timestampsCount = findTimestampsCount(position)
        holder.tvMark.setVisible(timestampsCount > 0)
        holder.tvMark.text = timestampsCount.toString()
        
        val attachmentSwapsCount = findAttachmentSelectionTimestampsCount(position)
        holder.tvMarkAttachmentSwaps.setVisible(attachmentSwapsCount > 0)
        holder.tvMarkAttachmentSwaps.text = attachmentSwapsCount.toString()
        
        val subtreeVisibilityCount = findSubtreeVisibilityTimestampsCount(position)
        
        val useVertical = timestampsCount == 0
        holder.tvMarkDisabledSubtrees.setVisible(subtreeVisibilityCount > 0 && !useVertical)
        holder.tvMarkDisabledSubtreesVertical.setVisible(subtreeVisibilityCount > 0 && useVertical)
        holder.tvMarkDisabledSubtrees.text = subtreeVisibilityCount.toString()
        holder.tvMarkDisabledSubtreesVertical.text = subtreeVisibilityCount.toString()
    }
    
    fun updateItemOnTime(time: Int) {
        val position = time / TIMESTAMP_PRECISION
        notifyItemChanged(position)
    }
    
    private val smoothScroller: SmoothScroller = object : LinearSmoothScroller(activity) {
        override fun getHorizontalSnapPreference() = SNAP_TO_START
        
        override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics?) =
            super.calculateSpeedPerPixel(displayMetrics) * 4
    }
    
    
    private fun highlightItem(view: View?) {
        lastHighlighted?.setBackgroundColor(notHighlightColor)
        view?.setBackgroundColor(highlightColor)
        lastHighlighted = view
    }
    
    private fun findTimestampsCount(position: Int): Int {
        val time = position * TIMESTAMP_PRECISION
        return animationDataHolder.findTimestampsCount(time)
    }
    
    private fun findAttachmentSelectionTimestampsCount(position: Int): Int {
        val time = position * TIMESTAMP_PRECISION
        return animationDataHolder.findAttachmentSelectionTimestampsCount(time)
    }
    
    private fun findSubtreeVisibilityTimestampsCount(position: Int): Int {
        val time = position * TIMESTAMP_PRECISION
        return animationDataHolder.findSubtreeVisibilityTimestampsCount(time)
    }
    
    private fun updateAnimationTime() {
        
        // https://stackoverflow.com/questions/27507715/android-how-to-get-the-current-x-offset-of-recyclerview
        val offset = rvTimeline.computeHorizontalScrollOffset()
        val extent = rvTimeline.computeHorizontalScrollExtent()
        val range = rvTimeline.computeHorizontalScrollRange()
        // use + 1 because we can scroll one pixel after MILLIS_COUNT millis.
        val percentage = offset / (range - extent + 1).toDouble()
        val time = (MILLIS_COUNT * percentage).toInt()
        activity.onAnimationTimeChanged(time)
    }
    
    inner class MyScrollListener : RecyclerView.OnScrollListener() {
        
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            val pos = layoutManager.findFirstVisibleItemPosition()
            
            if (lastPosition != pos) {
                val view = layoutManager.findViewByPosition(pos)
                highlightItem(view)
                lastPosition = pos
            }
            
            updateAnimationTime()
        }
    }
}


