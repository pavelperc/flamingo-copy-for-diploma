package com.flamingo.animator.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.flamingo.animator.R
import com.flamingo.animator.utils.commonUtils.setFilePreview
import java.io.File


class PreviewListAdapter(
    val previewAssets: List<Pair<String, File>>,
    val onSelected: (pos: Int) -> Unit
) :
    RecyclerView.Adapter<PreviewListAdapter.MyViewHolder>() {
    
    inner class MyViewHolder(val root: View) : RecyclerView.ViewHolder(root) {
        val tvName = root.findViewById<TextView>(R.id.tvName)!!
        val ivImage= root.findViewById<ImageView>(R.id.ivImage)!!
    }
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val root = LayoutInflater.from(parent.context)
            .inflate(R.layout.preview_list_item, parent, false)
        return MyViewHolder(root)
    }
    
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val (name, file) = previewAssets[position]
    
        holder.tvName.text = name
        holder.ivImage.setFilePreview(file, false)
        
        holder.root.setOnClickListener { onSelected(holder.adapterPosition) }
    }
    
    override fun getItemCount() = previewAssets.size
}