package com.flamingo.animator.adapters

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.flamingo.animator.R
import com.flamingo.animator.activity.DrawingActivity
import com.flamingo.animator.editors.drawUtils.CheckerBoard
import com.flamingo.animator.editors.drawingEditor.dataHolders.ImageContainer
import com.flamingo.animator.editors.drawingEditor.dataHolders.LayerContainer
import com.flamingo.animator.utils.dialogUtils.enterNameAlert
import com.flamingo.animator.utils.dialogUtils.fastAlert
import com.flamingo.animator.utils.realmUtils.transaction
import com.google.android.material.bottomsheet.BottomSheetDialog
import org.jetbrains.anko.alert
import org.jetbrains.anko.cancelButton
import org.jetbrains.anko.okButton
import kotlin.math.abs


class LayerListAdapter(
    private val activity: DrawingActivity,
    val recyclerView: RecyclerView,
    val bottomSheetDialog: BottomSheetDialog
) :
    RecyclerView.Adapter<LayerListAdapter.MyViewHolder>() {
    
    val layerContainers: List<LayerContainer>
        get() = activity.surfaceView.currentImageContainer.layerContainers
    
    val imageContainer: ImageContainer
        get() = activity.surfaceView.currentImageContainer
    
    
    private val itemTouchHelperCallback = MyItemTouchHelperCallback()
    // should be attached to rv
    val itemTouchHelper: ItemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
    
    inner class MyItemTouchHelperCallback : ItemTouchHelper.Callback() {
        override fun getMovementFlags(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder
        ) = makeMovementFlags(ItemTouchHelper.UP or ItemTouchHelper.DOWN, 0)
        
        private var lastY = 0f
        private var lastTime = System.currentTimeMillis()
        private var speed = 100f
        
        // moving is not working after merge dialog
        private var isMerging = false
        
        fun reset() {
            isMerging = false
            speed = 100f
            lastY = 0f
        }
        
        override fun onChildDraw(
            c: Canvas,
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            dX: Float,
            dY: Float,
            actionState: Int,
            isCurrentlyActive: Boolean
        ) {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            if (isMerging) {
                return
            }
            
            val timeDelta = System.currentTimeMillis() - lastTime
            val deltaY = abs(dY - lastY)
            speed = deltaY / timeDelta // zero division???
            lastTime = System.currentTimeMillis()
            lastY = dY
            
            
            val nearestView = (0 until recyclerView.childCount)
                .map { i -> recyclerView.getChildAt(i) }
                .filter { it != viewHolder.itemView }
                .minBy { view -> abs(viewHolder.itemView.top + dY - view.top) } ?: return
            
            val nearestViewHolder = recyclerView.getChildViewHolder(nearestView) as MyViewHolder
            val deltaToNearest = abs(viewHolder.itemView.top + dY - nearestView.top)

//            log(
//                "speed: $speed, deltaY: $deltaY, dY: $dY, timeDelta: $timeDelta, " +
//                        "nearestViewHolder: ${nearestViewHolder.tvName.text}, " +
//                        "deltaToNearest: $deltaToNearest"
//            )
            if (speed < 0.01 && deltaToNearest < 20) { // 20 is the same as checkers square size.
                isMerging = true
                merge(viewHolder, nearestViewHolder)
            }
        }
        
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            if (isMerging) {
                return false
            }
            val pos1 = viewHolder.adapterPosition
            val pos2 = target.adapterPosition
            
            imageContainer.moveLayer(pos1, pos2)
            notifyItemMoved(pos1, pos2)
            updateSelectedLayer()
            updateSurfaceViewState()
            return true
        }
        
        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {}
    }
    
    private fun merge(src: RecyclerView.ViewHolder, dst: RecyclerView.ViewHolder) {
        src as MyViewHolder
        dst as MyViewHolder
        activity.alert {
            title = "Merge layers"
            message = "Merge ${src.tvName.text} with ${dst.tvName.text}?"
            cancelButton { }
            okButton {
                imageContainer.mergeLayers(src.adapterPosition, dst.adapterPosition)
                notifyItemRemoved(src.adapterPosition)
                updateSelectedLayer() // update the rectangle
                updateSurfaceViewState()
            }
            
        }.show()
    }
    
    
    inner class MyViewHolder(val root: View) : RecyclerView.ViewHolder(root) {
        val tvName = root.findViewById<TextView>(R.id.tvName)!!
        val btnRemove = root.findViewById<ImageButton>(R.id.btnRemoveLayer)!!
        val btnDuplicate = root.findViewById<ImageButton>(R.id.btnDuplicateLayer)!!
        val btnDrag = root.findViewById<ImageButton>(R.id.btnDrag)!!
        val btnVisible = root.findViewById<ImageButton>(R.id.btnVisible)!!
        val ivLayer = root.findViewById<ImageView>(R.id.ivLayer)!!
        
        fun setLayerSelected(isSelected: Boolean) {
            root.isSelected = isSelected
        }
        
        fun setLayerVisible(isVisible: Boolean) {
            btnVisible.isActivated = isVisible
        }
    }
    
    /** Update the screen and back buttons state */
    private fun updateSurfaceViewState(updateBuiltImage: Boolean = true) {
//        activity.surfaceView.updateState(updateBuiltImage)
        with(activity.surfaceView) {
            if (updateBuiltImage) {
                currentImageContainer.invalidateBuiltImageCache()
            }
            // use custom updateScreen for ImageShifter tool!!!
            compoundToolsContainer.currentDrawer?.updateScreen() ?: updateScreen()
            onStateChanged()
        }
    }
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val root = LayoutInflater.from(parent.context)
            .inflate(R.layout.layer_list_item, parent, false)
        return MyViewHolder(root)
    }
    
    private var selectedLayerPosition = imageContainer.selectedLayerPosition
    
    /** Updates layer selection rectangle according to imageContainer */
    fun updateSelectedLayer() {
        if (selectedLayerPosition == imageContainer.selectedLayerPosition) {
            return
        }
        val prevSelected = recyclerView
            .findViewHolderForAdapterPosition(selectedLayerPosition) as? MyViewHolder
        val newSelected = recyclerView
            .findViewHolderForAdapterPosition(imageContainer.selectedLayerPosition) as? MyViewHolder
        
        selectedLayerPosition = imageContainer.selectedLayerPosition
        if (prevSelected == newSelected) {
            return
        }
        prevSelected?.setLayerSelected(false)
        newSelected?.setLayerSelected(true)
    }
    
    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val layerContainer = layerContainers[position]
        val layer = layerContainer.layer
        
        holder.ivLayer.background = CheckerBoard.createCheckeredDrawable()
        
        holder.ivLayer.setImageBitmap(layerContainer.restorableCanvas.mainBitmap)
        
        holder.tvName.text = layer.name
        holder.tvName.setOnClickListener {
            activity.enterNameAlert("Enter layer name", layer.name) { newName ->
                layer.transaction { name = newName }
                holder.tvName.text = newName
                true // may be not unique
            }
        }
        // select
        holder.setLayerSelected(position == imageContainer.selectedLayerPosition)
        holder.root.setOnClickListener {
            imageContainer.selectedLayerPosition = holder.adapterPosition
            updateSelectedLayer()
            layer.transaction { isVisible = true }
            holder.setLayerVisible(layer.isVisible)
            updateSurfaceViewState()
            bottomSheetDialog.dismiss()
        }
        // remove
        holder.btnRemove.setOnClickListener {
            if (layerContainers.size == 1) {
                return@setOnClickListener
            }
            activity.fastAlert("Remove layer?") {
                imageContainer.removeLayer(holder.adapterPosition)
                notifyItemRemoved(holder.adapterPosition)
                updateSelectedLayer() // update the rectangle
                updateSurfaceViewState()
            }
        }
        
        holder.btnDrag.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                itemTouchHelperCallback.reset()
                itemTouchHelper.startDrag(holder)
            }
            true
        }
        // visibility
        holder.setLayerVisible(layer.isVisible)
        holder.btnVisible.setOnClickListener {
            layer.transaction { isVisible = !isVisible }
            updateSurfaceViewState()
            
            holder.setLayerVisible(layer.isVisible)
        }
        holder.btnDuplicate.setOnClickListener {
            activity.fastAlert("Duplicate layer?") {
                val pos = holder.adapterPosition
                imageContainer.duplicateLayer(pos)
                notifyItemInserted(pos + 1)
                if (selectedLayerPosition > pos) {
                    selectedLayerPosition ++
                }
                updateSelectedLayer()
                // to reset the back button and refresh the screen
                activity.surfaceView.updateState()
            }
        }
    }
    
    override fun getItemCount() = layerContainers.size
}