package com.flamingo.animator.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SortedList
import androidx.recyclerview.widget.SortedListAdapterCallback
import com.bumptech.glide.Glide
import com.bumptech.glide.signature.ObjectKey
import com.flamingo.animator.R
import com.flamingo.animator.activity.SpinesMainActivity
import com.flamingo.animator.activity.spine.SpineInfoActivity
import com.flamingo.animator.model.spine.Spine
import com.flamingo.animator.utils.commonUtils.onShowing
import kotlinx.android.synthetic.main.activity_spines_main.*
import org.jetbrains.anko.contentView
import org.jetbrains.anko.dip


/** Sorted by first created */
class OnlySpineSortedList(adapter: RecyclerView.Adapter<*>) :
    SortedList<Spine>(Spine::class.java, object : SortedListAdapterCallback<Spine>(adapter) {
        override fun areItemsTheSame(a1: Spine, a2: Spine) = a1.id == a2.id
        // sorting by id ascending
        override fun compare(a1: Spine, a2: Spine) = a1.id.compareTo(a2.id)
        
        override fun areContentsTheSame(a1: Spine, a2: Spine) =
            a1.id == a2.id
    })

/** Adapter for recycle view containing only spines. */
class OnlySpineListAdapter(
    val activity: SpinesMainActivity
) :
    RecyclerView.Adapter<OnlySpineListAdapter.MyViewHolder>() {
    
    val assetRepo get() = activity.assetRepo
    
    /** List with assets, bound to recycler view. */
    val spines = OnlySpineSortedList(this)
    
    inner class MyViewHolder(val root: View) : RecyclerView.ViewHolder(root) {
        val tvName = root.findViewById<TextView>(R.id.tvName)!!
        val chbSelected = root.findViewById<CheckBox>(R.id.chbSelected)!!
        val ivPreview = root.findViewById<ImageView>(R.id.ivPreview)
    }
    
    val selectedSpines = mutableSetOf<Spine>()
    
    var selectionEnabled = false
        private set
    
    val rvSpines get() = activity.rvSpines
    
    fun dip(x: Int) = activity.dip(x)
    
    var cellWidth = dip(100)
    
    init {
        initRecycleView()
    }
    
    private fun initRecycleView() {
        val contentView = activity.contentView ?: return
        
        contentView.onShowing {
            val width = contentView.width - dip(24)
            val cellCount = width / dip(180)
            cellWidth = (width / cellCount) - dip(8)
            
            rvSpines.layoutManager = GridLayoutManager(activity, cellCount)
        }
    }
    
    
    fun updateSpine(position: Int) {
        spines.updateItemAt(position, spines[position])
    }
    
    
    fun enableSelectionMode(enable: Boolean) {
        if (selectionEnabled == enable) {
            return
        }
        // always clear selection. Useful for delete method.
        selectedSpines.clear()
        selectionEnabled = enable
        notifyDataSetChanged()
    }
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val root = LayoutInflater.from(parent.context)
            .inflate(R.layout.spine_cell_item, parent, false)
        root.layoutParams.apply {
            width = cellWidth
            height = cellWidth
        }
        return MyViewHolder(root)
    }
    
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val tv = holder.tvName
        val spine = spines[position]!!
        val assetName = spine.name
        tv.text = assetName
        
        val previewFile = spine.previewReq.getFile(assetRepo.assetsDir)
        Glide
            .with(activity)
            .load(previewFile)
            .signature(ObjectKey(previewFile.lastModified()))
            .into(holder.ivPreview)
        
        holder.root.setOnLongClickListener {
            if (!selectionEnabled) {
                val pos = holder.adapterPosition
                activity.enableSelectionMode(true)
                selectedSpines.add(spine)
                notifyItemChanged(pos)
            }
            true
        }
        
        holder.root.setOnClickListener {
            if (selectionEnabled) {
                // click checkbox
                holder.chbSelected.performClick()
            } else {
                onSpineClick(spine, holder)
            }
        }
        
        holder.chbSelected.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                selectedSpines.add(spine)
            } else {
                selectedSpines.remove(spine)
            }
        }
        
        holder.chbSelected.visibility = if (selectionEnabled) View.VISIBLE else View.GONE
        holder.chbSelected.isChecked = selectionEnabled && spine in selectedSpines
    }
    
    override fun getItemCount() = spines.size()
    
    private fun onSpineClick(asset: Spine, holder: MyViewHolder) {
        // there was a random bug when accessed from activity result
        val pos = holder.adapterPosition
        activity.activityHelper.startForResult<SpineInfoActivity>(
            "projectName" to SpinesMainActivity.SPINE_PROJECT_NAME,
            "assetId" to asset.id
        ) { _, intent ->
            updateSpine(pos)
        }
    }
}