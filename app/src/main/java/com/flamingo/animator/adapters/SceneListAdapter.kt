package com.flamingo.animator.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.flamingo.animator.R
import com.flamingo.animator.activity.SceneActivity
import com.flamingo.animator.activity.projectTabs.ScenesTab
import com.flamingo.animator.model.Scene
import com.flamingo.animator.utils.dialogUtils.enterNameAlert
import com.flamingo.animator.utils.dialogUtils.fastAlert
import com.flamingo.animator.utils.realmUtils.transaction
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

// We refuse from sorting by names, stay sorted by database adding position.

class SceneListAdapter(
    private val scenesTab: ScenesTab
) :
    RecyclerView.Adapter<SceneListAdapter.MyViewHolder>() {
    
    private val activity = scenesTab.activity
    val assetRepo get() = activity.assetRepo
    val sceneRepo get() = activity.sceneRepo
    
    /** Scenes, that are stored in the list.
     * Adapter should be manually updated on its change.
     * THE ORDER IS OPPOSITE TO DATABASE: last added comes to the beginning. */
    val scenes = mutableListOf<Scene>()
    
    inner class MyViewHolder(val root: View) : RecyclerView.ViewHolder(root) {
        val tvName = root.findViewById<TextView>(R.id.tvName)!!
        
        val menu = PopupMenu(activity, root)
        val menuRemove = menu.menu.add("Remove")
        val menuRename = menu.menu.add("Rename")
        
        init {
            root.setOnLongClickListener {
                menu.show()
                true
            }
        }
    }
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val root = LayoutInflater.from(parent.context).inflate(R.layout.scene_item, parent, false)
        return MyViewHolder(root)
    }
    
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val tvName = holder.tvName
        val scene = scenes[position]
        
        // todo add preview bitmap
        
        tvName.text = scene.name
        
        // on root long click:
        holder.menu.setOnMenuItemClickListener { item ->
            when (item) {
                holder.menuRemove -> {
                    activity.fastAlert("Delete scene ${scene.name}?") {
                        scenes.removeAt(holder.adapterPosition)
                        notifyItemRemoved(holder.adapterPosition)
                        sceneRepo.removeScene(scene)
                    }
                }
                holder.menuRename -> {
                    activity.enterNameAlert(initialName = scene.name) { newName ->
                        if (!sceneRepo.isSceneNameUnique(newName)) {
                            activity.toast("Name $newName is already used.")
                            false
                        } else {
                            scene.transaction { name = newName }
                            tvName.text = newName
                            true
                        }
                    }
                }
            }
            true
        }
        
        holder.root.setOnClickListener {
            activity.startActivity<SceneActivity>(
                "projectName" to activity.projectName,
                "sceneId" to scene.id
            )
        }
    }
    
    override fun getItemCount() = scenes.size
}