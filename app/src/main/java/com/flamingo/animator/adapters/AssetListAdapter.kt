package com.flamingo.animator.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SortedList
import androidx.recyclerview.widget.SortedListAdapterCallback
import com.flamingo.animator.R
import com.flamingo.animator.activity.BackgroundActivity
import com.flamingo.animator.activity.ObjectActivity
import com.flamingo.animator.activity.projectTabs.AssetsTab
import com.flamingo.animator.activity.spine.SpineInfoActivity
import com.flamingo.animator.model.Asset
import com.flamingo.animator.model.Background
import com.flamingo.animator.model.Object
import com.flamingo.animator.model.spine.Spine
import com.flamingo.animator.repository.AssetRepo

class AssetAdapterCallback<A : Asset>(adapter: RecyclerView.Adapter<*>) :
    SortedListAdapterCallback<A>(adapter) {
    override fun areItemsTheSame(a1: A, a2: A) = a1.id == a2.id
    override fun compare(a1: A, a2: A) = a1.name.compareTo(a2.name) // sorting by name
    override fun areContentsTheSame(a1: A, a2: A) =
        a1.name == a2.name // may be invalid for image preview
}

class ObjectSortedList(adapter: RecyclerView.Adapter<*>) :
    SortedList<Object>(Object::class.java, AssetAdapterCallback<Object>(adapter))

class BackgroundSortedList(adapter: RecyclerView.Adapter<*>) :
    SortedList<Background>(Background::class.java, AssetAdapterCallback<Background>(adapter))

class SpineSortedList(adapter: RecyclerView.Adapter<*>) :
    SortedList<Spine>(Spine::class.java, AssetAdapterCallback<Spine>(adapter))

abstract class AssetListAdapter<A : Asset>(
    val assetsTab: AssetsTab
) :
    RecyclerView.Adapter<AssetListAdapter<A>.MyViewHolder>() {
    
    protected val activity = assetsTab.activity
    
    val assetRepo: AssetRepo
        get() = activity.assetRepo
    
    abstract fun onAssetClick(asset: A, holder: MyViewHolder)
    
    /** List with assets, bound to recycler view. */
    abstract val assets: SortedList<A>
    
    inner class MyViewHolder(val root: View) : RecyclerView.ViewHolder(root) {
        val tvName = root.findViewById<TextView>(R.id.tvName)!!
        val chbSelected = root.findViewById<CheckBox>(R.id.chbSelected)!!
    }
    
    val selectedAssets = mutableSetOf<A>()
    
    var selectionEnabled = false
        private set
    
    fun enableSelectionMode(enable: Boolean) {
        if (selectionEnabled == enable) {
            return
        }
        // always clear selection. Useful for delete method.
        selectedAssets.clear()
        selectionEnabled = enable
        notifyDataSetChanged()
    }
    
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val root = LayoutInflater.from(parent.context)
            .inflate(R.layout.asset_item, parent, false)
        return MyViewHolder(root)
    }
    
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val tv = holder.tvName
        val asset = assets[position]!!
        val assetName = asset.name
        tv.text = assetName
        
        holder.root.setOnLongClickListener {
            if (!selectionEnabled) {
                val pos = holder.adapterPosition
                assetsTab.enableSelectionMode(true)
                selectedAssets.add(asset)
                notifyItemChanged(pos)
            }
            true
        }
        
        holder.root.setOnClickListener {
            if (selectionEnabled) {
                // click checkbox
                holder.chbSelected.performClick()
            } else {
                onAssetClick(asset, holder)
            }
        }
        
        holder.chbSelected.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                selectedAssets.add(asset)
            } else {
                selectedAssets.remove(asset)
            }
        }
        
        holder.chbSelected.visibility = if (selectionEnabled) View.VISIBLE else View.GONE
        holder.chbSelected.isChecked = selectionEnabled && asset in selectedAssets
    }
    
    override fun getItemCount() = assets.size()
    
    fun updateAsset(position: Int) {
        assets.updateItemAt(position, assets[position])
    }
}

class BackgroundListAdapter(assetsTab: AssetsTab) : AssetListAdapter<Background>(assetsTab) {
    override val assets = BackgroundSortedList(this)
    
    override fun onAssetClick(asset: Background, holder: MyViewHolder) {
        activity.activityHelper.startForResult<BackgroundActivity>(
            "projectName" to activity.projectName,
            "createNew" to false,
            "assetId" to asset.id
        ) { _, intent ->
            updateAsset(holder.adapterPosition)
            assetsTab.updateAssetsIfNeed(intent)
        }
    }
}

class ObjectListAdapter(assetsTab: AssetsTab) : AssetListAdapter<Object>(assetsTab) {
    override val assets = ObjectSortedList(this)
    
    override fun onAssetClick(asset: Object, holder: MyViewHolder) {
        activity.activityHelper.startForResult<ObjectActivity>(
            "projectName" to activity.projectName,
            "createNew" to false,
            "assetId" to asset.id
        ) { _, intent ->
            updateAsset(holder.adapterPosition)
            assetsTab.updateAssetsIfNeed(intent)
        }
    }
}

class SpineListAdapter(assetsTab: AssetsTab) : AssetListAdapter<Spine>(assetsTab) {
    override val assets = SpineSortedList(this)
    
    override fun onAssetClick(asset: Spine, holder: MyViewHolder) {
        activity.activityHelper.startForResult<SpineInfoActivity>(
            "projectName" to activity.projectName,
            "assetId" to asset.id
        ) { _, intent ->
            updateAsset(holder.adapterPosition)
            assetsTab.updateAssetsIfNeed(intent)
        }
    }
}